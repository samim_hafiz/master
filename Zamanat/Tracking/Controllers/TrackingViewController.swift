//
//  TrackingViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 2/4/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import Lottie
import GoogleMaps
import GooglePlaces
import Floaty

private let cellId = "CellIdentifier";

class TrackingViewController: UIViewController, TrackingDelegate {
    
    var trackers = [Tracking]();
    var markers = [String: GMSMarker]();
    var myMapView: GMSMapView!
    let locationManager = CLLocationManager()
    var lastLocation: CLLocationCoordinate2D?
    var popupViewHeightAnchor: NSLayoutConstraint?
    var tableViewTopAnchor: NSLayoutConstraint?
    var isPopupOpen = false;
    
    let popupView: UIView = {
        let view = UIView();
        view.backgroundColor = Style.mainColor.withAlphaComponent(0.8);
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    lazy var openClosePopup: UIButton = {
        let button = UIButton();
        button.setImage(#imageLiteral(resourceName: "openArrow").withRenderingMode(.alwaysTemplate), for: .normal);
        button.imageView?.tintColor = .white;
        button.imageView?.contentMode = .scaleAspectFit;
        button.addTarget(self, action: #selector(openOrClosePopup), for: .touchUpInside);
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    lazy var trackersTableView: UITableView = {
        let table = UITableView();
        table.separatorStyle = .none;
        table.delegate = self;
        table.dataSource = self;
        table.register(TrackerFriendCell.self, forCellReuseIdentifier: cellId);
        table.translatesAutoresizingMaskIntoConstraints = false;
        return table;
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        
        let camera = GMSCameraPosition.camera(withLatitude: 34.479295, longitude: 69.11341099999, zoom: 15);
        myMapView = GMSMapView.map(withFrame: view.bounds, camera: camera);
        myMapView.settings.myLocationButton = false;
        myMapView.settings.compassButton = false;
        myMapView.delegate = self;
        myMapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0);
        view = myMapView;
        
        locationManager.delegate = self;
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            myMapView.isMyLocationEnabled = true
            myMapView.settings.myLocationButton = true
            
        } else {
            checkLocationAuthorizationStatus();
        }
        
        view.addSubview(popupView);
        popupView.addSubview(openClosePopup);
        popupView.addSubview(trackersTableView);
        
        loadTrackings();
        setupConstraints();
        
        listenToAddRemoveTrack();
        listenToCreateTripTrack();
        listenToLiveTrackDetails();
        listenToTripUpdateStatus();
    }
    
    @objc private func openOrClosePopup() {
        isPopupOpen = !isPopupOpen;
        
        if(isPopupOpen) {
            popupView.backgroundColor = .white;
            openClosePopup.setImage(#imageLiteral(resourceName: "collapseArrow").withRenderingMode(.alwaysTemplate), for: .normal);
            openClosePopup.imageView?.tintColor = Style.mainColor;
            popupViewHeightAnchor?.constant = 300;
            tableViewTopAnchor?.isActive = true;
        } else {
            popupView.backgroundColor = Style.mainColor.withAlphaComponent(0.8);
            openClosePopup.setImage(#imageLiteral(resourceName: "openArrow").withRenderingMode(.alwaysTemplate), for: .normal);
            popupViewHeightAnchor?.constant = 40;
            tableViewTopAnchor?.isActive = false;
            openClosePopup.imageView?.tintColor = .white;
        }
        
        if self.trackers.count <= 0 {
            self.trackersTableView.backgroundView = EmptyStateView(message: "NoTrackerFound".localized);
        } else {
            self.trackersTableView.backgroundView = UIView();
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded();
        })
    }
    
    private func loadTrackings() {
        guard let userId = Values.currentUser?.id else { return }
        
        let parameters = ["user_id": userId];
        self.view.startActivityIndicator();
        Helpers.request(api: API.getMyTracks, method: .post, parameters: parameters) { (result) in
            self.view.stopActivityIndicator();
            
            guard let result = result else { return }
            if let success = result["success"] as? Bool, success {
                guard let trackersData = result["data"] as? Array<[String: Any]> else { return }
                for track in trackersData {
                    let tracker = Tracking(dic: track);
                    self.trackers.append(tracker);
                    if let tripId = tracker.trip?.id {
                        SocketIOManager.sharedInstance.socket.emit("joinroom", ["trip_id": tripId, "user_id": userId]);
                    }
                }
                self.showTrackers();
                self.trackersTableView.reloadData();
                
                if self.trackers.count <= 0 {
                    self.trackersTableView.backgroundView = EmptyStateView(message: "No tracking found. When people invited you to track them, they will be appear here.".localized);
                }
            }
        }
    }
}

extension TrackingViewController: UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, GMSMapViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trackers.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TrackerFriendCell;
        cell.person = trackers[indexPath.row].tracker;
        cell.trackingDelegate = self;
        return cell;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58;
    }
    
    /// Check if user authorized access to current location of the device.
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            myMapView.isMyLocationEnabled = false;
            myMapView.settings.myLocationButton = false;
            locationManager.startUpdatingLocation();
        } else {
            locationManager.requestWhenInUseAuthorization();
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            myMapView.isMyLocationEnabled = false;
            myMapView.settings.myLocationButton = false;
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            if(lastLocation == nil){
                self.myMapView.camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 14);
                lastLocation = location.coordinate;
            }
            self.myMapView.selectedMarker?.position = location.coordinate;
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return false;
    }
}

extension TrackingViewController {

    fileprivate func setupConstraints() {
        popupView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true;
        popupView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true;
        popupViewHeightAnchor = popupView.heightAnchor.constraint(equalToConstant: 40);
        popupViewHeightAnchor?.isActive = true;
        popupView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        openClosePopup.topAnchor.constraint(equalTo: popupView.topAnchor).isActive = true;
        openClosePopup.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        openClosePopup.heightAnchor.constraint(equalToConstant: 38).isActive = true;
        openClosePopup.widthAnchor.constraint(equalToConstant: 38).isActive = true;
        
        self.tableViewTopAnchor = trackersTableView.topAnchor.constraint(equalTo: openClosePopup.bottomAnchor, constant: 8);
        self.tableViewTopAnchor?.isActive = false;
        trackersTableView.widthAnchor.constraint(equalTo: popupView.widthAnchor).isActive = true;
        trackersTableView.bottomAnchor.constraint(equalTo: popupView.bottomAnchor, constant: -4).isActive = true;
        trackersTableView.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
    }
    
    func updateTracking(id: String, tracking: Bool) {
        if !tracking {
            self.markers[id]?.map = nil;
        } else  {
            self.markers[id]?.map = self.myMapView;
        }
    }
    
    fileprivate func listenToAddRemoveTrack() {
        SocketIOManager.sharedInstance.socket.on("add-remove-live-track") { (result, _) in
            if let response = result as? Array<[String: Any]> {
                if let data = response[0]["data"] as? [String: Any] {
                    print("add-remove-live-track: ", data)
                }
            }
        }
    }
    
    fileprivate func listenToCreateTripTrack() {
        SocketIOManager.sharedInstance.socket.on("create-trip-track") { (result, _) in
            if let response = result as? Array<[String: Any]> {
                if let data = response[0]["data"] as? [String: Any] {
                    print("create-trip-track: ", data)
                }
            }
        }
    }
    
    fileprivate func listenToLiveTrackDetails() {
        SocketIOManager.sharedInstance.socket.on("liveTrack") { (result, _) in
            if let response = result as? Array<[String: Any]> {
                if let data = response[0]["data"] as? [String: Any] {
                    print("liveTrack: ", data)
                }
            }
        }
    }
    
    fileprivate func listenToTripUpdateStatus() {
        SocketIOManager.sharedInstance.socket.on("trip-update-status") { (result, _) in
            if let response = result as? Array<[String: Any]> {
                if let data = response[0]["data"] as? [String: Any] {
                    print("trip-update-status: ", data)
                }
            }
        }
    }
    
    private func showTrackers() {
        for tracker in trackers {
            var lastLocation: CLLocationCoordinate2D!
            if let id = tracker.id {
                for cordinates in tracker.trackingLocations {
                    if self.markers[id] != nil { self.markers[id]?.map = nil }
                    lastLocation = cordinates;
                }
                self.markers[id] = GMSMarker(position: lastLocation);
                self.markers[id]?.tracksViewChanges = true;
                if tracker.tracker?.imageUrl == nil {
               
                    self.markers[id]?.icon = #imageLiteral(resourceName: "roundedProfile").imageWithImage(scaledToSize: CGSize(width: 32, height: 32));
                    self.markers[id]?.map = self.myMapView;
                } else {
                    Helpers.getImageFromUrl(urlStr: tracker.tracker!.imageUrl!, scale: CGSize(width: 32, height: 32), completion: { (uiimage) in
                        self.markers[id]?.icon = uiimage
                        self.markers[id]?.map = self.myMapView;
                    })
                }
            }
        }
    }
}

