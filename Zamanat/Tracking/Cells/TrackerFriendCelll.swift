//
//  TrackerCellTableViewCell.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 6/2/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class TrackerFriendCell: UITableViewCell {
    
    var trackingDelegate: TrackingDelegate?
    
    var person: Tracker? {
        didSet {
            if let url = person?.imageUrl {
                self.profileImageView.setImageFromUrl(urlStr: url);
            }
            fullNameLabel.text = person?.displayName;
            phoneNumberLabel.text = person?.phone;
            if let track = person?.isTracking {
                self.isSelected = track;
            }
        }
    }

    let profileImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "roundedProfile").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFill;
        imageView.layer.borderColor = Style.mainColor.cgColor;
        imageView.layer.borderWidth = 1;
        imageView.layer.cornerRadius = 22;
        imageView.layer.masksToBounds = true;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let fullNameLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let phoneNumberLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallFont;
        label.textColor = Style.grayFontColor.withAlphaComponent(0.8);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var eyeButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(toggleTrack), for: .touchUpInside);
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
        button.setImage(#imageLiteral(resourceName: "eye").withRenderingMode(.alwaysTemplate), for: .normal);
        button.tintColor = Style.mainColor;
        button.imageView?.contentMode = .scaleAspectFit;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        backgroundColor = .white;
        self.selectionStyle = .none;
        
        addSubview(profileImageView);
        addSubview(fullNameLabel);
        addSubview(phoneNumberLabel);
        addSubview(eyeButton);
        
        setupConstraints();
    }
    
    @objc private func toggleTrack(){
         self.isSelected = !isSelected;
        if let id = self.person?.id {
            self.trackingDelegate?.updateTracking(id: id, tracking: self.isSelected);
        }
    }
    
    private func setupConstraints() {
        //        profileImageView
        profileImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
        profileImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true;
        profileImageView.heightAnchor.constraint(equalToConstant: 44).isActive = true;
        profileImageView.widthAnchor.constraint(equalToConstant: 44).isActive = true;
        
        //        fullNameLabel
        fullNameLabel.topAnchor.constraint(equalTo: profileImageView.topAnchor).isActive = true;
        fullNameLabel.heightAnchor.constraint(equalTo: profileImageView.heightAnchor, multiplier: 0.5).isActive = true;
        fullNameLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 12).isActive = true;
        
        
        //        phoneNumberLabel
        phoneNumberLabel.topAnchor.constraint(equalTo: fullNameLabel.bottomAnchor, constant: 2).isActive = true;
        phoneNumberLabel.leadingAnchor.constraint(equalTo: fullNameLabel.leadingAnchor).isActive = true;
        
        eyeButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true;
        eyeButton.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true;
        eyeButton.heightAnchor.constraint(equalToConstant: 40).isActive = true;
        eyeButton.widthAnchor.constraint(equalToConstant: 40).isActive = true;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.profileImageView.layer.opacity = 1;
                self.fullNameLabel.layer.opacity = 1;
                 self.phoneNumberLabel.layer.opacity = 1;
                self.eyeButton.layer.opacity = 1;
            } else {
                self.profileImageView.layer.opacity = 0.5;
                self.fullNameLabel.layer.opacity = 0.5;
                self.phoneNumberLabel.layer.opacity = 0.5;
                self.eyeButton.layer.opacity = 0.5;
            }
        }
    }
}

