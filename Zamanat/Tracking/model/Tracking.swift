//
//  Tracking.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 6/2/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation
import GoogleMaps

enum TrackingType: Int {
    case trip = 1, ride = 2, liveTrack = 3;
}

class Tracking {
    var id: String?
    var userId: String?
    var refId: String?
    var type: TrackingType?
    var updated: TimeInterval?
    var created: TimeInterval?
    var lastSeen: TimeInterval?
    var tracker: Tracker?
    var trip: Trip?
    var trackingLocations = [CLLocationCoordinate2D]();
    
    init(dic: [String: Any]) {
        
        self.id = dic["_id"] as? String;
        self.userId = dic["user_id"] as? String;
        self.refId = dic["ref_id"] as? String;
        if let typeNum = dic["type"] as? Int {
            self.type = TrackingType(rawValue: typeNum);
        }
        self.lastSeen = dic["last_seen"] as? TimeInterval;
        if let userDic = dic["user"] as? [String: Any] {
            self.tracker = Tracker(dic: userDic);
        }
        if let tripDic = dic["trip"] as? [String: Any] {
            self.trip = Trip(dic: tripDic);
        }
        if let trakingData = dic["tracking"] as? Array<[String: Any]> {
            for trackDic in trakingData {
                if let lat = trackDic["lat"] as? Double, let long = trackDic["lng"] as? Double {
                    self.trackingLocations.append(CLLocationCoordinate2D(latitude: lat, longitude: long));
                }
            }
        }
    }
}
