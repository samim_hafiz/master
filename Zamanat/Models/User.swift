//
//  User.swift
//  Maktab
//
//  Created by SNSR on 2/9/18.
//  Copyright © 2018 Sepehr Technologies. All rights reserved.
//

import Foundation

//enum RegisterMethod: Int {
//    case facebook = 1, google = 2, phone = 3;
//}

class User: NSObject, NSCoding {
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "_id");
        aCoder.encode(self.name, forKey: "name");
        aCoder.encode(self.lastName, forKey: "last_name");
        aCoder.encode(self.displayName, forKey: "display_name");
        aCoder.encode(self.email, forKey: "email");
        aCoder.encode(self.lastUpdate, forKey: "last_update");
        aCoder.encode(self.dateOfBirth, forKey: "date_of_birth");
        aCoder.encode(self.thumbUrl, forKey: "thumb_url");
        aCoder.encode(self.job, forKey: "job");
        aCoder.encode(self.province, forKey: "province");
        aCoder.encode(self.state, forKey: "state");
        aCoder.encode(self.country, forKey: "country");
        aCoder.encode(self.lastLogin, forKey: "lastlogin");
        aCoder.encode(self.phone, forKey: "phone");
        aCoder.encode(self.typeInt, forKey: "type");
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "_id") as? String;
        self.name = aDecoder.decodeObject(forKey: "name") as? String;
        self.lastName = aDecoder.decodeObject(forKey: "last_name") as? String;
        self.displayName = aDecoder.decodeObject(forKey: "display_name") as? String;
        self.email = aDecoder.decodeObject(forKey: "email") as? String;
        self.lastUpdate = aDecoder.decodeObject(forKey: "last_update") as? TimeInterval;
        self.dateOfBirth = aDecoder.decodeObject(forKey: "date_of_birth") as? TimeInterval;
        self.thumbUrl = aDecoder.decodeObject(forKey: "thumb_url") as? String;
        self.job = aDecoder.decodeObject(forKey: "job") as? String;
        self.province = aDecoder.decodeObject(forKey: "province") as? String;
        self.country = aDecoder.decodeObject(forKey: "country") as? String;
        self.state = aDecoder.decodeObject(forKey: "state") as? String;
        self.lastLogin = aDecoder.decodeObject(forKey: "lastlogin") as? String;
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String;
        self.typeInt = aDecoder.decodeObject(forKey: "type") as? Int;
    }
    
    var id: String?
    var name: String?
    var lastName: String?
    var displayName: String?
    var email: String?
    var lastUpdate: TimeInterval?
    var dateOfBirth: TimeInterval?
    var thumbUrl: String?
    var job: String?
    var province: String?
    var country: String?
    var state: String?
    var lastLogin: String?
    var phone: String?
    private var typeInt: Int?
    var type: UserType! {
        if let type = self.typeInt {
            return UserType(rawValue: type);
        }
        return .normalUser;
    }
    
    private var registeredMethod: Int? = 3;
    
    init(dic: [String: Any]) {
        
        //print("Dic received: \n\n\n\n\n\n\n", dic);
        
        if let id = dic["_id"] as? String { self.id = id }
        if let name = dic["name"] as? String { self.name = name }
        if let displayName = dic["display_name"] as? String {self.displayName = displayName};
        if let last_name = dic["last_name"] as? String { self.lastName = last_name }
        if let email = dic["email"] as? String { self.email = email }
        if let lastUpdate = dic["last_update"] as? TimeInterval { self.lastUpdate = lastUpdate }
        if let date_of_birth = dic["date_of_birth"] as? TimeInterval { self.dateOfBirth = date_of_birth }
        if let thumb_url = dic["thumb_url"] as? String { self.thumbUrl = thumb_url }
        if let job = dic["job"] as? String { self.job = job }
        if let province = dic["province"] as? String { self.province = province }
        if let country = dic["country"] as? String { self.country = country }
        if let state = dic["state"] as? String { self.state = state }
        if let lastLogin = dic["lastlogin"] as? String { self.lastLogin = lastLogin }
        if let phone = dic["phone"] as? String { self.phone = phone } else if let phoneInt = dic["phone"] as? Int {
            self.phone = "\(phoneInt)";
        }
        if let type = dic["type"] as? Int { self.typeInt = type }
    }
    
//    var registerMethod: RegisterMethod {
//        return RegisterMethod(rawValue: self.registeredMethod!)!;
//    }
}
enum UserType: Int {
    case normalUser = 1, driver = 2;
}

