//
//  Person.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 4/10/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation

class Friend {
    var id: String?
    var name: String?
    var lastName: String?
    var displayName: String?
    var lastUpdate: TimeInterval?
    var thumbUrl: String?
    var phone: String?
    var favorite = false;
    var friendship: Friendship = .notFriend;
    
    init(dic: [String: Any]) {
        print("Dic: ", dic);
        if let favorite = dic["favorite"] as? Bool { self.favorite = favorite }
        if let lastUpdate = dic["last_update"] as? TimeInterval { self.lastUpdate = lastUpdate }
        if let userDic = dic["user_id"] as? [String: Any] {
            if let id = userDic["_id"] as? String { self.id = id }
            if let name = userDic["name"] as? String { self.name = name }
            if let displayname = userDic["display_name"] as? String { self.displayName = displayname }
            if let last_name = userDic["last_name"] as? String { self.lastName = last_name }
            if let thumb_url = userDic["thumb_url"] as? String { self.thumbUrl = thumb_url }
            if let phone = userDic["phone"] as? String { self.phone = phone } else if let phoneInt = userDic["phone"] as? Int {
                self.phone = "\(phoneInt)";
            }
        }
    }
}

