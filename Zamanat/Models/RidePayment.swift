//
//  RidePayment.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 10/13/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation

enum Currency: String {
    case afn = "AFN";
}

struct RideDetailsPayment {
    var id: String?
    var amount: Double?
    var currency: Currency = .afn;
    var distance: Double = 0;
    var distanceStr: String = "";
    var duration: Int = 0;
    var durationStr: String = "";
    var toAddress: String?
    var userId: String?
    var tripId: String?
    var driver: Driver?
    var message: String?
    
    init(dic: [String: Any]) {
        print("\n\n\nRide Payment Dic: ", dic)
        self.id = dic["_id"] as? String;
        self.amount = dic["amount"] as? Double;
        if let currency = dic["currency"] as? String {
            self.currency = Currency(rawValue: currency)!;
        }
        if let distance = dic["distance"] as? String {
            self.distanceStr = distance;
            self.distance = Double(distance.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression))!
        }
        if let duration = dic["duration"] as? String {
            self.durationStr = duration;
            self.duration = Int(duration.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression))!
        }
        self.toAddress = dic["to_addr"] as? String;
        self.userId = dic["to_user_id"] as? String;
        self.tripId = dic["trip_id"] as? String;
        if let driverDic = dic["user"] as? [String: Any] {
            self.driver = Driver(dic: driverDic);
        }
        self.message = dic["message"] as? String;
    }
}
