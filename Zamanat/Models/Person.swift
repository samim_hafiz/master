//
//  Person.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 4/10/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation

class Person {
    var id: String?
    var name: String?
    var lastName: String?
    var displayName: String?
    var email: String?
    var lastUpdate: TimeInterval?
    var thumbUrl: String?
    var phone: String?
    var friendship: Friendship = .notFriend;
    
    init(dic: [String: Any]) {
        self.id = dic["_id"] as? String;
        self.name = dic["name"] as? String;
        self.displayName = dic["display_name"] as? String;
        if let last_name = dic["last_name"] as? String { self.lastName = last_name }
        if let email = dic["email"] as? String { self.email = email }
        if let lastUpdate = dic["last_update"] as? TimeInterval { self.lastUpdate = lastUpdate }
        if let thumb_url = dic["thumb_url"] as? String { self.thumbUrl = thumb_url }
        if let phone = dic["phone"] as? String { self.phone = phone } else if let phoneInt = dic["phone"] as? Int {
            self.phone = "\(phoneInt)";
        }
    }
}

class Driver {
    var id: String?
    private var name_: String?
    private var lastName_: String?
    var displayName: String?
    var thumbUrl: String?
    var phone: String?
    var carPlateNo: String?
    var carType: String?
    var carMake: String?
    var carModel: String?
    var carColor: String?
    var latitude: Float?
    var langtitude: Float?
    var distanceToUser: String?
    var durationToUser: String?
    
    init(dic: [String: Any]) {
        print("driver dic: \n\n\n\n\n\n\n\n", dic, "\n\n\n\n\n\n\n\n\n")
        self.id = dic["_id"] as? String;
        self.displayName = dic["display_name"] as? String;
        self.thumbUrl = dic["thumb_url"] as? String;
        if let phone = dic["phone"] as? String {
            self.phone = phone
        } else if let phoneInt = dic["phone"] as? Int {
            self.phone = "\(phoneInt)";
        }
        self.carPlateNo = dic["car_plate_no"] as? String;
        self.carType = dic["car_type"] as? String;
        self.carMake = dic["car_make"] as? String;
        self.carModel = dic["car_model"] as? String;
        self.carColor = dic["car_color"] as? String;
        if let currentAddress = dic["current_address"] as? [String: Any] {
            self.latitude = currentAddress["lat"] as? Float;
            self.langtitude = currentAddress["lng"] as? Float;
        }
    }
}

//enum FriendshipAction: Int {
//    case none = -1, sentMeRequest = 0, sentHimRequest = 1, blockedMe = -11, blockedHim = 5, friend = -12, cancelRequest = 2, rejectRequst = 3, acceptRequest = 4;
//}

enum Friendship: Int {
    case notFriend = 1, friend = 2, requestSent = 3, requestReceived = 4;
}


