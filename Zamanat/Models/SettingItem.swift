//
//  SettingItem.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 5/13/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

enum SettingAction {
    case wallet, paymentMethod, history, fares, profile, blocking, tutorial, about, logout, none;
}
struct SettingItem {
    var image: UIImage?
    var title: String?
    var action: SettingAction = .none;
}
