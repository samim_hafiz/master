//
//  Avatar.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 3/26/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation

class Tracker {
    var id: String?
    var imageUrl: String?
    var name: String?
    var displayName: String?
    var isTracking = false;
    var phone: String?
    
    init(dic: [String: Any]){
        print("Tracker DIC: ", dic);
         if let userDic = dic["user_id"] as? [String: Any] {
            self.id = userDic["_id"] as? String;
            self.name = userDic["name"] as? String;
            self.displayName = userDic["display_name"] as? String;
            self.imageUrl = userDic["thumb_url"] as? String;
            if let phone = userDic["phone"] as? String {
                self.phone = phone;
            } else if let phoneInt = userDic["phone"] as? Int {
                self.phone = "\(phoneInt)";
            }
         } else {
            self.id = dic["_id"] as? String;
            self.name = dic["name"] as? String;
            self.displayName = dic["display_name"] as? String;
            self.imageUrl = dic["thumb_url"] as? String;
            if let phone = dic["phone"] as? String {
                self.phone = phone;
            } else if let phoneInt = dic["phone"] as? Int {
                self.phone = "\(phoneInt)";
            }
        }
    }
}
