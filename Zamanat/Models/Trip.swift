//
//  Trip.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 5/8/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation

enum TripStatus: Int {
    case active = 0, completed = 1, cancel = 2;
}

class Trip  {
    var track: Track?
    var status: TripStatus?
    var id: String?
    var fromAddress: String?
    var fromLatitude: Float?
    var fromLongtitude: Float?
    var toAddress: String?
    var toLatitude: Float?
    var toLongtitude: Float?
    var user: Person?
    var updated: TimeInterval?
    var duration: String?
    var distance: String?
    var ride: Ride?
    var fromLocationId: String?
    var toLocationId: String?
    
    init(dic: [String: Any]) {
        
        if let track = dic["track"] as? [String: Any] {
            self.track = Track(dic: track);
        }
        
        var friends = [Tracker]();
        if let trackers_ = dic["friends"] as? Array<[String: Any]> {
            for tracker in trackers_ {
                friends.append(Tracker(dic: tracker));
            }
            self.track?.updateTrakers(friends: friends);
        }
        
        if let trip = dic["trip"] as? [String: Any] {
            
//            print("Trip details: ", trip);
    
            
            self.id = trip["_id"] as? String;
            self.fromAddress = trip["from_addr"] as? String;
            if let status = trip["status"] as? Int {
                self.status = TripStatus(rawValue: status);
            }
            self.fromLatitude = trip["from_lat"] as? Float;
            self.fromLongtitude = trip["from_lng"] as? Float;
            self.toAddress = trip["to_addr"] as? String;
            self.updated = trip["updated"] as? TimeInterval;
            self.toLatitude = trip["to_lat"] as? Float;
            self.toLongtitude = trip["to_lng"] as? Float;
            if let userDic = trip["user_id"] as? [String: Any] {
                self.user = Person(dic: userDic);
            }
            self.distance = trip["distance"] as? String;
            
            
            self.duration = trip["duration"] as? String;
        } else {
            self.id = dic["_id"] as? String;
            self.fromAddress = dic["from_addr"] as? String;
            if let status = dic["status"] as? Int {
                self.status = TripStatus(rawValue: status);
            }
            self.fromLatitude = dic["from_lat"] as? Float;
            self.fromLongtitude = dic["from_lng"] as? Float;
            self.toAddress = dic["to_addr"] as? String;
            self.updated = dic["updated"] as? TimeInterval;
            self.toLatitude = dic["to_lat"] as? Float;
        }
        
        if let ride = dic["ride"] as? [String: Any] {
            self.ride = Ride(dic: ride);
        }
    }
}

struct Ride {
    var driverAssigned: Bool = false;
    var rideStatus: RideStatus = .inactive;
    var paymentStatus: PaymentStatus?
    var paymentMethod: PaymentMethod?
    var price: Float?
    var currency: String?
    var id: String?
    var passengerId: String?
    var tripId: String?
    var placeId: String?
    var durationToUser: String?
    var distanceToUser: String?
    var update: Double?
    var driver: Driver?
    var paymentDetails = [String: Any]();
    
    init(dic: [String: Any]) {
        
        //print("Ride Details ", dic);
        
        if let reserved = dic["reserved"] as? Bool { self.driverAssigned = reserved }
        if let rideStatusNum = dic["ride_status"] as? Int { self.rideStatus = RideStatus(rawValue: rideStatusNum)! }
        if let paymentStatusNum = dic["payment"] as? Int { self.paymentStatus = PaymentStatus(rawValue: paymentStatusNum)! }
        if let paymentMethodNum = dic["payment_type"] as? Int { self.paymentMethod = PaymentMethod(rawValue: paymentMethodNum)! }
        
        if let price = dic["cost"] as? Double{
            self.price = Float(price)
        }
        
        self.currency = dic["currency"] as? String;
        if let rideId = dic["_id"] as? String {
            self.id = rideId;
        } else {
            self.id = dic["ride_id"] as? String;
        }
        self.passengerId = dic["user_id"] as? String;
        self.tripId = dic["trip_id"] as? String;
        self.placeId = dic["placeid"] as? String;
        self.durationToUser = dic["to_user_duration"] as? String;
        self.distanceToUser = dic["to_user_distance"] as? String;
        self.update = dic["updated"] as? Double;
        if let driverDic = dic["driver_id"] as? [String: Any] {
            self.driver = Driver(dic: driverDic);
        }
//
//        if let rideId = self.id, let price = self.price, let currency = self.currency, let tripId = self.tripId, let userId = Values.currentUser?.id, let toUserId = self.driver?.id, let toAddress = self.ri {
//       paymentDetails = ["amount": self.price ?? 0, "currency": self.currency ?? "AFN", "_id": rideId, "trip_id": self.tripId ?? "--", "user_id": userId, "to_user_id": toUserId, "to_addr": toAddress, "distance": distance, "duration": duration, "user": user] as [String : Any];
//        }
    }
}

class Track {
    private var _trackers = [Tracker]();
    var id: String?
    var refId: String?
    var trackers = [Tracker]();
    
    init(dic: [String: Any]) {
        self.id = dic["_id"] as? String;
        self.refId = dic["ref_id"] as? String;
        if let traking = dic["trackers"] as? Array<[String: Any]> {
            for trackerDic in traking {
                let tracker = Tracker(dic: trackerDic);
                self._trackers.append(tracker);
            }
        }
    }
    
    func updateTrakers(friends: [Tracker]) {
        self.trackers = friends;
        for tracker in self._trackers {
            self.trackers.first(where: {$0.id! == tracker.id!})?.isTracking = true;
        }
    }
}

enum RideStatus: Int {
    case active = 1, inactive = 0, completed = 2;
}
enum PaymentMethod: Int {
    case wallet = 2, cash = 1;
}
enum PaymentStatus: Int {
    case paid = 1, notPaid = 0, pending = 2;
}

