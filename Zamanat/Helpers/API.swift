//
//  API.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 3/27/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation

struct API {
    static let live =  "http://139.59.2.44:3000";
//    static let testLive = "http://139.59.2.44:3001";
    static let server = live;
    static let apiServer = server + "/api/";
    static let socketUrl = server;
    static let login = apiServer + "login";
    static let searchUsers = apiServer + "searchUsers";
    static let friendship = apiServer + "friendRequest";
    static let unfriend = apiServer + "unFriend";
    static let checkPhone = apiServer + "checkPhone";
    static let registerUser = apiServer + "register";
    static let addTrip =  apiServer + "trip";
    static let changeTripStatus = apiServer + "tripStatusUpdate";
    static let getMyTracks = apiServer + "getMyTracks";
    static let blockUnblockUser = apiServer + "blockUnblockUser";
    static let addRemoveTracker = apiServer + "addRemoveTrackers";
    static let getCurrentActiveTrip = apiServer + "getCurrentTrip";
    static let updateFcmToken = apiServer + "addDeviceToken";
    static let rateDriver = apiServer + "addDriverRate";
    static let requestRide = apiServer + "requestRide";
    static let getRidePrice = apiServer + "getRidePrice";
    static let resetPassword = apiServer + "resetPassword";
    static let fareCalculation = apiServer + "fareCalculation";
    static func getAllActiveTrips(userId: String = "", page: Int = 1) -> String {
       return apiServer + "getAllUserTrips/\(userId)/\(page)";
    }
    static func getAllUserTrips(userId: String = "", page: Int = 1) -> String {
        return apiServer + "getAllUserTrips/\(userId)/\(page)";
    }
    static func getAllFriends(userId: String) -> String {
        return apiServer + "getAllFriends/" + userId;
    }
    static func imageUrl(name: String) -> String {
        
        print("Image url: \(server + "/public/uploads/" + name)");
        return server + "/public/uploads/" + name;
    }
    static let getPaymentDetails = apiServer + "getPaymentDetails"
    static let uploadImage = apiServer + "uploadImages"
    static let staticData = apiServer + "getStatic"
    static let updateUserDetails = apiServer + "update"
    
    static func getAllBlockedUsers(userId: String) -> String {
        return apiServer + "/getBlockedUsers/" + userId;
    }
}
