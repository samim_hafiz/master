//
//  KeyStrings+Values.swift
//  CryptoNews
//
//  Created by SNSR on 11/26/17.
//  Copyright © 2017 Sepehr Technologies. All rights reserved.
//

import UIKit

struct KeyStrings {
    static let currentTheme = "currentTheme";
    static let phoneNumber = "phoneNumber";
    static let statusText = "statusText";
    static let userDetails = "userDetails";
    static let currentLanguage = "currentLanguage";
    static var fcmToken: String? = nil;
    static let currentLanguageCode = "currentLanguageCode";
    static let header = ["CB-VERSION": "2017-08-07"]
    static let googleAPIKey = "AIzaSyDeD6RV5ck1QI0lfX2iVLs7TMU3mtmUzkA";
    static let googlePlacesKey = "AIzaSyA_MK1mWQd7ejCZ1KKyhH3iWjiwpmvXkzg";
    
    //Notification Name
    static let paymentCellSelectonNotificationName = "paymentCellSelectionName"
}

struct Values {
    static var numberOfDiverMessage = "Driver not Assigned Yet";
    static var isFcmTokenSent = false;
    static var numberOfDriversAvailable = 0;
    static let checkAvailableDriversTimeInterval: TimeInterval = 30;
    static var currentUser: User? {
        set {
            if newValue != nil {
                let encodedUser = NSKeyedArchiver.archivedData(withRootObject: newValue!);
                UserDefaults.standard.setValue(encodedUser, forKey: "currentUser");
                UserDefaults.standard.synchronize();
            }  else {
                UserDefaults.standard.setValue(nil, forKey: "currentUser");
                UserDefaults.standard.synchronize();
            }
        }
        
        get {
            if let dataUser = UserDefaults.standard.object(forKey: "currentUser") as? Data {
                return NSKeyedUnarchiver.unarchiveObject(with: dataUser) as? User
            } else {
                return nil;
            }
        }
    }
}

