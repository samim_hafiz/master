//
//  LanguageManager.swift
//  maktab
//
//  Created by SNSR on 11/9/17.
//  Copyright © 2017 Sepehr Technologies. All rights reserved.
//

import Foundation
enum Language: String {
    case dari = "dari"; case english = "english"; case pashto = "pashto";
}

enum LanguageCode: String {
    case dariCode = "fa-AF"; case englishCode = "en"; case pashtoCode = "ps-AF"
}

struct LanguageManager {
    
    static var currentLanguageCode: LanguageCode {
        get {
            if let languageCode = UserDefaults.standard.value(forKey: KeyStrings.currentLanguageCode) as? String {
                return LanguageCode(rawValue: languageCode)!;
            } else {
                return LanguageCode.dariCode;
            }
        }
    }
    
    
    static var currentLanguage: Language {
        get {
            if let language = UserDefaults.standard.value(forKey: KeyStrings.currentLanguage) as? String {
                return Language(rawValue: language)!;
            } else {
                return Language.dari;
            }
        }
        
        set {
            print("current language: ", currentLanguage.rawValue);
            
            switch currentLanguage {
            case .dari:
                UserDefaults.standard.setValue(LanguageCode.dariCode.rawValue, forKey: KeyStrings.currentLanguageCode);
                Locale.preferredLanguage = LanguageCode.dariCode.rawValue;
            case .english:
                UserDefaults.standard.setValue(Language.english.rawValue, forKey: KeyStrings.currentLanguageCode);
                Locale.preferredLanguage = LanguageCode.englishCode.rawValue;
            case .pashto:
                UserDefaults.standard.setValue(Language.english.rawValue, forKey: KeyStrings.currentLanguageCode);
                Locale.preferredLanguage = LanguageCode.pashtoCode.rawValue;
            }
            UserDefaults.standard.setValue(newValue.rawValue, forKey: KeyStrings.currentLanguage);
            UserDefaults.standard.synchronize();
        }
    }
}

