//
//  Extensions.swift
//  maktab
//
//  Created by SNSR on 11/9/17.
//  Copyright © 2017 Sepehr Technologies. All rights reserved.
//

import UIKit
import Kingfisher
import NVActivityIndicatorView

extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1);
    }
    
    convenience init(colorString: String) {
        let valueArray = colorString.components(separatedBy: ":");
        
        if (valueArray.count < 3) {
            self.init(r: 181/255, g: 181/255, b: 181/255);
        } else {
            self.init(red: CGFloat(Float(valueArray[0])!/255.0), green: CGFloat(Float(valueArray[1])!/255.0), blue: CGFloat(Float(valueArray[2])!/255.0), alpha: 1);
        }
    }
}

extension Locale {
    static var preferredLanguage: String {
        get {
            return self.preferredLanguages.first ?? LanguageManager.currentLanguageCode.rawValue;
        }
        set {
            UserDefaults.standard.set([newValue], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
        }
    }
}

extension String {
    var localized: String {
        
        var result: String
        
        let languageCode = Locale.preferredLanguage;
        
        var path = Bundle.main.path(forResource: languageCode, ofType: "lproj");
        if path == nil, let index = languageCode.index(of: "-") {
            let languageCodeShort =  String(languageCode[..<index]);
            path = Bundle.main.path(forResource: languageCodeShort, ofType: "lproj");
        }
        
        if let path = path, let locBundle = Bundle(path: path) {
            result = locBundle.localizedString(forKey: self, value: nil, table: nil);
        } else {
            result = NSLocalizedString(self, comment: "");
        }
        
        return result
    }
}


extension UIImageView {
    func setImageFromUrl(urlStr: String) {
        if let url = URL(string: API.imageUrl(name: urlStr)) {
            //print("king_fisher_url: \(url)")
            self.kf.setImage(with: url)
        }
    }
}

extension Dictionary {
    func toJSONString() -> String {
        guard   let data = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted),
            let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            else {
                return "";
        }
        
        return jsonString as String;
    }
}

extension UIImage {
    func circularImage(size: CGSize?) -> UIImage {
        let newSize = size ?? self.size
        
        let minEdge = min(newSize.height, newSize.width)
        let size = CGSize(width: minEdge, height: minEdge)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        
        self.draw(in: CGRect(origin: CGPoint.zero, size: size), blendMode: .copy, alpha: 1.0)
        
        context!.setBlendMode(.copy)
        context!.setFillColor(UIColor.clear.cgColor)
        
        let rectPath = UIBezierPath(rect: CGRect(origin: CGPoint.zero, size: size))
        let circlePath = UIBezierPath(ovalIn: CGRect(origin: CGPoint.zero, size: size))
        rectPath.append(circlePath)
        rectPath.usesEvenOddFillRule = true
        rectPath.fill()
        
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return result!
    }
    
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / self.size.width
        let heightRatio = targetSize.height / self.size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func imageWithImage(scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

var theView: UIView?
extension UIView {
    func roundCorners(_ corners:UIRectCorner,_ cormerMask:CACornerMask, radius: CGFloat) {
        if #available(iOS 11.0, *){
            self.clipsToBounds = false
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = cormerMask
        }else{
            let rectShape = CAShapeLayer()
            rectShape.bounds = self.frame
            rectShape.position = self.center
            rectShape.path = UIBezierPath(roundedRect: self.bounds,    byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
            self.layer.mask = rectShape
        }
    }
    
    func hideKeyboardOnTap(for_ view: UIView? = nil) {
        if (view == nil){
            theView = self;
        } else {
            theView = view;
        }
        let tap = UITapGestureRecognizer(target: theView, action: #selector(viewTapped));
        tap.cancelsTouchesInView = false;
        theView?.addGestureRecognizer(tap);
        theView?.isUserInteractionEnabled = true;
    }
    
    @objc private func viewTapped(){
        theView!.endEditing(true);
    }
    
    var activityIndicatorTag: Int { return 9427342 }
    
    func startActivityIndicator(type: Int = 5) {
        
        DispatchQueue.main.async {
            
            if self.subviews.filter({$0.tag == self.activityIndicatorTag}).first != nil {
                //print("Already exists");
                return;
            }
            
            let frame = CGRect(x: (self.frame.size.width-50)/2, y: self.frame.size.height/2.5, width: 50, height: 50);
            let activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                                type: NVActivityIndicatorType(rawValue: type)!);
            activityIndicatorView.tag = self.activityIndicatorTag;
            //            activityIndicatorView.backgroundColor = .clear;
            activityIndicatorView.color = Style.mainColor;
            //            activityIndicatorView.padding = 20;
            self.addSubview(activityIndicatorView);
            
            activityIndicatorView.startAnimating();
        }
    }
    
    func stopActivityIndicator() {
        DispatchQueue.main.async {
            if let activityIndicator = self.subviews.filter({$0.tag == self.activityIndicatorTag}).first as? NVActivityIndicatorView {
                activityIndicator.stopAnimating();
                activityIndicator.removeFromSuperview();
            }
        }
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension Double {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}

extension Float {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}


extension BinaryInteger {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}

extension Date {
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
        
    }
}
extension Float {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places: Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


extension Date{
    func fromMiliSecondToDate(_ milisecond:Double)->Date{
        return Date(timeIntervalSince1970: TimeInterval(milisecond)/1000)
    }
    
    func getDateDay()->Int{
        let calender = Calendar.init(identifier: Calendar.Identifier.gregorian)
        let component = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: self)
        return component.day!
    }
    func getDateMonth()->Int{
        let calender = Calendar.init(identifier: Calendar.Identifier.gregorian)
        let component = calender.dateComponents([.year, .month, .day, .hour, .minute,.second], from: self)
        return component.month!
    }
    func getDateYer()->Int{
        let calender = Calendar.init(identifier: Calendar.Identifier.gregorian)
        let component = calender.dateComponents([.year, .month, .day, .hour, .minute,.second], from: self)
        return component.year!
    }
    
    func getDateLabel(_ date:Date)->String{
        let calender = Calendar.init(identifier: Calendar.Identifier.gregorian)
        let component = calender.dateComponents([.year, .month, .day, .hour, .minute,.second], from: date)
        let monthSymbol = Calendar.current.shortMonthSymbols[component.month! - 1]
        return "\(monthSymbol) \(component.day!),\(component.year!)"
    }
    
    func getHourLabel(_ date:Date) -> String{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a "
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        
        let currentDateStr = formatter.string(from: date)
        return currentDateStr
    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController();
    }
}

extension UIViewController {
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            if let tabbar = self as? TabBarViewController {
                return tabbar.viewControllers![tabbar.selectedIndex];
            }
            return self
        }
        
        if let navigation = self.presentedViewController as? UINavigationController {
            if let visibleController = navigation.visibleViewController {
                return visibleController.topMostViewController()
            }
        }
        
        if let tab = self.presentedViewController as? UITabBarController {
            print("Selected index: ", tab.selectedIndex);
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
}

