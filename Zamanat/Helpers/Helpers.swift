//
//  Helpers.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 3/27/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation
import Alamofire
import CoreTelephony
import CoreLocation
import Kingfisher

class Helpers {
    
    static func getPhoneCode(countryCode: String) -> String? {
        let countryDictionary = ["AF":"93", "AL":"355", "DZ":"213","AS":"1", "AD":"376", "AO":"244", "AI":"1","AG":"1","AR":"54","AM":"374","AW":"297","AU":"61","AT":"43","AZ":"994","BS":"1","BH":"973","BD":"880","BB":"1","BY":"375","BE":"32","BZ":"501","BJ":"229","BM":"1","BT":"975","BA":"387","BW":"267","BR":"55","IO":"246","BG":"359","BF":"226","BI":"257","KH":"855","CM":"237","CA":"1","CV":"238","KY":"345","CF":"236","TD":"235","CL":"56","CN":"86","CX":"61","CO":"57","KM":"269","CG":"242","CK":"682","CR":"506","HR":"385","CU":"53","CY":"537","CZ":"420","DK":"45","DJ":"253","DM":"1","DO":"1","EC":"593","EG":"20","SV":"503","GQ":"240","ER":"291","EE":"372","ET":"251","FO":"298","FJ":"679","FI":"358","FR":"33","GF":"594","PF":"689","GA":"241","GM":"220","GE":"995","DE":"49","GH":"233","GI":"350","GR":"30","GL":"299","GD":"1","GP":"590","GU":"1","GT":"502","GN":"224","GW":"245","GY":"595","HT":"509","HN":"504","HU":"36","IS":"354","IN":"91","ID":"62","IQ":"964","IE":"353","IL":"972","IT":"39","JM":"1","JP":"81","JO":"962","KZ":"77","KE":"254","KI":"686","KW":"965","KG":"996","LV":"371","LB":"961","LS":"266","LR":"231","LI":"423","LT":"370","LU":"352","MG":"261","MW":"265","MY":"60","MV":"960","ML":"223","MT":"356","MH":"692","MQ":"596","MR":"222","MU":"230","YT":"262","MX":"52","MC":"377","MN":"976","ME":"382","MS":"1","MA":"212","MM":"95","NA":"264","NR":"674","NP":"977","NL":"31","AN":"599","NC":"687","NZ":"64","NI":"505","NE":"227","NG":"234","NU":"683","NF":"672","MP":"1","NO":"47","OM":"968","PK":"92","PW":"680","PA":"507","PG":"675","PY":"595","PE":"51","PH":"63","PL":"48","PT":"351","PR":"1","QA":"974","RO":"40","RW":"250","WS":"685","SM":"378","SA":"966","SN":"221","RS":"381","SC":"248","SL":"232","SG":"65","SK":"421","SI":"386","SB":"677","ZA":"27","GS":"500","ES":"34","LK":"94","SD":"249","SR":"597","SZ":"268","SE":"46","CH":"41","TJ":"992","TH":"66","TG":"228","TK":"690","TO":"676","TT":"1","TN":"216","TR":"90","TM":"993","TC":"1","TV":"688","UG":"256","UA":"380","AE":"971","GB":"44","US":"1", "UY":"598","UZ":"998", "VU":"678", "WF":"681","YE":"967","ZM":"260","ZW":"263","BO":"591","BN":"673","CC":"61","CD":"243","CI":"225","FK":"500","GG":"44","VA":"379","HK":"852","IR":"98","IM":"44","JE":"44","KP":"850","KR":"82","LA":"856","LY":"218","MO":"853","MK":"389","FM":"691","MD":"373","MZ":"258","PS":"970","PN":"872","RE":"262","RU":"7","BL":"590","SH":"290","KN":"1","LC":"1","MF":"590","PM":"508","VC":"1","ST":"239","SO":"252","SJ":"47","SY":"963","TW":"886","TZ":"255","TL":"670","VE":"58","VN":"84","VG":"284","VI":"340"]
        if let code = countryDictionary[countryCode.uppercased()] {
            return "+" + code
        } else {
            return nil
        }
    }
    
    private static let countryDictionary = ["AF":"+93", "AL":"+355", "DZ":"+213","AS":"+1", "AD":"+376", "AO":"+244", "AI":"+1","AG":"+1","AR":"+54","AM":"+374","AW":"+297","AU":"+61","AT":"+43","AZ":"+994","BS":"+1","BH":"+973","BD":"+880","BB":"+1","BY":"+375","BE":"+32","BZ":"+501","BJ":"+229","BM":"+1","BT":"+975","BA":"+387","BW":"+267","BR":"+55","IO":"+246","BG":"+359","BF":"+226","BI":"+257","KH":"+855","CM":"+237","CA":"+1","CV":"+238","KY":"+345","CF":"+236","TD":"+235","CL":"+56","CN":"+86","CX":"+61","CO":"+57","KM":"+269","CG":"+242","CK":"+682","CR":"+506","HR":"+385","CU":"+53","CY":"+537","CZ":"+420","DK":"+45","DJ":"+253","DM":"+1","DO":"+1","EC":"+593","EG":"+20","SV":"+503","GQ":"+240","ER":"+291","EE":"+372","ET":"+251","FO":"+298","FJ":"+679","FI":"+358","FR":"+33","GF":"+594","PF":"+689","GA":"+241","GM":"+220","GE":"+995","DE":"+49","GH":"+233","GI":"+350","GR":"+30","GL":"+299","GD":"+1","GP":"+590","GU":"+1","GT":"+502","GN":"+224","GW":"+245","GY":"+595","HT":"+509","HN":"+504","HU":"+36","IS":"+354","IN":"+91","ID":"+62","IQ":"+964","IE":"+353","IL":"+972","IT":"+39","JM":"+1","JP":"+81","JO":"+962","KZ":"+77","KE":"+254","KI":"+686","KW":"+965","KG":"+996","LV":"+371","LB":"+961","LS":"+266","LR":"+231","LI":"+423","LT":"+370","LU":"+352","MG":"+261","MW":"+265","MY":"+60","MV":"+960","ML":"+223","MT":"+356","MH":"+692","MQ":"+596","MR":"+222","MU":"+230","YT":"+262","MX":"+52","MC":"+377","MN":"+976","ME":"+382","MS":"+1","MA":"+212","MM":"+95","NA":"+264","NR":"+674","NP":"+977","NL":"+31","AN":"+599","NC":"+687","NZ":"+64","NI":"+505","NE":"+227","NG":"+234","NU":"+683","NF":"+672","MP":"+1","NO":"+47","OM":"+968","PK":"+92","PW":"+680","PA":"+507","PG":"+675","PY":"+595","PE":"+51","PH":"+63","PL":"+48","PT":"+351","PR":"+1","QA":"+974","RO":"+40","RW":"+250","WS":"+685","SM":"+378","SA":"+966","SN":"+221","RS":"+381","SC":"+248","SL":"+232","SG":"+65","SK":"+421","SI":"+386","SB":"+677","ZA":"+27","GS":"+500","ES":"+34","LK":"+94","SD":"+249","SR":"+597","SZ":"+268","SE":"+46","CH":"+41","TJ":"+992","TH":"+66","TG":"+228","TK":"+690","TO":"+676","TT":"+1","TN":"+216","TR":"+90","TM":"+993","TC":"+1","TV":"+688","UG":"+256","UA":"+380","AE":"+971","GB":"+44","US":"+1", "UY":"+598","UZ":"+998", "VU":"+678", "WF":"+681","YE":"+967","ZM":"+260","ZW":"+263","BO":"+591","BN":"+673","CC":"+61","CD":"+243","CI":"+225","FK":"+500","GG":"+44","VA":"+379","HK":"+852","IR":"+98","IM":"+44","JE":"+44","KP":"+850","KR":"+82","LA":"+856","LY":"+218","MO":"+853","MK":"+389","FM":"+691","MD":"+373","MZ":"+258","PS":"+970","PN":"+872","RE":"+262","RU":"+7","BL":"+590","SH":"+290","KN":"+1","LC":"+1","MF":"+590","PM":"+508","VC":"+1","ST":"+239","SO":"+252","SJ":"+47","SY":"+963","TW":"+886","TZ":"+255","TL":"+670","VE":"+58","VN":"+84","VG":"+284","VI":"+340"];
    
    static func getRegionFor(code: String) -> String {
        for (key, value) in countryDictionary {
            if (value == code){
                return key;
            }
        }
        return "";
    }
    class func getCountryCodes() -> [String] {
        print( (countryDictionary as NSDictionary).allValues as! [String]);
        let list =  Array(Set((countryDictionary as NSDictionary).allValues as! [String]));
        return list.sorted(by: { (string1, string2) -> Bool in
            string1 < string2;
        })
    }
    
    class func getCurrentCountryCode() -> String {
        print("Getting country code");
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        if let countryCode = carrier?.isoCountryCode, let code = getPhoneCode(countryCode: countryCode) {
            return code;
        } else {
            return "--";
        }
    }
    
    class func request(api: String, method: HTTPMethod, parameters: [String: Any] = [String: Any](), completion: @escaping ([String: Any]?) -> Void) {
        let headers = ["api_key": "jT2kjFQGLkR5HRFpO7oAafeDxJbM6GGl7dCXVHQ23s"];
        
        
        Alamofire.request(api, method: method, parameters: parameters, headers: headers).responseJSON { (responseJson) in
            //            print("HHHHHHHHHHH API: ", api);
            //            print("parameters: ", parameters);
                                    print("\n\n\n\n\n\n\nResponse: ", responseJson);
            completion(responseJson.result.value as? [String: Any]);
        }
    }
    
    class func getImageFromUrl(urlStr: String, scale: CGSize, completion: @escaping (UIImage?) -> Void) {
        if let url = URL(string: API.imageUrl(name: urlStr)) {
            
            KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                if image == nil {
                    completion(nil);
                } else {
                    completion(image!.circularImage(size: scale))
                }
            })
        }
    }
    
    class func requestWithArray(api: String, method: HTTPMethod, parameters: [String: Any] = [String: Any](), completion: @escaping ([String: Any]?) -> Void) {
        let headers = ["api_key": "jT2kjFQGLkR5HRFpO7oAafeDxJbM6GGl7dCXVHQ23s"];
        if let url = URL(string: api){
            var request = URLRequest(url: url)
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers;
            
            request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: []);
            Alamofire.request(request).responseJSON { (responseJson) in
                completion(responseJson.result.value as? [String: Any]);
            }
        }
    }
    
    class func getCoordinateDetails(coordinate: CLLocationCoordinate2D, completion: @escaping (_ title: String, _ snipet: String) -> Void) {
        
        let api = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?language=en&location=\(coordinate.latitude),\(coordinate.longitude)&radius=1&key=\(KeyStrings.googlePlacesKey)";
        
        //        print("API: ", api);
        Helpers.request(api: api, method: .get, completion: { (result) in
            guard let resultDic = result else { return }
            
            if let resultArr = resultDic["results"] as? Array<[String: Any]> {
                if resultArr.count > 0 {
                    var title = "";
                    var snipet = "";
                    if let title_ = resultArr[0]["name"] as? String {
                        title = title_;
                    }
                    if let snippet_ = resultArr[0]["vicinity"] as? String {
                        snipet = snippet_;
                    }
                    completion(title, snipet);
                }
            }
        })
    }
    
    class func getPlaceId(coordinate: CLLocationCoordinate2D, completion: @escaping (_ id: String) -> Void) {
        
        let api = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?language=en&location=\(coordinate.latitude),\(coordinate.longitude)&radius=1&key=\(KeyStrings.googlePlacesKey)";
        
        Helpers.request(api: api, method: .get, completion: { (result) in
            
            guard let resultDic = result else { return }
            
            if let resultArr = resultDic["results"] as? Array<[String: Any]> {
                if resultArr.count > 0 {
                    var placeId = "";
                    if let placeId_ = resultArr[0]["place_id"] as? String {
                        placeId = placeId_;
                    }
                    completion(placeId);
                }
            }
        })
    }
    
    class func parsePaymentDetails(trip: Trip) -> [String: Any] {
        
        print("Called")
        var user = [String: Any]();
        var data = [String: Any]();
        if let userId = trip.user?.id, let displayName = trip.user?.displayName {
            let phoneNumber = trip.user?.phone ?? "0799";
            let thumbNail = trip.user?.thumbUrl ?? "";
            user = ["display_name": displayName, "thumb_url": thumbNail, "_id": userId, "phone": phoneNumber];
            
        } else { print("Here it is"); return [:] };
        
        DispatchQueue.main.async {
            
        if let amount = trip.ride?.price, let currency = trip.ride?.currency, let id = trip.ride?.id, let tripId = trip.id, let userId = trip.user?.id, let toUserId = trip.ride?.driver?.id, let toAddress = trip.toAddress, let distance = trip.distance, let duration = trip.duration {
            data = ["amount": amount, "currency": currency, "_id": id, "trip_id": tripId, "user_id": userId, "to_user_id": toUserId, "to_addr": toAddress, "distance": distance.replacingOccurrences(of: "km", with: "").replacingOccurrences(of: "کیلو متر", with: "").replacingOccurrences(of: " ", with: ""), "duration": duration.replacingOccurrences(of: "min", with: "").replacingOccurrences(of: "دقیقه", with: "").replacingOccurrences(of: " ", with: ""), "user": user] as [String : Any];
            
            }
        }
        return data;
    }
    
    class func getTripCost(distance: Double) -> Double {
        var cost = 0.0;
        if (distance <= 6) {
            cost = 70;
        } else {
            cost = distance * 15;
        }
        return cost;
    }
    
    class func getPeople(dic: [String: Any]) -> [Person] {
        var people = [Person]();
        if(Values.currentUser == nil) {
            return [Person]();
        }
        if let users = dic["user"] as? Array<[String: Any]> {
            let friendRequests = dic["friend_request"] as! Array<[String: Any]>
            let friends = dic["friends"] as! Array<[String:Any]>
            
            for userDic in users {
                let person = Person(dic: userDic);
                for i in (friendRequests.count > friends.count ? 0..<friendRequests.count : 0..<friends.count) {
                    if i < friendRequests.count {
                        // check in friendRequest
                        if let senderId = friendRequests[i]["sender"] as? String, let receiverId = friendRequests[i]["receiver"] as? String, let currentUserId = Values.currentUser?.id {
                            if(person.id! == senderId || person.id! == receiverId) {
                                if(currentUserId == senderId){
                                    person.friendship = .requestSent;
                                } else {
                                    person.friendship = .requestReceived;
                                }
                            }
                        }
                    }
                    if i < friends.count {
                        //check in friends
                        if let friendId = friends[i]["user_id"] as? String {
                            if(person.id! == friendId) {
                                person.friendship = .friend;
                            }
                        }
                    }
                }
                people.append(person);
            }
        }
        return people;
    }
    
    class func updateFcmTocke(token: String) {
        guard let userId = Values.currentUser?.id else { return }
        let parameters = ["user_id": userId, "device_token": token, "device_id": UIDevice.current.identifierForVendor!.uuidString, "app_type": 1] as [String : Any];
        print("Sending FCM Token...");
        self.request(api: API.updateFcmToken, method: .post, parameters: parameters) { (result) in
            print("FCM Result: ", result);
            guard let result = result else { return }
            if let success = result["success"] as? Bool, success {
                Values.isFcmTokenSent = true;
                print("FCM Updated!");
            }
        }
    }
    
    class func isInternetAccessAvailable() -> Bool {
        if (Utilities.isInternetAvailable()){
            return true;
        } else {
            //                PopupMessages.showWarningMessage(message: "noInternetAccess".localized);
            return false;
        }
    }
    
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func logout() {
        Values.currentUser = nil;
        UserDefaults.standard.set(nil, forKey: "currentUser");
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.checkLogin();
        }
    }
}
