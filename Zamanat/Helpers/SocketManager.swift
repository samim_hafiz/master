//
//  SocketManager.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 5/8/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//
import SocketIO

class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager();
    var called = false;
    var socket: SocketIOClient!
    let manager = SocketManager(socketURL: URL(string: API.socketUrl)!, config: [.log(false), .forcePolling(true), .reconnects(false), .reconnectAttempts(0), .forceWebsockets(true)]);
    
    private override init() {
        socket = manager.defaultSocket;
        socket.on("reconnect") { (result, emitter) in
            print("Reconnecting data: ", result);
            if let userId = Values.currentUser?.id {
                SocketIOManager.sharedInstance.emitWelcome("welcome", ["user_id": userId]);
            }
        }
        
        socket.on("disconnect") { (result, _) in
            print("Discunnecting data: ", result);
            SocketIOManager.sharedInstance.called = false;
        }
    }
    func establishConnection() {
        print("************************** Connecting...");
        socket.connect();
    }
    func closeConnection(){
                socket.disconnect();
                print("************************** Disconnected!")
                self.called = false;
    }
    
    func emitWelcome(_ event: String, _ userData: [String: Any]) {
        if (self.socket.status != SocketIOStatus.connected) {
            self.called = false;
        }
        socket.on("connect") { (data, _) in
            if(!self.called) {
                print("Welcome called");
                print("***************************** Connected!")
                self.socket.emit(event, userData);
                self.called = true;
            }
        }
    }
    
    func emit(_ event: String, _ data: [String: Any]) {
        var data = data;
        data["language"] = LanguageManager.currentLanguage.rawValue;
        self.socket.emit(event, data);
    }
}
