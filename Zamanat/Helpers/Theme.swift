//
//  Helpers.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 2/4/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

enum Theme: String {
    case default_ = "default";
    
    static var currentTheme: Theme {
        get {
            if UserDefaults.standard.value(forKey: KeyStrings.currentTheme) != nil {
                return Theme(rawValue: UserDefaults.standard.value(forKey: KeyStrings.currentTheme) as! String)!;
            } else {
                return Theme.default_;
            }
        }
        set {
            Style.setupTheme(theme: newValue);
        }
    }
}

struct Style {
    //these files use static color of mainColor
    //More/wallet/SegmentControlView/extenstion
    //More/paymentMethod/PaymentMethodView/extenstion
    //More/history/HistoryTableView/extenstion
    // ======= Colors ============
    static var mainColor: UIColor!
    static var differColor: UIColor!
    static var grayFontColor: UIColor!
    static var grayColor: UIColor!
    static var lightGrayColor: UIColor!
     static var blueColor: UIColor!
    static var minBackground : UIColor!
    static var greenColor : UIColor!
    
    
    // ======= Fonts =============
    static var veryBigFont: UIFont!
     static var bigFont: UIFont!
    static var bigBoldFont: UIFont!
    static var normalFont: UIFont!
    static var normalBoldFont: UIFont!
    static var biggerFont: UIFont!
    static var smallFont: UIFont!
    static var smallBoldFont: UIFont!
    static var smallerFont: UIFont!
    static var miniFont: UIFont!
    
    static func setupTheme(theme: Theme){
        UserDefaults.standard.setValue(theme.rawValue, forKey: KeyStrings.currentTheme);
        
        switch theme {
        case .default_:
            setupDefaultTheme();
        }
    }
    
    private static func setupDefaultTheme(){
        // -------- Colors ----------------
        mainColor = UIColor(r: 31, g: 71, b: 98);
        blueColor = UIColor(r: 10, g: 125, b: 251);
        differColor = UIColor(r: 245, g: 186, b: 52);
        minBackground = UIColor(r: 250, g: 250, b: 250)
        greenColor = UIColor(r: 1, g: 182, b: 1)
        grayFontColor = UIColor.gray;
        grayColor = UIColor.lightGray;
        lightGrayColor = UIColor(r: 190, g: 190, b: 190)
        // -------- Fonts ----------------
        veryBigFont = UIFont(name: "Aller-Regular", size: 26);
        bigFont = UIFont(name: "Aller-Regular", size: 18);
        bigBoldFont = UIFont(name: "Aller-Bold", size: 18);
        smallFont = UIFont(name: "Aller-Regular", size: 11);
        smallBoldFont = UIFont(name: "Aller-Bold", size: 11);
        smallerFont = UIFont(name: "Aller-Regular", size: 8);
        normalFont = UIFont(name: "Aller-Regular", size: 14);
        miniFont = UIFont(name: "Aller-Regular", size: 12)
        biggerFont = UIFont(name: "Aller-Regular", size: 17);
        normalBoldFont = UIFont(name: "Aller-Bold", size: 15);
        
        
//        messageTitlesFont = UIFont(name: "Aller-Bold", size: Diems.screenWidth/30);
//        biggerTitlesFont = UIFont(name: "Aller-Regular", size: Diems.screenWidth/22);
//        veryBigNormalFont = UIFont(name: "Aller-Light", size: Diems.screenWidth/14);
    }
}


