//
//  Enums.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 7/22/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation

enum NotificationType: String {
    case driver_response = "b"; // ride accepted
    case req_pay_for_ride = "g"; // Driver asked for ride payment
    case pay_for_ride_wallet = "d"; // User emits to pay-for-ride and specifies the type (cash or walled, and user receives a response to this notification type
    case pay_for_ride_cash = "c" // User emits to pay-for-ride and specifies the type (cash or walled, and user receives a response to this notification type
    case received_cash = "e"; // Driver received cash successfully
    case drop_off = "f"; // User reached to his destination
    case cancel_ride_by_driver = "i"; // Ride cancelled by the driver
    case trip_update_status = "m" // cancell or complete the trip
    case user_ignored_ride_payment = "h";
//    case location_sharing_complete_or_cancel = "m";
    case create_trip_or_track = "l";
    case other;
}
