//
//  BlockingTableViewController.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 6/12/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

private var blockingCellId:String = "blocking CellId"
class BlockingTableViewController: UITableViewController,UnBlockedUserDeleget {
    
    

    var blockedUserList:[User] = []{
        didSet{
            if self.blockedUserList.count > 0{
                self.blockeMessageLabel.isHidden = true
                self.tableView.separatorColor = .lightGray
            }else{
                self.blockeMessageLabel.isHidden = false
                self.tableView.separatorColor = .clear
            }
            self.tableView.reloadData()
        }
    }
    
    var refresher:UIRefreshControl!
    let blockeMessageLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "NoBlockedUser".localized
        lbl.font = Style.normalFont
        lbl.textAlignment = .center
        lbl.textColor = UIColor.darkGray
        lbl.isHidden = false
        return lbl
    }()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(BlockingTableCell.self, forCellReuseIdentifier: blockingCellId)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorColor = .clear
        setupViews()
        self.loadBlockedUser()
    }
    
    func setupViews(){
        self.navigationController?.navigationBar.barTintColor = Style.mainColor
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.title = "BlockedUsers".localized
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = Style.mainColor
        self.refresher.addTarget(self, action: #selector(self.refreshing), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refresher)
        
        view.addSubview(blockeMessageLabel)
        blockeMessageLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        blockeMessageLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        
    }
    @objc func refreshing(){
        self.loadBlockedUser()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return blockedUserList.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: blockingCellId, for: indexPath) as? BlockingTableCell
        cell?.user = blockedUserList[indexPath.row]
        cell?.delegation = self
        cell?.selectionStyle = .none
        return cell!
    }

    
    func loadBlockedUser(){
        if !self.refresher.isRefreshing{
            self.view.startActivityIndicator()
        }
        self.blockedUserList = []
        Helpers.request(api: API.getAllBlockedUsers(userId: (Values.currentUser?.id)!), method: .get) { (resultDect) in
            
            self.view.stopActivityIndicator()
            self.refresher.endRefreshing()
            guard let result:[String : Any] = resultDect else { return }
            if let data:[String : Any] = result["data"] as? [String : Any]{
                if let blockedUsers:Array<[String : Any]> = data["block"] as? Array<[String : Any]>{
                    
                    var blockedList:[User] = []
                    for blockUser in blockedUsers{
                        blockedList.append(User(dic: (blockUser["user_id"] as? [String : Any])!))
                    }
                    self.blockedUserList = blockedList
                }
            }
        }
    }
    
    func onUserOnblocked(userId:String) {
        for i in 0..<blockedUserList.count{
            if blockedUserList[i].id == userId{
                self.blockedUserList.remove(at: i)
            }
        }
    }

}


protocol UnBlockedUserDeleget {
    func onUserOnblocked(userId:String)
}

class BlockingTableCell:UITableViewCell{
    
    var user:User!{
        didSet{
            self.nameLabel.text = user.displayName!
            self.phoneLabel.text = user.phone!
            if self.user.thumbUrl != nil{
                self.profileImageView.setImageFromUrl(urlStr: self.user.thumbUrl!)
            }
        }
    }
    
    var delegation:UnBlockedUserDeleget!
    let profileImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 30
        imageView.layer.masksToBounds = true
        imageView.image = #imageLiteral(resourceName: "roundedProfile")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    
    let nameLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = Style.normalFont
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    let phoneLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = Style.normalFont
        lbl.textColor = UIColor.gray
        return lbl
    }()
    
    lazy var unBlockBtn:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = Style.mainColor
        btn.titleLabel?.textColor = UIColor.white
        btn.titleLabel?.font = Style.normalFont
        btn.layer.cornerRadius = 4
        btn.addTarget(self, action: #selector(unBlockUser), for: .touchUpInside)
        btn.setTitle("Unblock".localized.uppercased(), for: .normal)
        return btn
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
        
    }
    func setupViews(){
        addSubview(profileImageView)
        addSubview(nameLabel)
        addSubview(unBlockBtn)
        addSubview(phoneLabel)
        
        profileImageView.topAnchor.constraint(equalTo: topAnchor,constant:15).isActive = true
        profileImageView.leftAnchor.constraint(equalTo: leftAnchor,constant:15).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        nameLabel.topAnchor.constraint(equalTo: profileImageView.topAnchor,constant:12).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor,constant:15).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: unBlockBtn.leadingAnchor,constant:-15).isActive = true
        phoneLabel.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor).isActive = true
        phoneLabel.trailingAnchor.constraint(equalTo: nameLabel.trailingAnchor).isActive = true
        phoneLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor,constant:10).isActive = true
        
        
        unBlockBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant:-15).isActive = true
        unBlockBtn.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        unBlockBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        unBlockBtn.widthAnchor.constraint(equalToConstant: 80).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    @objc func unBlockUser(){
        guard let userid = Values.currentUser?.id else { return }
        let parameters:[String : Any] = ["user_id": userid,"block_user_id" : user.id!,"option" : "0"];

        Helpers.request(api: API.blockUnblockUser, method: .post, parameters: parameters) { (resutlDict) in
            guard let result:[String : Any] = resutlDict else { return }
            if (result["success"] as? Bool)!{
                self.delegation.onUserOnblocked(userId:self.user.id!)
            }
        }
    }
}
