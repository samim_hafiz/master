//
//  ProfileViewController.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 6/10/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import Alamofire

class ProfileViewController: UIViewController,UITextFieldDelegate{

    lazy var imagePicker: UIImagePickerController = {
        let picker = UIImagePickerController();
        picker.delegate = self;
        return picker;
    }()
    var profileThumb:String? = Values.currentUser?.thumbUrl
    var image: UIImage!
    var isProfileUpdated:Bool = false
    let backgroundView:UIView = {
        let view = UIView()
        view.backgroundColor = Style.mainColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let infoContainer:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 8
        return view
    }()
    
    let profileImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 45
        imageView.layer.masksToBounds = true
        imageView.image = #imageLiteral(resourceName: "roundedProfile")
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.layer.borderWidth = 0.6
        return imageView
    }()
    
    let fnameContainer:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Style.lightGrayColor
        return view
    }()
    let lnameContainer:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Style.lightGrayColor
        return view
    }()
    let updateBtn:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.titleLabel?.textColor = UIColor.white
        btn.setTitle("Update".localized.uppercased(), for: .normal)
        btn.setTitle("Update".localized.uppercased(), for: .disabled)
        btn.backgroundColor = Style.mainColor
        btn.addTarget(self, action: #selector(updateInfo), for: .touchUpInside)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitleColor(UIColor.gray, for: .disabled)
        btn.isEnabled = true
        return btn
    }()
    
    let fnameTextField:UITextField = {
        let field = UITextField()
        field.backgroundColor = UIColor.clear
        field.translatesAutoresizingMaskIntoConstraints = false
        field.placeholder = "FirstName".localized
        field.returnKeyType = .next
        return field
    }()
    
    let lnameTextField:UITextField = {
        let field = UITextField()
        field.backgroundColor = UIColor.clear
        field.translatesAutoresizingMaskIntoConstraints = false
        field.placeholder = "LastName".localized
        field.returnKeyType = .done
        return field
    }()
    
    let changProfileBtn:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = Style.mainColor
        btn.layer.cornerRadius = 15
        btn.layer.masksToBounds = true
        btn.contentMode = .scaleAspectFill
        btn.setImage(#imageLiteral(resourceName: "gallery"), for: .normal)
        btn.addTarget(self, action: #selector(chooseImageFromGallery), for: .touchUpInside)
        btn.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        return btn
    }()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        view.backgroundColor = Style.minBackground
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
       
    }
    
    func setupViews(){
        self.navigationController?.navigationBar.barTintColor = Style.mainColor
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.title = "Profile".localized
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        view.addSubview(backgroundView)
        view.addSubview(infoContainer)
        infoContainer.addSubview(profileImageView)
        infoContainer.addSubview(fnameContainer)
        infoContainer.addSubview(lnameContainer)
        infoContainer.addSubview(updateBtn)
        infoContainer.addSubview(changProfileBtn)
        
        fnameContainer.addSubview(fnameTextField)
        lnameContainer.addSubview(lnameTextField)
        
        fnameTextField.delegate = self
        lnameTextField.delegate = self
        
        setupConstraint()
    }
    
    func setupConstraint(){
        backgroundView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        backgroundView.heightAnchor.constraint(equalToConstant: 230).isActive = true
        
        infoContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        infoContainer.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-40).isActive = true
        infoContainer.heightAnchor.constraint(equalTo: backgroundView.heightAnchor).isActive = true
        infoContainer.centerYAnchor.constraint(equalTo: backgroundView.bottomAnchor).isActive = true
        
        profileImageView.centerXAnchor.constraint(equalTo: infoContainer.centerXAnchor).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: infoContainer.topAnchor).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        profileImageView.widthAnchor.constraint(equalTo: profileImageView.heightAnchor).isActive = true
        
        fnameContainer.widthAnchor.constraint(equalTo: infoContainer.widthAnchor,constant:-30).isActive = true
        fnameContainer.centerXAnchor.constraint(equalTo: infoContainer.centerXAnchor).isActive = true
        fnameContainer.heightAnchor.constraint(equalToConstant: 45).isActive = true
        fnameContainer.topAnchor.constraint(equalTo: profileImageView.bottomAnchor,constant:20).isActive = true
        
        lnameContainer.widthAnchor.constraint(equalTo: infoContainer.widthAnchor,constant:-30).isActive = true
        lnameContainer.centerXAnchor.constraint(equalTo: infoContainer.centerXAnchor).isActive = true
        lnameContainer.heightAnchor.constraint(equalToConstant: 45).isActive = true
        lnameContainer.topAnchor.constraint(equalTo: fnameContainer.bottomAnchor,constant:8).isActive = true
        
        updateBtn.widthAnchor.constraint(equalTo: infoContainer.widthAnchor,constant:-30).isActive = true
        updateBtn.centerXAnchor.constraint(equalTo: infoContainer.centerXAnchor).isActive = true
        updateBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        updateBtn.topAnchor.constraint(equalTo: lnameContainer.bottomAnchor,constant:8).isActive = true
        
        
        fnameTextField.topAnchor.constraint(equalTo: fnameContainer.topAnchor).isActive = true
        fnameTextField.leftAnchor.constraint(equalTo: fnameContainer.leftAnchor,constant:8).isActive = true
        fnameTextField.rightAnchor.constraint(equalTo: fnameContainer.rightAnchor,constant:-8).isActive = true
        fnameTextField.bottomAnchor.constraint(equalTo: fnameContainer.bottomAnchor).isActive = true
        
        lnameTextField.topAnchor.constraint(equalTo: lnameContainer.topAnchor).isActive = true
        lnameTextField.leftAnchor.constraint(equalTo: lnameContainer.leftAnchor,constant:8).isActive = true
        lnameTextField.rightAnchor.constraint(equalTo: lnameContainer.rightAnchor,constant:-8).isActive = true
        lnameTextField.bottomAnchor.constraint(equalTo: lnameContainer.bottomAnchor).isActive = true
        
        changProfileBtn.centerYAnchor.constraint(equalTo: profileImageView.bottomAnchor,constant:-25).isActive = true
        changProfileBtn.centerXAnchor.constraint(equalTo: profileImageView.rightAnchor,constant:-10).isActive = true
        changProfileBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        changProfileBtn.widthAnchor.constraint(equalTo: changProfileBtn.heightAnchor).isActive = true
        
        getProfileInfo()
    }
    
    func getProfileInfo(){
        fnameTextField.text = Values.currentUser?.name
        lnameTextField.text = Values.currentUser?.lastName
   

        if Values.currentUser?.thumbUrl != nil{
            profileImageView.setImageFromUrl(urlStr: (Values.currentUser?.thumbUrl)!)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fnameTextField{
            lnameTextField.becomeFirstResponder()
        }else{
            lnameTextField.resignFirstResponder()
        }
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        var fname = (fnameTextField.text)!
//        var lname = (lnameTextField.text)!
//        if textField == fnameTextField{
//            fname = fname + string
//        }else{
//            lname = lname + string
//        }
//
        return true
    }
    
}
extension ProfileViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    @objc func chooseImageFromGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true;
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // ------------------------ Image Picker Delegates ------------------
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {}
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
        image = originalImage
        } else {
            image = info[UIImagePickerControllerEditedImage] as? UIImage;
        }
        
        profileImageView.image = image;
        
        let imageData = UIImageJPEGRepresentation(image, 0.5);
        if let imageURL =  info[UIImagePickerControllerReferenceURL] as? URL {
            let fileName = imageURL.absoluteString
            uploadImage(imageData: imageData!, imageURL: imageURL, imageName: fileName, withCompresion: 0.6);
        }
    }
    
    private func uploadImage(imageData: Data, imageURL: URL, imageName: String, withCompresion: CGFloat){

        self.view.startActivityIndicator()
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "photo",fileName: "file.jpg", mimeType: "image/jpg")
        },to:API.uploadImage)
        { (result) in
            self.view.stopActivityIndicator()
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                   if let responseData:[String : Any] = response.result.value as? [String : Any]{
                    if let responseArray:Array<[String : Any]> = responseData["data"] as? Array<[String : Any]>{
                        print("response arrr: \(responseArray)")
                        self.profileThumb = responseArray[0]["filename"] as? String
                    }
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    
    @objc func updateInfo(){
        let parameters:[String : Any]!
        if profileThumb != nil{
            parameters = ["user_id" : (Values.currentUser?.id)!,"name": String(describing: (fnameTextField.text)!), "last_name" : String(describing: (lnameTextField.text)!), "display_name" : String(describing: (fnameTextField.text)!) + " " + String(describing: (lnameTextField.text)!),"thumb_url" : profileThumb!];
        }else{
            parameters = ["user_id" : (Values.currentUser?.id)!,"name": String(describing: (fnameTextField.text)!), "last_name" : String(describing: (lnameTextField.text)!), "display_name" : String(describing: (fnameTextField.text)!) + " " + String(describing: (lnameTextField.text)!)];
        }
        self.view.startActivityIndicator()
        Helpers.request(api: API.updateUserDetails, method: .put, parameters: parameters) { (resultDic) in
            self.view.stopActivityIndicator()
            guard let result:[String : Any] = resultDic else { return }
            if (result["success"] as? Bool)!{
                Values.currentUser = User(dic: result["data"] as! [String : Any])
//                self.setUpdateButtonEnibility(false)
            }
        }
    }
    
}
