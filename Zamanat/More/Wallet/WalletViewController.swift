//
//  WalletSegmentController.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 5/29/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {
    
    
    var wallet:Wallet!
    var walletHistoryInfoList:[WalletHistoryInfo] = []
    var cashHistoryInfoList:[WalletHistoryInfo] = []
    let segmentConrolContainer:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Style.mainColor
        return view
    }()
    let segmentedControl:UISegmentedControl = {
        let control = UISegmentedControl(items: ["Wallet".localized.uppercased(),"Cash".localized.uppercased()])
//        let titleTextAttributes = [NSAttributedStringKey.foregroundColor:Style.mainColor]
        let unselectedAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        control.setTitleTextAttributes(unselectedAttributes, for: .normal)
        control.translatesAutoresizingMaskIntoConstraints = false
        control.selectedSegmentIndex = 0
                control.backgroundColor = UIColor.clear
        control.addTarget(self, action: #selector(switchSegmentController), for: .valueChanged)
        control.tintColor = UIColor.white // Style.mainColor
        return control
    }()

    
    let walletTitleView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Style.mainColor
        return view
    }()
    
    let walletLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        lbl.font = Style.biggerFont
        lbl.text = "0.0"
        return lbl
    }()
    
    let currencyLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        lbl.font = Style.smallBoldFont
        lbl.text = "Afs"
        return lbl
    }()
    let walletControlView:SegmentControlView = {
        let view = SegmentControlView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    let cashControlView:SegmentControlView = {
        let view = SegmentControlView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    let reloadView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.isHidden = true
        return view
    }()
    
    let reloadText:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = Style.grayColor
        lbl.font = Style.normalFont
        lbl.text = "Something went wrong".localized
        return lbl
    }()
    
    let reloadButton:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Reload", for: .normal)
        btn.titleLabel?.font = Style.bigBoldFont
        btn.setTitleColor(Style.grayFontColor, for: .normal)
        btn.addTarget(self, action: #selector(reloadData), for: .touchUpInside)
        return btn
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.loadPaymentDetails()
        
    }
    
    func setupViews(){
        view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Style.mainColor
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.title = "Wallet".localized
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        
        view.addSubview(segmentConrolContainer)
        view.addSubview(walletControlView)
        view.addSubview(cashControlView)
        walletControlView.refreshDelegate = self
        cashControlView.refreshDelegate = self
        view.addSubview(reloadView)
        
        segmentConrolContainer.addSubview(walletTitleView)
        segmentConrolContainer.addSubview(segmentedControl)
        
        walletTitleView.addSubview(walletLabel)
        walletTitleView.addSubview(currencyLabel)
        
        reloadView.addSubview(reloadText)
        reloadView.addSubview(reloadButton)
        
        setupConstraint()
        switchSegmentController()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    func setupConstraint(){
        segmentConrolContainer.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        segmentConrolContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        segmentConrolContainer.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        segmentConrolContainer.heightAnchor.constraint(equalToConstant: 110).isActive = true
        
        walletTitleView.topAnchor.constraint(equalTo: segmentConrolContainer.topAnchor).isActive = true
        walletTitleView.leftAnchor.constraint(equalTo: segmentConrolContainer.leftAnchor).isActive = true
        walletTitleView.rightAnchor.constraint(equalTo: segmentConrolContainer.rightAnchor).isActive = true
        walletTitleView.bottomAnchor.constraint(equalTo: segmentedControl.topAnchor).isActive = true

        segmentedControl.leftAnchor.constraint(equalTo: segmentConrolContainer.leftAnchor).isActive = true
        segmentedControl.rightAnchor.constraint(equalTo: segmentConrolContainer.rightAnchor).isActive = true
        segmentedControl.heightAnchor.constraint(equalToConstant: 50).isActive = true
        segmentedControl.bottomAnchor.constraint(equalTo: segmentConrolContainer.bottomAnchor,constant:-2).isActive = true
        
        
        walletControlView.topAnchor.constraint(equalTo: segmentConrolContainer.bottomAnchor).isActive = true
        walletControlView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        walletControlView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        walletControlView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        cashControlView.topAnchor.constraint(equalTo: segmentConrolContainer.bottomAnchor).isActive = true
        cashControlView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        cashControlView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        cashControlView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        walletLabel.centerXAnchor.constraint(equalTo: walletTitleView.centerXAnchor).isActive = true
        walletLabel.centerYAnchor.constraint(equalTo: walletTitleView.centerYAnchor).isActive = true
        
        currencyLabel.rightAnchor.constraint(equalTo: walletLabel.leftAnchor).isActive = true
        currencyLabel.topAnchor.constraint(equalTo: walletLabel.topAnchor).isActive = true
        
        reloadView.topAnchor.constraint(equalTo: walletControlView.topAnchor).isActive = true
        reloadView.leftAnchor.constraint(equalTo: walletControlView.leftAnchor).isActive = true
        reloadView.bottomAnchor.constraint(equalTo: walletControlView.bottomAnchor).isActive = true
        reloadView.rightAnchor.constraint(equalTo: walletControlView.rightAnchor).isActive = true
        
        reloadButton.centerXAnchor.constraint(equalTo: reloadView.centerXAnchor).isActive = true
        reloadButton.centerYAnchor.constraint(equalTo: reloadView.centerYAnchor).isActive = true
        reloadText.centerXAnchor.constraint(equalTo: reloadView.centerXAnchor).isActive = true
        reloadText.bottomAnchor.constraint(equalTo: reloadButton.topAnchor,constant:-5).isActive = true
    }
    
    @objc func switchSegmentController(){
        if segmentedControl.selectedSegmentIndex == 0{
            walletControlView.isHidden = false
            cashControlView.isHidden = true
        }else{
            walletControlView.isHidden = true
            cashControlView.isHidden = false
        }
    }
    
    // loading details of payment from server
    private func loadPaymentDetails(){
        self.walletHistoryInfoList = []
        self.cashHistoryInfoList = []
        self.reloadView.isHidden = true
        if !self.walletControlView.refresher.isRefreshing && !self.cashControlView.refresher.isRefreshing{
            self.view.startActivityIndicator()
        }
        
        guard let userid = Values.currentUser?.id else { return }
        let parameters = ["user_id": userid];
        print("this is the userId: \(userid)")
        Helpers.request(api: API.getPaymentDetails, method: .post, parameters: parameters) { (resultDic) in
            self.view.stopActivityIndicator()
            if resultDic == nil{
                self.reloadView.isHidden = false
            }
            else{ self.reloadView.isHidden = true }
            
            //whem reloading or refreshing the segment control views
            self.walletControlView.refresher.endRefreshing()
            self.cashControlView.refresher.endRefreshing()
            
            guard let result:[String : Any] = resultDic else { return }
            let data:[String: Any] = (result["data"] as? [String : Any])!

            if let history = data["history"] as? Array<[String : Any]>, let wallets:[String: Any] = data["wallet"] as? [String : Any]{
                self.wallet = Wallet(dict: wallets)
                
                self.walletHistoryInfoList = WalletHistory().parseWalletHistory(history)
                self.cashHistoryInfoList = WalletHistory().parseCashHistory(history)
                self.walletControlView.walletInfoList = self.walletHistoryInfoList
                self.cashControlView.walletInfoList = self.cashHistoryInfoList
                
                self.walletLabel.text = String(describing: (self.wallet.amount?.rounded(toPlaces: 2))!)
                self.currencyLabel.text = self.wallet.currency
                
            }
        }
    }

}

extension WalletViewController:RefreshControlDelegation{
    func onRefreshed() {
        self.loadPaymentDetails()
    }
    
    @objc func reloadData(){
        self.loadPaymentDetails()
    }
}

