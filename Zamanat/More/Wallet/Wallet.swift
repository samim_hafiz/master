//
//  Wallet.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 5/30/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//
import Foundation
//struct WalletConfigs {
//    static let PAYMENT_TYPE_CASH:Int = 1
//    static let PAYMENT_TYPE_WALLET:Int = 2
//}
enum PaymentType:Int {
    case cash = 1, wallet = 2
}
class Wallet{
    var id:String?
    var amount:Double?
    var created:Double?
    var currency:String?
    var loan:Double?
    var updated: Double?
    var userId:String?
    
    init(dict:[String: Any]) {
        if let id = dict["_id"] as? String{ self.id = id}
        if let amount = dict["amount"] as? Double{ self.amount = amount}
        if let created = dict["created"] as? Double {self.created = created}
        if let currency = dict["currency"] as? String {self.currency = currency}
        if let updated = dict["updated"] as? Double {self.updated = updated}
        if let loan = dict["loan"] as? Double {self.loan = loan}
        if let userId = dict["user_id"] as? String {self.userId = userId}
    }
}


struct WalletHistoryInfo {
    var updated:Double?
    var historyDetails:[WalletHistoryDetails]?
}
struct WalletHistoryDetails {
    var id:String?
    var amount:Double?
    var created:Double?
    var currency:String?
    var paymentType: PaymentType?
    var refId:String?
    var refType:Int?
    var toUserId:String?
    var updated: Double?
    var userId:String?
    init(dict:[String: Any]) {
        if let id = dict["_id"] as? String{ self.id = id}
        if let amount = dict["amount"] as? Double{ self.amount = amount}
        if let created = dict["created"] as? Double {self.created = created}
        if let currency = dict["currency"] as? String {self.currency = currency}
        if let paymentType = dict["payment_type"] as? Int, paymentType != 0 {
            self.paymentType = PaymentType(rawValue: paymentType)!
            
        }
        if let refId = dict["ref_id"] as? String {self.refId = refId}
        if let refType = dict["ref_type"] as? Int {self.refType = refType}
        if let toUserId = dict["to_user_id"] as? String {self.toUserId = toUserId}
        if let updated = dict["updated"] as? Double {self.updated = updated}
        if let userId = dict["user_id"] as? String {self.userId = userId}
    }
}

class WalletHistory{
    var walletHistoryInfoList:[WalletHistoryInfo] = []
    var cashHistoryInfoList:[WalletHistoryInfo] = []
    func parseWalletHistory(_ historyData:Array<[String: Any]>)->[WalletHistoryInfo]{
        for history in historyData{
            let walletDetails = WalletHistoryDetails(dict: history)
            
            if walletDetails.paymentType == PaymentType.wallet{
                let listIndex:Int = checkForSameDate(walletDetails.created!,PaymentType.wallet.rawValue)
                if  listIndex != -1{
                    self.walletHistoryInfoList[listIndex].historyDetails?.append(walletDetails)
                }else{
                    var detailsList:[WalletHistoryDetails] = []
                    detailsList.append(walletDetails)
                    self.walletHistoryInfoList.append(WalletHistoryInfo(updated: walletDetails.created,  historyDetails: detailsList))
                }
            }
        }
        return self.walletHistoryInfoList
    }
    
    func parseCashHistory(_ historyData:Array<[String: Any]>)->[WalletHistoryInfo]{
        for history in historyData{
            let walletDetails = WalletHistoryDetails(dict: history)
            
            if walletDetails.paymentType == .cash{
                let listIndex:Int = checkForSameDate(walletDetails.created!,PaymentType.cash.rawValue)
                if  listIndex != -1{
                    self.cashHistoryInfoList[listIndex].historyDetails?.append(walletDetails)
                }else{
                    var detailsList:[WalletHistoryDetails] = []
                    detailsList.append(walletDetails)
                    self.cashHistoryInfoList.append(WalletHistoryInfo(updated: walletDetails.created, historyDetails: detailsList))
                }
            }
        }
        return self.cashHistoryInfoList
    }
    
    func checkForSameDate(_ milDate:Double,_ type:Int)->Int{
        let date:Date = Date().fromMiliSecondToDate(milDate)
        for i in 0..<(type == 1 ? self.cashHistoryInfoList.count : self.walletHistoryInfoList.count){
            let listDate : Date = type == 1 ? Date().fromMiliSecondToDate(self.cashHistoryInfoList[i].updated!) : Date().fromMiliSecondToDate(self.walletHistoryInfoList[i].updated!)
            if date.getDateDay() == listDate.getDateDay() && date.getDateMonth() == listDate.getDateMonth() && date.getDateYer() == listDate.getDateYer(){
                return i
            }
        }
        return -1
    }
}




