//
//  SegmentControlView.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 5/30/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
private var segmentTableCellId:String = "segmentCellId"
private var segmentHeaderId:String = "dateId"
protocol RefreshControlDelegation{
    func onRefreshed()
}
class SegmentControlView:UITableView,UITableViewDelegate,UITableViewDataSource{
    var walletInfoList:[WalletHistoryInfo]!{
        didSet{
            self.reloadData()
        }
    }
    var refreshDelegate:RefreshControlDelegation!
    var refresher:UIRefreshControl!
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        self.register(SegmentControlViewCell.self, forCellReuseIdentifier: segmentTableCellId)
        self.register(DateHeaderView.self, forHeaderFooterViewReuseIdentifier: segmentHeaderId);
        self.dataSource = self
        self.delegate = self
        
        setupRefresher()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.walletInfoList == nil ? 0 : self.walletInfoList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.walletInfoList[section].historyDetails?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: segmentTableCellId, for: indexPath) as? SegmentControlViewCell
        cell?.walletHistoryDetails = self.walletInfoList[indexPath.section].historyDetails![indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: segmentHeaderId) as! DateHeaderView;
        view.date = self.walletInfoList[section].updated
        return view;
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SegmentControlViewCell:UITableViewCell{
    
    var walletHistoryDetails:WalletHistoryDetails!{
        didSet{
            if walletHistoryDetails.paymentType == .cash{
                self.paymentLabel.text = "CashPayment".localized
            }else{
                self.paymentLabel.text = "WalletPayment".localized
            }
            let minusLbl = walletHistoryDetails.userId == Values.currentUser?.id ? "-" : ""
            self.amountLabel.text = "\(minusLbl)\(String(describing: walletHistoryDetails.currency!)) \(String(describing: (self.walletHistoryDetails.amount?.rounded(toPlaces: 2))!))"
            self.timeLabel.text = Date().getHourLabel(Date().fromMiliSecondToDate(self.walletHistoryDetails.updated!))
            
        }
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    func setupViews(){
        addSubview(paymentLabel)
        addSubview(timeLabel)
        addSubview(amountLabel)
        
        paymentLabel.topAnchor.constraint(equalTo: self.topAnchor,constant:10).isActive = true
        paymentLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant:10).isActive = true
        
        amountLabel.topAnchor.constraint(equalTo: paymentLabel.bottomAnchor,constant:-5).isActive = true
        amountLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant:-10).isActive = true
        
        timeLabel.topAnchor.constraint(equalTo: amountLabel.bottomAnchor,constant:-5).isActive = true
        timeLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant:10).isActive = true
    }
    
    let paymentLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = Style.normalFont
        return lbl
    }()
    
    let timeLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.gray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = Style.smallFont
        return lbl
    }()
    
    
    let amountLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = Style.mainColor
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = Style.smallBoldFont
        return lbl
    }()
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DateHeaderView: UITableViewHeaderFooterView {
    var date:Double!{
        didSet{
            self.headerTitleLabel.text = Date().getDateLabel(Date().fromMiliSecondToDate(date))
        }
    }
    let headerTitleLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalFont;
        label.textColor = UIColor.black.withAlphaComponent(0.7);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier);
        
        self.addSubview(headerTitleLabel);
        
        headerTitleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true;
        headerTitleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SegmentControlView{
    func setupRefresher(){
        //MARK: setting up the refresher
        self.refresher = UIRefreshControl()
        ////////kl;zxkcv
        self.refresher.tintColor = UIColor(r: 31, g: 71, b: 98)
        self.refresher.addTarget(self, action: #selector(self.refreshing), for: UIControlEvents.valueChanged)
        self.addSubview(refresher)
    }
    
    @objc func refreshing(){
        self.refreshDelegate.onRefreshed()
    }
}



