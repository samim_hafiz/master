//
//  InfoWebViewController.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 6/11/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class InfoWebViewController: UIViewController {
    var type: WebviewInfoType? {
        didSet {
            if let type = type?.rawValue {
                loadInfo(type: type);
                navigationItem.title = type == WebviewInfoType.about.rawValue ? "About".localized : "Fares".localized
            }
        }
    }
    let infoView:UIWebView = {
        let view = UIWebView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        return view
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        view.backgroundColor = Style.minBackground
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    
    func setupViews(){
        self.navigationController?.navigationBar.barTintColor = Style.mainColor
        navigationController?.navigationBar.tintColor = UIColor.white
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        view.addSubview(infoView)
        infoView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        infoView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        infoView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        infoView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
    
    func loadInfo(type: Int){
            let parameters = ["type": "\(type)"];
            self.view.startActivityIndicator()
            Helpers.request(api: API.staticData, method: .post, parameters: parameters) { (resultDic) in
                self.view.stopActivityIndicator()
                guard let result:[String : Any] = resultDic else { return }
                let data:String = (result["data"] as? String)!
                self.infoView.loadHTMLString(data, baseURL: nil)
            }
    }
}


enum WebviewInfoType:Int {
    case pricacyPolicy = 1, termsAndCondition = 2, fares = 4, about = 3;
}
