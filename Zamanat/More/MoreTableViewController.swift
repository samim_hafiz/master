//
//  MoreTableViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 5/12/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import YouTubePlayer

private let settingIdentifier = "cellId";
private let headerIdentifier = "header";

class MoreTableViewController: UITableViewController, YouTubePlayerDelegate {

    var settingItems = [SettingItem]();
    var settingsDetail2 = [SettingItem]();
    let videoPlayerController = UIViewController();
    let videoPlayer = YouTubePlayerView(frame: UIScreen.main.bounds);
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.tableView.backgroundColor = .white;
        self.tableView.tableFooterView = UIView();
        self.tableView.register(SettingCell.self, forCellReuseIdentifier: settingIdentifier);
        self.tableView.register(SettingHeaderView.self, forHeaderFooterViewReuseIdentifier: headerIdentifier);
        
        videoPlayer.backgroundColor = .black;
        videoPlayerController.view.backgroundColor = .black;
        
        loadSettings();
    }
    
    private func loadSettings() {
        settingItems.append(SettingItem(image: #imageLiteral(resourceName: "wallet"), title: "Wallet".localized, action: .wallet));
        settingItems.append(SettingItem(image: #imageLiteral(resourceName: "payements") , title: "PaymentMethod".localized, action: .paymentMethod));
        settingItems.append(SettingItem(image: #imageLiteral(resourceName: "history"), title: "History".localized, action: .history));
        settingItems.append(SettingItem(image: #imageLiteral(resourceName: "fares"), title: "Fares".localized, action: .fares));
        
        settingsDetail2.append(SettingItem(image: #imageLiteral(resourceName: "profile-1"), title: "Profile".localized, action: .profile));
        settingsDetail2.append(SettingItem(image: #imageLiteral(resourceName: "blocking"), title: "Blocking".localized, action: .blocking));
        settingsDetail2.append(SettingItem(image: #imageLiteral(resourceName: "tutorial_icon"), title: "Tutorial".localized, action: .tutorial));
        settingsDetail2.append(SettingItem(image: #imageLiteral(resourceName: "about"), title: "About".localized, action: .about));
        settingsDetail2.append(SettingItem(image: #imageLiteral(resourceName: "logout"), title: "Logout".localized, action: .logout));
    }
    
    func playerStateChanged(_ videoPlayer: YouTubePlayerView, playerState: YouTubePlayerState) {
        if(playerState == .Paused){
            print("Paused");
            videoPlayerController.dismiss(animated: true, completion: nil);
        }
    }
    
    func playerReady(_ videoPlayer: YouTubePlayerView) {
        print("Video is ready");
        videoPlayer.play();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.setNavigationBarHidden(true, animated: false);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        self.navigationController?.setNavigationBarHidden(false, animated: false);
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return settingItems.count;
        } else {
            return settingsDetail2.count;
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: settingIdentifier, for: indexPath) as! SettingCell;
        cell.settingItem = indexPath.section == 0 ? self.settingItems[indexPath.item] : self.settingsDetail2[indexPath.item];
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48;
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as! SettingHeaderView;
        view.headerTitleLabel.text = section == 0 ? "Details".localized.uppercased() : "Settings".localized.uppercased();
        return view;
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = indexPath.section == 0 ? settingItems[indexPath.row] : settingsDetail2[indexPath.row];
        
        switch item.action {
            
        case .wallet:
            let waletController = WalletViewController()
            self.navigationController?.pushViewController(waletController, animated: true);
            
        case .paymentMethod:
            let paymentController = PaymentMethodController()
            self.navigationController?.pushViewController(paymentController, animated: true);
            
        case .history:
            let historyController = HistoryTableViewController()
            self.navigationController?.pushViewController(historyController, animated: true);
            
        case .fares:
            let faresVc = InfoWebViewController()
            faresVc.type = WebviewInfoType.fares
            self.navigationController?.pushViewController(faresVc, animated: true);
            
        case .profile:
            let provileVc = ProfileViewController()
            self.navigationController?.pushViewController(provileVc, animated: true);
            
        case .blocking:
            let blockingVc = BlockingTableViewController()
            self.navigationController?.pushViewController(blockingVc, animated: true);
            
        case .tutorial:
            if !Helpers.isInternetAccessAvailable() {
                PopupMessages.showInfoMessage(message: "noInternetAccess".localized);
                return;
            }
            videoPlayer.loadVideoID("dfvNRofzskM"); 
            videoPlayerController.view = videoPlayer;
            self.navigationController?.present(videoPlayerController, animated: true, completion: nil);
            
        case .about:
            let aboutVc = InfoWebViewController()
            aboutVc.type = WebviewInfoType.about
            self.navigationController?.pushViewController(aboutVc, animated: true);
            
        case .logout:
            let alert = UIAlertController(title: "Logout".localized, message: "AreYouSureForLogout".localized, preferredStyle: .alert);
            let yesAction = UIAlertAction(title: "Yes".localized, style: .default, handler: { (_) in
                self.dismiss(animated: false, completion: nil);
                 Helpers.logout();
            });
            let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil);
            alert.addAction(yesAction);
            alert.addAction(cancelAction);
            self.present(alert, animated: true, completion: nil);
           
        case .none:
            print(item.action);
        }
    }
}

class SettingHeaderView: UITableViewHeaderFooterView {
    let headerTitleLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalFont;
        label.textColor = UIColor.black.withAlphaComponent(0.7);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier);
        
        self.addSubview(headerTitleLabel);
        
        headerTitleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true;
        headerTitleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16).isActive = true;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
