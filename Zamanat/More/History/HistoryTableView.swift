//
//  HistoryTableView.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 6/6/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
private var rideHistoryCellID:String = "ridehistoryCellId"
private var tripHistoryCellID:String = "triphistoryCellId"

//protocol RefreshControlDelegation{
//    func onRefreshed()
//}
class HistoryTableView:UITableView,UITableViewDelegate,UITableViewDataSource{
    var refreshDelegate:RefreshControlDelegation!
    var refresher:UIRefreshControl!
    var historyList:[RideHistory] = []{
        didSet{
            self .reloadData()
        }
    }
    override init(frame: CGRect, style: UITableViewStyle) {
        
        super.init(frame: frame, style: style)
        self.register(RideHistoryCell2.self, forCellReuseIdentifier: rideHistoryCellID)
        self.register(TripHistoryCell.self, forCellReuseIdentifier: tripHistoryCellID)
        self.dataSource = self
        self.delegate = self
        
        
        
        setupRefresher()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.historyList[indexPath.row].type == TripType.ride.rawValue{
            let cell = tableView.dequeueReusableCell(withIdentifier: rideHistoryCellID, for: indexPath) as? RideHistoryCell2
            cell?.rideHistory = self.historyList[indexPath.row]
            return cell!
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: tripHistoryCellID, for: indexPath) as? TripHistoryCell
        cell?.rideHistory = self.historyList[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.historyList[indexPath.row].type == TripType.ride.rawValue{
            return 260
        }else{
            return 220
        }
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



extension HistoryTableView{
    func setupRefresher(){
        //MARK: setting up the refresher
        self.refresher = UIRefreshControl()
        self.refresher = UIRefreshControl()
        //skdafsadflljnalsksdfj
        self.refresher.tintColor = UIColor(r: 31, g: 71, b: 98)
        self.refresher.addTarget(self, action: #selector(self.refreshing), for: UIControlEvents.valueChanged)
        self.addSubview(refresher)
    }
    
    @objc func refreshing(){
        self.refreshDelegate.onRefreshed()
    }
}
