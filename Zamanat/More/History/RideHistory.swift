//
//  RideHistory.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 6/5/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation
class RideHistory{
    var trip:Trip!
    var type:Int!
    var driver:Driver!
    var trackers:[Tracker] = []
    //type == 1 :  get trips
    //type == 2 : get ride
    init(type:Int,dict:[String : Any]) {
        self.type = type
        self.trip = Trip(dic: dict)
        if let driver:[String : Any] = dict["driver"] as? [String : Any]{
            self.driver = Driver(dic: driver)
        }
        
        if let trackers:Array<[String : Any]>  = dict["trackers"] as? Array<[String: Any]>{
            for tracker in trackers{
                self.trackers.append(Tracker(dic: tracker))
            }
        }
    }
}
