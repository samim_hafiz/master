//
//  RideHistoryCell2.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 6/6/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import Foundation
import UIKit

private let cellIdetifier = "cellId";
class RideHistoryCell2: UITableViewCell {
    var rideHistory:RideHistory!{
        didSet{
            self.driverProfileImageView.setImageFromUrl(urlStr: self.rideHistory.trip.ride?.driver?.thumbUrl == nil ? "" : (self.rideHistory.trip.ride?.driver?.thumbUrl)!)
            
            self.driverNameLabel.text = self.rideHistory.driver?.displayName
            self.driverPhoneLabel.text = self.rideHistory.driver?.phone
//            let cur:String = (rideHistory.trip.ride?.currency)!
            if rideHistory.trip.ride?.currency != nil && rideHistory.trip.ride?.price != nil{
                self.priceLabel.text = "\((rideHistory.trip.ride?.currency)!) \((rideHistory.trip.ride?.price)!)"
            }
            
            self.trackingTitleLabel.text = "\("You have been Tracked by".localized.uppercased()) \(self.rideHistory.trackers.count) \("People".localized.uppercased())"
            
            self.trackers = self.rideHistory.trackers
            
            self.fromAddressLabel.text = self.rideHistory.trip.fromAddress
            self.toAddressLabel.text = self.rideHistory.trip.toAddress
            
            self.durationValueLabel.text = self.rideHistory.trip.duration
            self.distanceValueLabel.text = self.rideHistory.trip.distance
            if self.rideHistory.trip.ride?.update != nil{
                let updateDate:Date = Date().fromMiliSecondToDate((self.rideHistory.trip.ride?.update)!)
                let dateLabelStr:String = Date().getDateLabel(updateDate)
                let timeLabel:String = Date().getHourLabel(updateDate)
                self.dateLabel.text = "\(dateLabelStr) \(timeLabel)"
            }
        }
    }
    
    var trackers:[Tracker] = []{
        didSet{
            self.trackingCollectionView.reloadData()
        }
    }
    let leftContent:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let rightContent:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let bottomContent:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalBoldFont;
        label.textColor = Style.mainColor;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverLabel: UILabel = {
        let label = UILabel();
        label.text = "Driver".localized.uppercased();
        label.font = Style.smallFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverProfileImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.contentMode = .scaleAspectFill;
        imageView.layer.borderColor = Style.mainColor.cgColor;
        imageView.layer.borderWidth = 1;
        imageView.layer.cornerRadius = 20;
        imageView.layer.masksToBounds = true;
        imageView.image  = #imageLiteral(resourceName: "roundedProfile");
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let driverNameLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallBoldFont;
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverPhoneLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallBoldFont;
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let trackingImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "tracking").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFill;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
//        imageView.image = #imageLiteral(resourceName: "defaultProfile")
        return imageView;
    }();
    
    let trackingTitleLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallFont;
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let fromLabel: UILabel = {
        let label = UILabel();
        label.text = "From".localized;
        label.font = Style.smallFont;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let fromAddressLabel: UILabel = {
        let label = UILabel();
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        label.font = Style.smallBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let toLabel: UILabel = {
        let label = UILabel();
        label.text = "To".localized;
        label.font = Style.smallFont;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let toAddressLabel: UILabel = {
        let label = UILabel();
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        label.font = Style.smallBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let durationLabel: UILabel = {
        let label = UILabel();
        label.text = "Duration".localized;
        label.font = Style.smallFont;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let durationValueLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceLabel: UILabel = {
        let label = UILabel();
        label.text = "Distance".localized;
        label.font = Style.smallFont;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceValueLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let dateLabel: UILabel = {
        let label = UILabel();
        label.text = "2018-01-13 5:40 PM";
        label.font = Style.smallFont;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var trackingCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout();
        layout.scrollDirection = .horizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout);
        collection.delegate = self;
        collection.dataSource = self;
        collection.backgroundColor = .white;
        collection.showsHorizontalScrollIndicator = false;
        collection.alwaysBounceHorizontal = true;
        collection.register(TrackerCell.self, forCellWithReuseIdentifier: cellIdetifier);
        collection.translatesAutoresizingMaskIntoConstraints = false;
        return collection;
    }();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(leftContent)
        addSubview(rightContent)
        addSubview(bottomContent)
        
        leftContent.addSubview(priceLabel);
        leftContent.addSubview(driverLabel);
        leftContent.addSubview(driverProfileImageView);
        leftContent.addSubview(driverNameLabel);
        leftContent.addSubview(driverPhoneLabel);
        leftContent.addSubview(trackingImageView);
        leftContent.addSubview(trackingTitleLabel);
        
        
        rightContent.addSubview(fromLabel);
        rightContent.addSubview(fromAddressLabel);
        rightContent.addSubview(toLabel);
        rightContent.addSubview(toAddressLabel);
        rightContent.addSubview(durationLabel);
        rightContent.addSubview(durationValueLabel);
        rightContent.addSubview(distanceLabel);
        rightContent.addSubview(distanceValueLabel);
        rightContent.addSubview(dateLabel);
        
        
        bottomContent.addSubview(trackingCollectionView);
        
        self.setupConstraint()
        
    }
    
    func setupConstraint(){
        leftContent.topAnchor.constraint(equalTo: topAnchor).isActive = true
        leftContent.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        leftContent.rightAnchor.constraint(equalTo: centerXAnchor).isActive = true
        leftContent.bottomAnchor.constraint(equalTo: bottomContent.topAnchor).isActive = true
        
        rightContent.topAnchor.constraint(equalTo: topAnchor).isActive = true
        rightContent.leftAnchor.constraint(equalTo: centerXAnchor,constant:10).isActive = true
        rightContent.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        rightContent.bottomAnchor.constraint(equalTo: bottomContent.topAnchor).isActive = true
        
        bottomContent.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomContent.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bottomContent.bottomAnchor.constraint(equalTo: bottomAnchor,constant:-10).isActive = true
        bottomContent.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        ///childes constraint
        //price label
        priceLabel.topAnchor.constraint(equalTo: leftContent.topAnchor, constant: 16).isActive = true
        priceLabel.leadingAnchor.constraint(equalTo: leftContent.leadingAnchor, constant: 16).isActive = true
        
        driverLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 15).isActive = true;
        driverLabel.leadingAnchor.constraint(equalTo: driverProfileImageView.trailingAnchor, constant: 16).isActive = true;
        
        driverProfileImageView.leadingAnchor.constraint(equalTo: priceLabel.leadingAnchor).isActive = true;
        driverProfileImageView.topAnchor.constraint(equalTo: driverLabel.bottomAnchor, constant: 10).isActive = true;
        driverProfileImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true;
        driverProfileImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true;
        
        driverNameLabel.leadingAnchor.constraint(equalTo: driverLabel.leadingAnchor).isActive = true;
        driverNameLabel.topAnchor.constraint(equalTo: driverProfileImageView.topAnchor, constant: 2).isActive = true;
        
        driverNameLabel.rightAnchor.constraint(equalTo: leftContent.rightAnchor).isActive = true
        
        //driverPhoneLabel
        driverPhoneLabel.topAnchor.constraint(equalTo: driverNameLabel.bottomAnchor, constant: 5).isActive = true;
        driverPhoneLabel.leadingAnchor.constraint(equalTo: driverNameLabel.leadingAnchor).isActive = true;
        driverPhoneLabel.rightAnchor.constraint(equalTo: leftContent.rightAnchor).isActive = true
        
        
        
        
        
        ///right conent childs
        
        //        fromLabel
        fromLabel.topAnchor.constraint(equalTo: priceLabel.topAnchor).isActive = true;
        fromLabel.leadingAnchor.constraint(equalTo: fromAddressLabel.leadingAnchor).isActive = true;
        
        //        fromAddressLabel
        fromAddressLabel.topAnchor.constraint(equalTo: fromLabel.bottomAnchor, constant: 2).isActive = true;
        fromAddressLabel.trailingAnchor.constraint(equalTo: rightContent.trailingAnchor, constant: -16).isActive = true;
        fromAddressLabel.leadingAnchor.constraint(equalTo: rightContent.leadingAnchor).isActive = true
        
        //        toLabel
        toLabel.topAnchor.constraint(equalTo: fromAddressLabel.bottomAnchor, constant: 10).isActive = true;
        toLabel.leadingAnchor.constraint(equalTo: fromAddressLabel.leadingAnchor).isActive = true;
        
        //        toAddressLabel
        toAddressLabel.topAnchor.constraint(equalTo: toLabel.bottomAnchor, constant: 2).isActive = true;
        toAddressLabel.leadingAnchor.constraint(equalTo: toLabel.leadingAnchor).isActive = true;
        toAddressLabel.trailingAnchor.constraint(equalTo: rightContent.trailingAnchor, constant: -16).isActive = true;
        //        durationLabel
        durationLabel.topAnchor.constraint(equalTo: toAddressLabel.bottomAnchor, constant: 10).isActive = true;
        durationLabel.leadingAnchor.constraint(equalTo: toAddressLabel.leadingAnchor).isActive = true;
        
        //        durationValueLabel
        durationValueLabel.topAnchor.constraint(equalTo: durationLabel.bottomAnchor, constant: 2).isActive = true;
        durationValueLabel.leadingAnchor.constraint(equalTo: durationLabel.leadingAnchor).isActive = true;
        
        //        distanceLabel
        distanceLabel.leadingAnchor.constraint(equalTo: durationValueLabel.leadingAnchor).isActive = true;
        distanceLabel.topAnchor.constraint(equalTo: durationValueLabel.bottomAnchor, constant: 10).isActive = true;
        
        //        distanceValueLabel
        distanceValueLabel.topAnchor.constraint(equalTo: distanceLabel.bottomAnchor, constant: 2).isActive = true;
        distanceValueLabel.leadingAnchor.constraint(equalTo: distanceLabel.leadingAnchor).isActive = true;
        
        //dateLabel
        dateLabel.leadingAnchor.constraint(equalTo: distanceValueLabel.leadingAnchor).isActive = true;
        dateLabel.bottomAnchor.constraint(equalTo: rightContent.bottomAnchor, constant: -10).isActive = true;
        
        
        //
        //        trackingImageView
        trackingImageView.leadingAnchor.constraint(equalTo: priceLabel.leadingAnchor, constant: -4).isActive = true;
        trackingImageView.bottomAnchor.constraint(equalTo: leftContent.bottomAnchor, constant: -5).isActive = true;
        trackingImageView.heightAnchor.constraint(equalToConstant: 34).isActive = true;
        trackingImageView.widthAnchor.constraint(equalToConstant: 34).isActive = true;
        
        //        trackingTitleLabel
        trackingTitleLabel.leadingAnchor.constraint(equalTo: trackingImageView.trailingAnchor, constant: 12).isActive = true;
        trackingTitleLabel.centerYAnchor.constraint(equalTo: trackingImageView.centerYAnchor).isActive = true;
        trackingTitleLabel.rightAnchor.constraint(equalTo: leftContent.rightAnchor).isActive = true
        
        //        trackingCollectionView
        trackingCollectionView.topAnchor.constraint(equalTo: bottomContent.topAnchor,constant :0).isActive = true;
        trackingCollectionView.leadingAnchor.constraint(equalTo: bottomContent.leadingAnchor,constant:20).isActive = true;
        trackingCollectionView.trailingAnchor.constraint(equalTo: bottomContent.trailingAnchor, constant: -16).isActive = true;
        trackingCollectionView.bottomAnchor.constraint(equalTo: bottomContent.bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension RideHistoryCell2: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    // MARK: - Collection Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trackers.count;
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdetifier, for: indexPath) as! TrackerCell
        cell.isSelectable = false
        cell.tracker = trackers[indexPath.row];
        return cell;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 40, height: 40);
    }
}
