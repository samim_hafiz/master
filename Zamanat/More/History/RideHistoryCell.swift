//
//  RideHistoryCell.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 5/15/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

private let cellIdetifier = "cellId";
class RideHistoryCell: UITableViewCell {
    
    var trackers = [Tracker]();
    
    let priceLabel: UILabel = {
        let label = UILabel();
        label.font = Style.bigFont;
        label.textColor = Style.mainColor;
        label.text = "250 AFN";
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverLabel: UILabel = {
        let label = UILabel();
        label.text = "Driver".localized.uppercased();
        label.font = Style.smallFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverProfileImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "roundedProfile").withRenderingMode(.alwaysOriginal);
        imageView.tintColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFill;
        imageView.layer.borderColor = Style.mainColor.cgColor;
        imageView.layer.borderWidth = 1;
        imageView.layer.cornerRadius = 25;
        imageView.layer.masksToBounds = true;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let driverNameLabel: UILabel = {
        let label = UILabel();
//        label.text = "Rahmatullah Darwish".localized;
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverPhoneLabel: UILabel = {
        let label = UILabel();
//        label.text = "93797148432".localized;
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let trackingImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "tracking").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFill;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        imageView.isHidden = true
        return imageView;
    }();
    
    let trackingTitleLabel: UILabel = {
        let label = UILabel();
//        label.text = "Rahmatullah Darwish".localized;
        label.font = Style.normalFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        label.isHidden = true
        return label;
    }();
    
    lazy var trackingCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout();
        layout.scrollDirection = .horizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout);
        collection.delegate = self;
        collection.dataSource = self;
        collection.backgroundColor = .white;
        collection.showsHorizontalScrollIndicator = false;
        collection.alwaysBounceHorizontal = true;
        collection.register(TrackerCell.self, forCellWithReuseIdentifier: cellIdetifier);
        collection.translatesAutoresizingMaskIntoConstraints = false;
        return collection;
    }();
    
    let fromLabel: UILabel = {
        let label = UILabel();
        label.text = "From".localized;
        label.font = Style.smallFont;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let fromAddressLabel: UILabel = {
        let label = UILabel();
        label.text = "Darulaman Road, AUAF";
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let toLabel: UILabel = {
        let label = UILabel();
        label.text = "To".localized;
        label.font = Style.smallFont;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let toAddressLabel: UILabel = {
        let label = UILabel();
//        label.text = "Park-e-Share Naw";
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let durationLabel: UILabel = {
        let label = UILabel();
        label.text = "Duration".localized;
        label.font = Style.smallFont;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let durationValueLabel: UILabel = {
        let label = UILabel();
//        label.text = "00:43:43";
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceLabel: UILabel = {
        let label = UILabel();
        label.text = "Distance".localized;
        label.font = Style.smallFont;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceValueLabel: UILabel = {
        let label = UILabel();
//        label.text = "23 km";
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let dateLabel: UILabel = {
        let label = UILabel();
//        label.text = "2018-01-13 5:40 PM";
        label.font = Style.smallFont;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        self.addSubview(priceLabel);
        self.addSubview(driverLabel);
        self.addSubview(driverProfileImageView);
        self.addSubview(driverNameLabel);
        self.addSubview(driverPhoneLabel);
        self.addSubview(trackingImageView);
        self.addSubview(trackingTitleLabel);
        self.addSubview(trackingCollectionView);
        self.addSubview(fromLabel);
        self.addSubview(fromAddressLabel);
        self.addSubview(toLabel);
        self.addSubview(toAddressLabel);
        self.addSubview(durationLabel);
        self.addSubview(durationValueLabel);
        self.addSubview(distanceLabel);
        self.addSubview(distanceValueLabel);
        self.addSubview(dateLabel);
        
        setupConstraints();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
//        priceLabel
        priceLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 16).isActive = true;
        priceLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16).isActive = true;
        
        //        driverLabel
        driverLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 24).isActive = true;
        driverLabel.leadingAnchor.constraint(equalTo: driverProfileImageView.trailingAnchor, constant: 16).isActive = true;
        
//        driverProfileImageView
        driverProfileImageView.leadingAnchor.constraint(equalTo: priceLabel.leadingAnchor).isActive = true;
        driverProfileImageView.topAnchor.constraint(equalTo: driverLabel.bottomAnchor, constant: 20).isActive = true;
        driverProfileImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true;
        driverProfileImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true;
        
//        driverNameLabel
        driverNameLabel.leadingAnchor.constraint(equalTo: driverLabel.leadingAnchor).isActive = true;
        driverNameLabel.topAnchor.constraint(equalTo: driverProfileImageView.topAnchor, constant: 2).isActive = true;
        
//        driverPhoneLabel
        driverPhoneLabel.topAnchor.constraint(equalTo: driverNameLabel.bottomAnchor, constant: 2).isActive = true;
        driverPhoneLabel.leadingAnchor.constraint(equalTo: driverNameLabel.leadingAnchor).isActive = true;
        
//        trackingImageView
        trackingImageView.leadingAnchor.constraint(equalTo: priceLabel.leadingAnchor, constant: -4).isActive = true;
        trackingImageView.topAnchor.constraint(equalTo: driverProfileImageView.bottomAnchor, constant: 44).isActive = true;
        trackingImageView.heightAnchor.constraint(equalToConstant: 34).isActive = true;
        trackingImageView.widthAnchor.constraint(equalToConstant: 34).isActive = true;
   
//        trackingTitleLabel
        trackingTitleLabel.leadingAnchor.constraint(equalTo: trackingImageView.trailingAnchor, constant: 12).isActive = true;
        trackingTitleLabel.centerYAnchor.constraint(equalTo: trackingImageView.centerYAnchor).isActive = true;
        
//        trackingCollectionView
        trackingCollectionView.topAnchor.constraint(equalTo: trackingImageView.bottomAnchor, constant: 4).isActive = true;
        trackingCollectionView.leadingAnchor.constraint(equalTo: trackingImageView.leadingAnchor).isActive = true;
        trackingCollectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16).isActive = true;
        trackingCollectionView.heightAnchor.constraint(equalToConstant: 50).isActive = true;
        
//        fromLabel
        fromLabel.topAnchor.constraint(equalTo: priceLabel.topAnchor).isActive = true;
        fromLabel.leadingAnchor.constraint(equalTo: fromAddressLabel.leadingAnchor).isActive = true;
    
//        fromAddressLabel
        fromAddressLabel.topAnchor.constraint(equalTo: fromLabel.bottomAnchor, constant: 8).isActive = true;
        fromAddressLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16).isActive = true;
        
//        toLabel
        toLabel.topAnchor.constraint(equalTo: fromAddressLabel.bottomAnchor, constant: 16).isActive = true;
        toLabel.leadingAnchor.constraint(equalTo: fromAddressLabel.leadingAnchor).isActive = true;
        
//        toAddressLabel
        toAddressLabel.topAnchor.constraint(equalTo: toLabel.bottomAnchor, constant: 8).isActive = true;
        toAddressLabel.leadingAnchor.constraint(equalTo: toLabel.leadingAnchor).isActive = true;
        
//        durationLabel
        durationLabel.topAnchor.constraint(equalTo: toAddressLabel.bottomAnchor, constant: 16).isActive = true;
        durationLabel.leadingAnchor.constraint(equalTo: toAddressLabel.leadingAnchor).isActive = true;
        
//        durationValueLabel
        durationValueLabel.topAnchor.constraint(equalTo: durationLabel.bottomAnchor, constant: 8).isActive = true;
        durationValueLabel.leadingAnchor.constraint(equalTo: durationLabel.leadingAnchor).isActive = true;
        
//        distanceLabel
        distanceLabel.leadingAnchor.constraint(equalTo: durationValueLabel.leadingAnchor).isActive = true;
        distanceLabel.topAnchor.constraint(equalTo: durationValueLabel.bottomAnchor, constant: 16).isActive = true;
        
//        distanceValueLabel
        distanceValueLabel.topAnchor.constraint(equalTo: distanceLabel.bottomAnchor, constant: 8).isActive = true;
        distanceValueLabel.leadingAnchor.constraint(equalTo: distanceLabel.leadingAnchor).isActive = true;
        
//        dateLabel
        dateLabel.leadingAnchor.constraint(equalTo: distanceValueLabel.leadingAnchor).isActive = true;
        dateLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16).isActive = true;
    }
}

extension RideHistoryCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    // MARK: - Collection Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 14;
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdetifier, for: indexPath) as! TrackerCell
//        cell.tracker = trackers[indexPath.row];
        return cell;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50);
    }
}
