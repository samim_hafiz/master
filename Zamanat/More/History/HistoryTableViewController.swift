//
//  HistoryTableViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 5/15/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class HistoryTableViewController: UIViewController,RefreshControlDelegation {
    

    var tripHistoryList:[RideHistory]!
    var rideHistoryList:[RideHistory]!
    let segmentConrolContainer:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Style.mainColor
        return view
    }()
    
    let segmentedControl:UISegmentedControl = {
        let control = UISegmentedControl(items: ["Trip".localized.uppercased(),"Location Sharing".localized.uppercased()])
        //        let titleTextAttributes = [NSAttributedStringKey.foregroundColor:Style.mainColor]
        let unselectedAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        control.setTitleTextAttributes(unselectedAttributes, for: .normal)
        control.translatesAutoresizingMaskIntoConstraints = false
        control.selectedSegmentIndex = 0
        control.backgroundColor = UIColor.clear
        control.addTarget(self, action: #selector(switchSegmentController), for: .valueChanged)
        control.tintColor = UIColor.white // Style.mainColor
        return control
    }()
    
    let tripsControllView:HistoryTableView = {
        let view = HistoryTableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    let rideControlView:HistoryTableView = {
        let view = HistoryTableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.loadRideHistoryData()
        
    }
    
    func setupViews(){
        view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Style.mainColor
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.title = "History".localized
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        view.addSubview(segmentConrolContainer)
        view.addSubview(tripsControllView)
        tripsControllView.refreshDelegate = self
        
        view.addSubview(rideControlView)
        rideControlView.refreshDelegate = self
        
        segmentConrolContainer.addSubview(segmentedControl)
        
        
        
        setupConstraint()
        switchSegmentController()
        
        
        
    }
    
    func setupConstraint(){
        segmentConrolContainer.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        segmentConrolContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        segmentConrolContainer.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        segmentConrolContainer.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        segmentedControl.topAnchor.constraint(equalTo: segmentConrolContainer.topAnchor,constant:0).isActive = true
        segmentedControl.widthAnchor.constraint(equalTo: segmentConrolContainer.widthAnchor).isActive = true
        segmentedControl.heightAnchor.constraint(equalTo: segmentConrolContainer.heightAnchor).isActive = true
        
        
        tripsControllView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor,constant:5).isActive = true
        tripsControllView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tripsControllView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tripsControllView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        rideControlView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor,constant:5).isActive = true
        rideControlView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        rideControlView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        rideControlView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
    
    @objc func switchSegmentController(){
        if segmentedControl.selectedSegmentIndex == 0{
            tripsControllView.isHidden = false
            rideControlView.isHidden = true
        }else{
            tripsControllView.isHidden = true
            rideControlView.isHidden = false
        }
    }
    
    
    private func loadRideHistoryData(_ page:Int = 0){
        self.tripHistoryList = []
        self.rideHistoryList = []
        if !self.tripsControllView.refresher.isRefreshing && !self.rideControlView.refresher.isRefreshing{
            self.view.startActivityIndicator()
        }
        
        guard let userid = Values.currentUser?.id else { return }
        
        Helpers.request(api: API.getAllUserTrips(userId: userid), method: .get) { (resultDic) in
            
            self.view.stopActivityIndicator()
            
            //whem reloading or refreshing the segment control views
            self.tripsControllView.refresher.endRefreshing()
            self.rideControlView.refresher.endRefreshing()
            
            guard let result:[String : Any] = resultDic else { return }
            if (result["success"] as? Bool)!{
                let datas:Array<[String: Any]> = (result["data"] as? Array<[String : Any]>)!
                
                for data in datas{
                    if let tripData = data["trip"] as? [String : Any]{
                        let tripType:Int = tripData["type"] as! Int
                        if tripType == TripType.ride.rawValue{
                            self.rideHistoryList.append(RideHistory(type:tripType, dict:data))
                        }else{
                            self.tripHistoryList.append(RideHistory(type:tripType, dict:data))
                        }
                    }
                }
                
                self.tripsControllView.historyList = self.tripHistoryList
                self.rideControlView.historyList = self.rideHistoryList
            }
        }
    }
    
    func onRefreshed() {
        self.loadRideHistoryData()
    }
}
