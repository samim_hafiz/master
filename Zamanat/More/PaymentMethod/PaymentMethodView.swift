//
//  PaymentMethodView.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 6/2/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
private var paymentMethodTableCellId:String = "paymentMethodCellId"
private var addPaymentMethodTableCellId:String = "addPaymentMethodCellId"
private var peymentHeaderId:String = "paymentHeader"
protocol RefreshPaymentMethodDelegation{
    func onRefreshed()
}
class PaymentMethodView:UITableView,UITableViewDelegate,UITableViewDataSource{
    var walletInfoList:[WalletHistoryInfo]!{
        didSet{
            self.reloadData()
        }
    }
    var size:Int = 0
    var refreshDelegate:RefreshPaymentMethodDelegation!
    var refresher:UIRefreshControl!
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        self.register(PaymentMethodViewCell.self, forCellReuseIdentifier: paymentMethodTableCellId)
        self.register(AddPaymentMethodCell.self, forCellReuseIdentifier: addPaymentMethodTableCellId)
        self.register(PaymentMethodHeader.self, forHeaderFooterViewReuseIdentifier: peymentHeaderId);
        self.dataSource = self
        self.delegate = self
        
        setupRefresher()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return size+1 // 1 : is for the last row (add button)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == size{
            let cell = tableView.dequeueReusableCell(withIdentifier: addPaymentMethodTableCellId, for: indexPath) as? AddPaymentMethodCell
            cell?.selectionStyle = .none
            return cell!
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: paymentMethodTableCellId, for: indexPath) as? PaymentMethodViewCell
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
   
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: peymentHeaderId) as! PaymentMethodHeader;
        return view;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == self.size{
            NotificationCenter.default.post(name: Notification.Name(KeyStrings.paymentCellSelectonNotificationName), object: nil)
        }else{
            
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PaymentMethodViewCell:UITableViewCell{
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.white
        setupViews()
    }
    
    func setupViews(){
        addSubview(bankLogo)
        addSubview(bankNameLabel)
        addSubview(amountLabel)
        
        bankLogo.topAnchor.constraint(equalTo: topAnchor,constant:10).isActive = true
        bankLogo.leftAnchor.constraint(equalTo: leftAnchor,constant:10).isActive = true
        bankLogo.bottomAnchor.constraint(equalTo: bottomAnchor,constant:-10).isActive = true
        bankLogo.widthAnchor.constraint(equalTo: bankLogo.heightAnchor).isActive = true
        
        
        bankNameLabel.leftAnchor.constraint(equalTo: bankLogo.rightAnchor,constant:10).isActive = true
        bankNameLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        bankNameLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bankNameLabel.rightAnchor.constraint(equalTo: amountLabel.leftAnchor).isActive = true
        
        amountLabel.rightAnchor.constraint(equalTo: rightAnchor,constant:-10).isActive = true
        amountLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        amountLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        
    }
    
    let bankLogo:UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.contentMode = .scaleAspectFill
        view.layer.masksToBounds = true
        view.image = UIImage(named:"buber_icon")
        return view
    }()
    
    let bankNameLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
//        lbl.text = "Azizi Bank"
        lbl.font = Style.normalFont
        return lbl
    }()
    
    let amountLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.gray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "***398439"
        lbl.font = Style.normalFont
        return lbl
    }()
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class AddPaymentMethodCell:UITableViewCell{
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.white
        setupViews()
    }
    
    func setupViews(){
        addSubview(addIcon)
        addSubview(addBtnLabel)
        addSubview(addBtn)
        
        addIcon.topAnchor.constraint(equalTo: topAnchor,constant:20).isActive = true
        addIcon.leftAnchor.constraint(equalTo: leftAnchor,constant:20).isActive = true
        addIcon.bottomAnchor.constraint(equalTo: bottomAnchor,constant:-20).isActive = true
        addIcon.widthAnchor.constraint(equalTo: addIcon.heightAnchor).isActive = true
        
        
        addBtnLabel.leftAnchor.constraint(equalTo: addIcon.rightAnchor,constant:20).isActive = true
        addBtnLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        addBtnLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        addBtnLabel.rightAnchor.constraint(equalTo: addBtn.leftAnchor).isActive = true
        
        addBtn.rightAnchor.constraint(equalTo: rightAnchor,constant:-20).isActive = true
        addBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        addBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        addBtn.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
    }
    
    let addIcon:UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.contentMode = .scaleAspectFill
        view.layer.masksToBounds = true
        view.image = #imageLiteral(resourceName: "add_icon")
        return view
    }()
    
    let addBtnLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.gray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "AddABankOrCard".localized
        lbl.font = Style.normalFont
        return lbl
    }()
    
    let addBtn:UIButton = {
        let btn = UIButton()
        btn.titleLabel?.textColor = UIColor.white
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Add".localized, for: .disabled)
        btn.isEnabled = false
        btn.backgroundColor = Style.greenColor
        btn.layer.cornerRadius = 10
        return btn
    }()
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PaymentMethodHeader: UITableViewHeaderFooterView {
    
    let headerTitleLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalFont;
        label.text = "LinkedAccounts".localized
        label.textColor = UIColor.black.withAlphaComponent(0.7);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier);
        
        self.addSubview(headerTitleLabel);
        
        headerTitleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true;
        headerTitleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PaymentMethodView{
    func setupRefresher(){
        //MARK: setting up the refresher
        self.refresher = UIRefreshControl()
        self.refresher = UIRefreshControl()
        //klsdfak;lkdsfaldfl;n;sdlf
        self.refresher.tintColor = UIColor(r: 31, g: 71, b: 98)
        self.refresher.addTarget(self, action: #selector(self.refreshing), for: UIControlEvents.valueChanged)
        self.addSubview(refresher)
    }
    
    @objc func refreshing(){
        self.refreshDelegate.onRefreshed()
    }
}
