//
//  AddPaymentComponentController.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 6/5/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class AddPaymentComponentController: UIViewController,UITextFieldDelegate {

    var titleText:String!{
        didSet{
            self.navigationItem.title = titleText.localized
        }
    }
    
    let fieldsComponentView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let nameField:CustomTextField = {
        let field = CustomTextField()
        field.translatesAutoresizingMaskIntoConstraints = false
        field.backgroundColor = UIColor.white
        field.returnKeyType = .next
        field.tag = 1
        field.becomeFirstResponder()
        field.placeholder = "FullName".localized
        return field
    }()
    
    let cardNumberField:CustomTextField = {
        let field = CustomTextField()
        field.translatesAutoresizingMaskIntoConstraints = false
        field.backgroundColor = UIColor.white
        field.returnKeyType = .next
        field.tag = 2
        field.placeholder = "CardNumber".localized
        return field
    }()
    
    let dateField:CustomTextField = {
        let field = CustomTextField()
        field.translatesAutoresizingMaskIntoConstraints = false
        field.backgroundColor = UIColor.white
        field.returnKeyType = .next
        field.tag = 3
        field.placeholder = "MM/YY".localized
        return field
    }()
    
    let cvvField:CustomTextField = {
        let field = CustomTextField()
        field.translatesAutoresizingMaskIntoConstraints = false
        field.backgroundColor = UIColor.white
        field.returnKeyType = .next
        field.tag = 4
        field.placeholder = "CVV".localized
        return field
    }()
    
    let zipCodeField:CustomTextField = {
        let field = CustomTextField()
        field.translatesAutoresizingMaskIntoConstraints = false
        field.backgroundColor = UIColor.white
        field.returnKeyType = .done
        field.tag = 5
        field.placeholder = "ZipCode".localized
        return field
    }()
    let dateFieldContainer:UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.distribution = .fillEqually
        view.spacing = 10
        return view
    }()
    
    let contBtn:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = Style.mainColor
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitle("Continue".localized, for: .normal)
        btn.layer.cornerRadius = 7
        return btn
    }()
    
    let processedIcon:UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = #imageLiteral(resourceName: "padlock")
        return view
    }()
    
    let proceccTitle:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = Style.mainColor
        lbl.font = Style.normalFont
        lbl.text = "ProcessedBy".localized
        return lbl
    }()
    
    let proceccMsg:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.darkGray
        lbl.font = Style.miniFont
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 2
        lbl.text = "Buber, 1008, Darulaman, Kabul, Afghanistan".localized
        return lbl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    func setupViews(){
        view.backgroundColor = Style.minBackground
        self.navigationController?.navigationBar.barTintColor = Style.mainColor
        navigationController?.navigationBar.tintColor = UIColor.white
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        view.addSubview(fieldsComponentView)
        fieldsComponentView.addSubview(nameField)
        fieldsComponentView.addSubview(cardNumberField)
        fieldsComponentView.addSubview(dateFieldContainer)
        
        dateFieldContainer.addArrangedSubview(dateField)
        dateFieldContainer.addArrangedSubview(cvvField)
        dateFieldContainer.addArrangedSubview(zipCodeField)
        
        
        fieldsComponentView.addSubview(contBtn)
        
        view.addSubview(processedIcon)
        view.addSubview(proceccTitle)
        view.addSubview(proceccMsg)
        
        
        nameField.delegate = self
        cardNumberField.delegate = self
        dateField.delegate = self
        cvvField.delegate = self
        zipCodeField.delegate = self
        
        setupConstraint()
        
    }
    
    func setupConstraint(){
        fieldsComponentView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        fieldsComponentView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        fieldsComponentView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        fieldsComponentView.heightAnchor.constraint(equalToConstant: 260).isActive = true
        
        nameField.topAnchor.constraint(equalTo: fieldsComponentView.topAnchor,constant:20).isActive = true
        nameField.leftAnchor.constraint(equalTo: fieldsComponentView.leftAnchor,constant:20).isActive = true
        nameField.rightAnchor.constraint(equalTo: fieldsComponentView.rightAnchor,constant:-20).isActive = true
        nameField.heightAnchor.constraint(equalToConstant: 45).isActive = true
    
        
        cardNumberField.topAnchor.constraint(equalTo: nameField.bottomAnchor,constant:5).isActive = true
        cardNumberField.leftAnchor.constraint(equalTo: fieldsComponentView.leftAnchor,constant:20).isActive = true
        cardNumberField.rightAnchor.constraint(equalTo: fieldsComponentView.rightAnchor,constant:-20).isActive = true
        cardNumberField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        dateFieldContainer.topAnchor.constraint(equalTo: cardNumberField.bottomAnchor,constant:5).isActive = true
        dateFieldContainer.leftAnchor.constraint(equalTo: fieldsComponentView.leftAnchor,constant:20).isActive = true
        dateFieldContainer.rightAnchor.constraint(equalTo: fieldsComponentView.rightAnchor,constant:-20).isActive = true
        dateFieldContainer.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        contBtn.topAnchor.constraint(equalTo: dateFieldContainer.bottomAnchor,constant:20).isActive = true
        contBtn.leftAnchor.constraint(equalTo: fieldsComponentView.leftAnchor,constant:20).isActive = true
        contBtn.rightAnchor.constraint(equalTo: fieldsComponentView.rightAnchor,constant:-20).isActive = true
        contBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        
        processedIcon.leftAnchor.constraint(equalTo: view.leftAnchor,constant:20).isActive = true
        processedIcon.heightAnchor.constraint(equalToConstant: 22).isActive = true
        processedIcon.widthAnchor.constraint(equalTo: processedIcon.heightAnchor).isActive = true
        processedIcon.topAnchor.constraint(equalTo: proceccTitle.topAnchor,constant:10).isActive = true
        
        proceccTitle.topAnchor.constraint(equalTo: fieldsComponentView.bottomAnchor,constant:20).isActive = true
        proceccTitle.leftAnchor.constraint(equalTo: processedIcon.rightAnchor,constant:20).isActive = true
        proceccTitle.rightAnchor.constraint(equalTo: fieldsComponentView.rightAnchor,constant:-20).isActive = true
        
        
        proceccMsg.leftAnchor.constraint(equalTo: proceccTitle.leftAnchor).isActive = true
        proceccMsg.rightAnchor.constraint(equalTo: proceccTitle.rightAnchor).isActive = true
        proceccMsg.topAnchor.constraint(equalTo: proceccTitle.bottomAnchor,constant:5).isActive = true
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1{
            self.cardNumberField.becomeFirstResponder()
        }else if textField.tag == 2{
            self.dateField.becomeFirstResponder()
        }else if textField.tag == 3{
            self.cvvField.becomeFirstResponder()
        }else if textField.tag == 4{
            self.zipCodeField.becomeFirstResponder()
        }else if textField.tag == 5{
            self.zipCodeField.resignFirstResponder()
        }
        
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
         let field:CustomTextField = textField as! CustomTextField
        UIView.animate(withDuration: 0.2) {
            field.bottomBorderHeightConstraint.constant = 0.8
            field.bottomBorder.backgroundColor = Style.mainColor
            field.bottomBorder.layoutIfNeeded()
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        let field:CustomTextField = textField as! CustomTextField
        UIView.animate(withDuration: 0.2) {
            field.bottomBorderHeightConstraint.constant = 0.5
            field.bottomBorder.backgroundColor = UIColor.gray
            field.bottomBorder.layoutIfNeeded()
        }
    }
}

class CustomTextField:UITextField,UITextFieldDelegate{
    
    let bottomBorder:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.gray
        return view
    }()
    
    var bottomBorderHeightConstraint:NSLayoutConstraint!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.delegate = self
        self.addSubview(bottomBorder)
        self.font = Style.normalFont
        
        bottomBorder.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        bottomBorder.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        
        bottomBorderHeightConstraint = bottomBorder.heightAnchor.constraint(equalToConstant: 0.5)
        bottomBorderHeightConstraint.isActive = true
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
