//
//  AddPaymentMethodController.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 6/3/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

private var compCellId:String = "paymentMethodComponentCellId"
class AddPaymentMethodController: UITableViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(PaymentMethodComponentCell.self, forCellReuseIdentifier: compCellId)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.setupViews()
    }
    
    func setupViews(){
        view.backgroundColor = Style.minBackground
        self.navigationController?.navigationBar.barTintColor = Style.mainColor
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.title = "AddPaymentMethod".localized
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: compCellId, for: indexPath) as? PaymentMethodComponentCell
        return cell!
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let addComponentVc = AddPaymentComponentController()
        addComponentVc.titleText = "AddDebitCard".localized
        self.navigationController?.pushViewController(addComponentVc, animated: true)
    }

}

class PaymentMethodComponentCell:UITableViewCell{
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.white
        self.setupView()
        self.accessoryType = .disclosureIndicator
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.accessoryView?.center = CGPoint(x: 10, y: 10)
    }
    func setupView(){
        addSubview(iconView)
        addSubview(componentLabel)
        addSubview(componentHint)
        addSubview(componentDescription)
        
        iconView.topAnchor.constraint(equalTo: topAnchor,constant:20).isActive = true
        iconView.leftAnchor.constraint(equalTo: leftAnchor,constant:20).isActive = true
        iconView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        iconView.widthAnchor.constraint(equalTo: iconView.heightAnchor).isActive = true
        
        componentLabel.topAnchor.constraint(equalTo: topAnchor,constant:17).isActive = true
        componentLabel.leftAnchor.constraint(equalTo: iconView.rightAnchor,constant:20).isActive = true
        
        componentHint.topAnchor.constraint(equalTo: componentLabel.bottomAnchor,constant:0).isActive = true
        componentHint.leftAnchor.constraint(equalTo: componentLabel.leftAnchor).isActive = true
        
        componentDescription.topAnchor.constraint(equalTo: componentHint.bottomAnchor,constant:15).isActive = true
        componentDescription.leftAnchor.constraint(equalTo: componentLabel.leftAnchor).isActive = true
        componentDescription.rightAnchor.constraint(equalTo: rightAnchor,constant:-20).isActive = true
        componentDescription.bottomAnchor.constraint(equalTo: bottomAnchor,constant:-10).isActive = true
        
    }
    
    let iconView:UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.layer.masksToBounds = true
        view.contentMode = .scaleAspectFit
        view.image = #imageLiteral(resourceName: "payements")
        return view
    }()
    
    let componentLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = Style.mainColor
        lbl.font = Style.normalBoldFont
        lbl.text = "DebitCard".localized;
        return lbl
    }()
    
    let componentHint:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.darkGray
        lbl.font = Style.normalFont
        lbl.text = "BuySmallAmounts".localized
        return lbl
    }()
    
    let componentDescription:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.darkGray
        lbl.font = Style.normalFont
        lbl.numberOfLines = 3
        lbl.lineBreakMode = .byWordWrapping
        lbl.text = "UseVisaOrMastercardToCredit".localized
        return lbl
    }()
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
