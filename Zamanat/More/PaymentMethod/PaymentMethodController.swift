//
//  PaymentMethodController.swift
//  Zamanat
//
//  Created by AHG-mac1436 on 6/2/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class PaymentMethodController: UIViewController {
    
    
    var wallet:Wallet!
    var walletHistoryInfoList:[WalletHistoryInfo] = []

    
    let currencyLabelContainer:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let walletLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = Style.normalFont
        lbl.text = "Wallet".localized
        return lbl
    }()
    
    let currencyLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = Style.normalFont
        lbl.text = "NativeCurrency".localized
        return lbl
    }()
    
    let currencyName:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.gray
        lbl.font = Style.normalFont
        lbl.text = "AFN".localized;
        return lbl
    }()
    
    let paymentMethodView:PaymentMethodView = {
        let view = PaymentMethodView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = false
        return view
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        //self.loadPaymentDetails()
        self.addObservers()
    }
    
    func setupViews(){
        view.backgroundColor = Style.minBackground
        self.navigationController?.navigationBar.barTintColor = Style.mainColor
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.title = "PaymentMethods".localized
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        view.addSubview(currencyLabelContainer)
        view.addSubview(paymentMethodView)
        view.addSubview(walletLabel)
        
        paymentMethodView.refreshDelegate = self
        currencyLabelContainer.addSubview(currencyLabel)
        currencyLabelContainer.addSubview(currencyName)
        
        setupConstraint()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    func setupConstraint(){
        paymentMethodView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        paymentMethodView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        paymentMethodView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        paymentMethodView.bottomAnchor.constraint(equalTo: walletLabel.topAnchor).isActive = true
        
        currencyLabelContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        currencyLabelContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        currencyLabelContainer.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        currencyLabelContainer.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        currencyLabel.leftAnchor.constraint(equalTo: currencyLabelContainer.leftAnchor,constant:10).isActive = true
        currencyLabel.centerYAnchor.constraint(equalTo: currencyLabelContainer.centerYAnchor).isActive = true
        currencyLabel.rightAnchor.constraint(equalTo: currencyName.leftAnchor,constant:-10).isActive = true
        
        currencyName.rightAnchor.constraint(equalTo: currencyLabelContainer.rightAnchor,constant:-10).isActive = true
        currencyName.centerYAnchor.constraint(equalTo: currencyLabelContainer.centerYAnchor).isActive = true
        //currencyName.rightAnchor.constraint(equalTo: currencyName.leftAnchor,constant:-10).isActive = true
        
        
        walletLabel.bottomAnchor.constraint(equalTo: currencyLabelContainer.topAnchor).isActive = true
        walletLabel.leftAnchor.constraint(equalTo: view.leftAnchor,constant:10).isActive = true
        walletLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }

    func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(paymentCellSelectedNotification(_:)), name: NSNotification.Name(rawValue: KeyStrings.paymentCellSelectonNotificationName), object: nil)
    }
    
    @objc func paymentCellSelectedNotification(_ notification:Notification){
        self.navigationController?.pushViewController(AddPaymentMethodController(), animated: true)
    }
}

extension PaymentMethodController:RefreshPaymentMethodDelegation{
    func onRefreshed() {
        //
    }
}
