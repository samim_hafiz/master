//
//  SettingCell.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 5/12/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {
    
    var settingItem: SettingItem? {
        didSet {
            self.iconImageView.image = settingItem?.image?.withRenderingMode(.alwaysTemplate);
            self.iconImageView.tintColor =  Style.mainColor;
            self.titleLabel.text = settingItem?.title;
        }
    }

    let iconImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let titleLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let detailsLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        
        self.addSubview(iconImageView);
        self.addSubview(titleLabel);
        self.addSubview(detailsLabel);
        
        self.accessoryType = .disclosureIndicator;
        
        setupConstraints();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        iconImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
        iconImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16).isActive = true;
        iconImageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.5).isActive = true;
        iconImageView.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.5).isActive = true;
        
        titleLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 16).isActive = true;
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
        
        detailsLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -18).isActive = true;
        detailsLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
    }
}
