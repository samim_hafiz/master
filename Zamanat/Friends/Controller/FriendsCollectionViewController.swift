//
//  FriendsCollectionViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 4/4/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import SwipeCellKit

private let reuseIdentifier = "Cell"
private let searchPeopleCellID = "SearchCell"
private let sectionHeader = "sectionHeader"

class FriendsCollectionViewController: UITableViewController, FriendsDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate, SwipeTableViewCellDelegate {
    
    lazy var refreshingControll: UIRefreshControl = {
        let controll = UIRefreshControl();
        controll.addTarget(self, action: #selector(loadFriends), for: .valueChanged);
        return controll;
    }();
    
    func updateSearchResults(for searchController: UISearchController) {
       self.tableView.reloadData();
    }
    func willDismissSearchController(_ searchController: UISearchController) {
       self.tableView.reloadData();
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let keyword = searchBar.text else { return }
        guard let userId = Values.currentUser?.id else { return }
        
        if(Values.currentUser == nil) {
            return;
        }
        
        let parameters = ["keyword": keyword, "user_id": userId];
        
        view.startActivityIndicator();
        Helpers.request(api: API.searchUsers, method: .post, parameters: parameters) { (result) in
            self.view.stopActivityIndicator();
            
            guard let resultDic = result else { return }
            if let data = resultDic["data"] as? [String: Any] {
                let people = Helpers.getPeople(dic: data);
                self.searchedPeople = people;
                self.tableView.reloadData();
            }
        }
    }
    
    var searchedPeople = [Person]();
    
    var favoriteFriends = [Friend]();
    var otherFriends = [Friend]();
    
    var searchController = UISearchController(searchResultsController: nil);
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.tableView.backgroundColor = .white;
        
        let searchBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showSearchController));
        self.navigationItem.leftBarButtonItem = searchBarButtonItem;

        // Register cell classes
        self.tableView.register(FriendCollectionViewCell.self, forCellReuseIdentifier: reuseIdentifier);
        self.tableView.register(SearchPeopleCell.self, forCellReuseIdentifier: searchPeopleCellID);
        self.tableView.register(FriendsHeaderView.self, forHeaderFooterViewReuseIdentifier: sectionHeader);
        self.tableView.separatorStyle = .none;
        self.tableView.tableFooterView = UIView();
        self.refreshControl = self.refreshingControll;
        
        // Search controller
        searchController.delegate = self;
        searchController.searchResultsUpdater = self;
        searchController.searchBar.isTranslucent = false;
        searchController.searchBar.delegate = self;
        searchController.dimsBackgroundDuringPresentation = false;
        searchController.hidesNavigationBarDuringPresentation = false;
        searchController.searchBar.showsCancelButton = true;
        definesPresentationContext = false;
        
        if (Values.currentUser == nil) {
            return;
        }
        
        loadFriends();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        reloadFriends();
    }
    
    @objc private func showSearchController() {
        present(searchController, animated: true, completion: nil);
    }

    @objc private func loadFriends() {
        guard let userid = Values.currentUser?.id else { return }
        if(!refreshingControll.isRefreshing){
            self.view.startActivityIndicator();
        }
        Helpers.request(api: API.getAllFriends(userId: userid), method: .get) { (result) in
            
            print("Friends: ", result);
            self.view.stopActivityIndicator();
            self.refreshingControll.endRefreshing();
            
            guard let result = result, let data = result["data"] as? [String: Any] else { return }
            if let people = data["friends"] as? Array<[String: Any]> {
                self.favoriteFriends.removeAll();
                self.otherFriends.removeAll();
                
                for personDic in people {
                    let friend = Friend(dic: personDic);
                    if friend.favorite {
                        self.favoriteFriends.append(friend);
                    } else {
                        self.otherFriends.append(friend);
                    }
                }
                self.tableView.reloadData();
            }
        }
    }
    
    private func reloadFriends() {
        guard let userid = Values.currentUser?.id else { return }
        Helpers.request(api: API.getAllFriends(userId: userid), method: .get) { (result) in
            guard let result = result else { return }
            if let people = result["friends"] as? Array<[String: Any]> {
                self.favoriteFriends.removeAll();
                self.otherFriends.removeAll();
                
                for personDic in people {
                    let friend = Friend(dic: personDic);
                    if friend.favorite {
                        self.favoriteFriends.append(friend);
                    } else {
                        self.otherFriends.append(friend);
                    }
                }
                self.tableView.reloadData();
            }
        }
    }

    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if(searchController.isActive){
            return 1
        } else {
            return 2
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchController.isActive){
            return searchedPeople.count;
        } else if(section == 0){
            return favoriteFriends.count;
        } else {
            return otherFriends.count;
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(searchController.isActive){
            return 0
        } else {
            if section == 0 && favoriteFriends.count <= 0 {
                return  0;
            } else {
                return  44;
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if searchController.isActive {
            let cell = tableView.dequeueReusableCell(withIdentifier: searchPeopleCellID, for: indexPath) as! SearchPeopleCell
            cell.person = searchedPeople[indexPath.row];
            return cell;
            
        } else if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! FriendCollectionViewCell
            favoriteFriends[indexPath.row].favorite = true;
            cell.friendDetails = (favoriteFriends[indexPath.row], indexPath);
            cell.delegate = self;
            cell.friendsDelegate = self;
            return cell;
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! FriendCollectionViewCell
            cell.friendDetails = (otherFriends[indexPath.row], indexPath);
            cell.delegate = self;
            cell.friendsDelegate = self;
            return cell;
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54;
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: sectionHeader) as! FriendsHeaderView;
        
        if(searchController.isActive) {
            sectionHeaderView.titleLabel.text = "";
        } else if favoriteFriends.count == 0 && otherFriends.count == 0 {
            if(section == 0 || searchController.isActive ){
                sectionHeaderView.titleLabel.text = "";
            } else {
                sectionHeaderView.titleLabel.text = "NoFriendInYourList".localized.uppercased();
                sectionHeaderView.titleLabel.textAlignment = .center;
            }
        } else if(section == 0){
            sectionHeaderView.titleLabel.text = favoriteFriends.count > 0 ? "NearestPeopleToYou".localized.uppercased() : "";
            sectionHeaderView.titleLabel.textAlignment = .left;
        } else {
            sectionHeaderView.titleLabel.text = "OtherPeopleInCircle".localized;
            sectionHeaderView.titleLabel.textAlignment = .left;
        }
        return sectionHeaderView;
    }

    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let blockAction = SwipeAction(style: .destructive, title: nil) { action, indexPath in
            
            if indexPath.section == 0 {
                self.blockUser(user: self.favoriteFriends[indexPath.row], indexPath: indexPath);
            } else {
                self.blockUser(user: self.otherFriends[indexPath.row], indexPath: indexPath);
            }
        }
        
        blockAction.image = #imageLiteral(resourceName: "block_action");
        
        return [blockAction]
    }
    
    private func blockUser(user: Friend, indexPath: IndexPath) {
        guard let name = user.displayName else { return }
        
        let alertController = UIAlertController(title: "Blocking".localized, message: String(format: "doYouWantToBlockUsre".localized, name), preferredStyle: .alert);
        
        let okAction = UIAlertAction(title: "Yes".localized, style: .default) { (_) in
             self.handleBlocking(friend: user, indexPath: indexPath);
        }
        let cancelAction = UIAlertAction(title: "No".localized, style: .cancel) { (_) in
            self.tableView.reloadData();
        }
        
        alertController.addAction(okAction);
        alertController.addAction(cancelAction);
        
        self.present(alertController, animated: true, completion: nil);
    }
    
    private func handleBlocking(friend: Friend, indexPath: IndexPath) {
        guard let blockUserId = friend.id else { return }
        guard let myUserId = Values.currentUser?.id else { return }
        
        let parameters = ["user_id": myUserId, "block_user_id": blockUserId, "option": 1] as [String : Any] // 1 = block, 0 = unblock
        
        self.view.startActivityIndicator();
        Helpers.request(api: API.blockUnblockUser, method: .post, parameters: parameters) { (result) in
            self.view.stopActivityIndicator();
            guard let result = result else {
                PopupMessages.showErrorMessage(message: "AnErrorOccurred".localized, buttonText: "");
                self.tableView.reloadData();
                return;
            }
            
            if let success = result["success"] as? Bool, success {
                PopupMessages.showSuccessMessage(message: "userBlocked".localized);
                if indexPath.section == 0 {
                    self.favoriteFriends.remove(at: indexPath.row);
                } else {
                    self.otherFriends.remove(at: indexPath.row);
                }
                self.tableView.reloadData();
            } else {
                PopupMessages.showErrorMessage(message: "AnErrorOccurred".localized, buttonText: "");
            }
        }
    }
    
    // MARK:- Friend Delegate
    func toggleFavorite(indexPath: IndexPath) {
        if(indexPath.section == 0){
            otherFriends.append(favoriteFriends[indexPath.row]);
            favoriteFriends[indexPath.row].favorite = false;
            favoriteFriends.remove(at: indexPath.row);
        } else {
            favoriteFriends.append(otherFriends[indexPath.row]);
            favoriteFriends[indexPath.row].favorite = true;
            otherFriends.remove(at: indexPath.row);
        }
        
        self.tableView.reloadData();
    }
}
