//
//  Avatar.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 2/17/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class TrackerCell: UICollectionViewCell {
    
    var trackingDelegate: TrackingDelegate?
    var isSelectable: Bool = true {
        didSet{
            if !isSelectable{
                checkMarkImageView.isHidden = true
            }
        }
    }
    var tracker: Tracker? {
        didSet {
            guard let tracker = tracker else { return }
            // update view data
            if let thumbUrl = tracker.imageUrl {
                self.thumbnailImageView.setImageFromUrl(urlStr: thumbUrl);
            }
            nameLabel.text = tracker.name;
            self.isSelected = tracker.isTracking;
        }
    }
    
    lazy var thumbnailImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "roundedProfile").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.layer.borderWidth = 2;
        imageView.layer.borderColor = Style.mainColor.cgColor;
        imageView.contentMode = .scaleAspectFill;
        let tap = UITapGestureRecognizer(target: self, action: #selector(selectDeselectTracker));
        imageView.isUserInteractionEnabled = true;
        imageView.addGestureRecognizer(tap);
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let checkMarkImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.layer.cornerRadius = 6;
        imageView.layer.masksToBounds =  true;
        imageView.image = #imageLiteral(resourceName: "checkmark");
        imageView.layer.borderWidth = 0.4;
        imageView.layer.borderColor = UIColor.white.cgColor;
        imageView.backgroundColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let nameLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallBoldFont;
        label.textAlignment = .center;
        label.adjustsFontSizeToFitWidth = true;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        self.addSubview(thumbnailImageView);
        self.addSubview(nameLabel);
        self.addSubview(checkMarkImageView);
        setupConstraints();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isSelected: Bool {
        didSet {
            
            if !isSelectable {return}
            
            if isSelected {
                self.checkMarkImageView.isHidden = false;
                self.thumbnailImageView.layer.opacity = 1;
                self.nameLabel.layer.opacity = 1;
            } else {
                self.checkMarkImageView.isHidden = true;
                self.thumbnailImageView.layer.opacity = 0.5;
                self.nameLabel.layer.opacity = 0.5;
            }
            
            if let id = tracker?.id {
                trackingDelegate?.updateTracking(id: id, tracking: isSelected);
            }
        }
    }
    
    @objc private func selectDeselectTracker() {
        isSelected = !isSelected;
        
        if let delegate = trackingDelegate as? TripDetailsPopupViewController {
            guard let userId = Values.currentUser?.id else { return }
            guard let tripId = delegate.trip.id else { return }
            guard let tracker_id = self.tracker?.id else { return }
            
            let parameters = ["user_id": userId, "status": isSelected ? 1 : 0, "ref_id": tripId, "trackers": [["user_id": tracker_id]]] as [String : Any];
            
            Helpers.request(api: API.addRemoveTracker, method: .post, parameters: parameters, completion: { (result) in
                print("Tracker status Update: ", result);
            })
        }
    }
    
    private func setupConstraints(){
        //        thumbnailImageView
        thumbnailImageView.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.65).isActive = true;
        thumbnailImageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.65).isActive = true;
        thumbnailImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true;
        thumbnailImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true;
        
        thumbnailImageView.layer.cornerRadius = self.frame.size.height * 0.65 / 2;
        thumbnailImageView.layer.masksToBounds = true;
        
        //        nameLabel
        nameLabel.topAnchor.constraint(equalTo: thumbnailImageView.bottomAnchor, constant: 2).isActive = true;
        nameLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 3).isActive = true;
        nameLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -3).isActive = true;
        
        //        checkMarkImageView
        checkMarkImageView.centerXAnchor.constraint(equalTo: thumbnailImageView.rightAnchor).isActive = true;
        checkMarkImageView.centerYAnchor.constraint(equalTo: thumbnailImageView.bottomAnchor, constant: -15).isActive = true;
        checkMarkImageView.widthAnchor.constraint(equalToConstant: 12).isActive = true;
        checkMarkImageView.heightAnchor.constraint(equalToConstant: 12).isActive = true;
    }
}

protocol TrackingDelegate {
    func updateTracking(id: String, tracking: Bool);
}
