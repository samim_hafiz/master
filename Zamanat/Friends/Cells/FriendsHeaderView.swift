//
//  FriendsHeaderView.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 4/4/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class FriendsHeaderView: UITableViewHeaderFooterView {
    
    let titleLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        label.textColor = Style.grayColor;
        return label;
    }();
    
    let seperatorView: UIView = {
        let view = UIView();
        view.backgroundColor = Style.grayColor.withAlphaComponent(0.5);
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier);
//        self.backgroundColor = Style.grayColor.withAlphaComponent(0.5);
        self.addSubview(titleLabel);
        self.addSubview(seperatorView);
        
        titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 24).isActive = true;
        titleLabel.bottomAnchor.constraint(equalTo: seperatorView.topAnchor, constant: -8).isActive = true;
        titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -24).isActive = true;
        
//        seperatorView
        seperatorView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor).isActive = true;
        seperatorView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true;
        seperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
