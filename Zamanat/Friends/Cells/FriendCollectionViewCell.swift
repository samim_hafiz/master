//
//  FriendCollectionViewCell.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 4/2/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import SwipeCellKit

protocol FriendsDelegate {
    func toggleFavorite(indexPath: IndexPath);
}

class FriendCollectionViewCell: SwipeTableViewCell {
    
    var friendsDelegate: FriendsDelegate?
    var friendDetails: (Friend, IndexPath)? {
        didSet {
            guard let friend = friendDetails?.0 else { return }
            if let url = friend.thumbUrl {
                self.profileImageView.setImageFromUrl(urlStr: url);
            }
            fullNameLabel.text = friend.displayName;
            phoneNumberLabel.text = friend.phone;
            if friend.favorite {
                self.setFavorite(true);
            } else {
                self.setFavorite(false);
            }
        }
    }
    
//    var person: Person? {
//        didSet {
//            if let url = person?.thumbUrl {
//                self.profileImageView.setImageFromUrl(urlStr: url);
//            }
//            fullNameLabel.text = person?.displayName;
//            phoneNumberLabel.text = person?.phone;
//        }
//    }
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.contentMode = .scaleAspectFill;
        imageView.layer.borderColor = Style.mainColor.cgColor;
        imageView.layer.borderWidth = 1;
        imageView.layer.cornerRadius = 25;
        imageView.image = #imageLiteral(resourceName: "roundedProfile").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.layer.masksToBounds = true;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let fullNameLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalFont;
        label.text = "Saifuddin Sepehr";
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let phoneNumberLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallFont;
        label.textColor = Style.grayFontColor.withAlphaComponent(0.8);
        label.text = "+93797134083";
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var favoriteButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(toggleFavorite), for: .touchUpInside);
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
        button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
        button.imageView?.contentMode = .scaleAspectFit;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        backgroundColor = .white;
        
        addSubview(profileImageView);
        addSubview(fullNameLabel);
        addSubview(phoneNumberLabel);
        addSubview(favoriteButton);
        self.selectionStyle = .none;
        
        setupConstraints();
    }
    
//    var favorite = false;
    @objc func toggleFavorite(){
        friendsDelegate?.toggleFavorite(indexPath: friendDetails!.1);
    }
    
    func setFavorite(_ fav: Bool) {
        if (fav){
         favoriteButton.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
        } else {
            favoriteButton.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
        }
    }
    
    private func setupConstraints() {
//        profileImageView
        profileImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
        profileImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true;
        profileImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true;
        profileImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true;
        
//        fullNameLabel
        fullNameLabel.topAnchor.constraint(equalTo: profileImageView.topAnchor).isActive = true;
        fullNameLabel.heightAnchor.constraint(equalTo: profileImageView.heightAnchor, multiplier: 0.5).isActive = true;
        fullNameLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 12).isActive = true;
        
        
//        phoneNumberLabel
        phoneNumberLabel.topAnchor.constraint(equalTo: fullNameLabel.bottomAnchor, constant: 2).isActive = true;
        phoneNumberLabel.leadingAnchor.constraint(equalTo: fullNameLabel.leadingAnchor).isActive = true;
        
        favoriteButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true;
        favoriteButton.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true;
        favoriteButton.heightAnchor.constraint(equalToConstant: 40).isActive = true;
        favoriteButton.widthAnchor.constraint(equalToConstant: 40).isActive = true;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
