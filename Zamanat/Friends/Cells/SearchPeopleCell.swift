//
//  FriendCollectionViewCell.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 4/2/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import SwipeCellKit


class SearchPeopleCell: SwipeTableViewCell {
    
    var person: Person? {
        didSet {
            guard let person = person else { return }
            if let url = person.thumbUrl {
                self.profileImageView.setImageFromUrl(urlStr: url);
            }
            fullNameLabel.text = person.displayName;
            phoneNumberLabel.text = person.phone;
            
            if person.friendship == .friend {
                self.actionButton.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            } else {
                self.actionButton.backgroundColor = Style.mainColor;
                self.actionButton.setTitleColor(.white, for: .normal);
                self.actionButton.layer.cornerRadius = 3;
                self.actionButton.layer.masksToBounds = true;
                self.actionButtonWidthAnchor?.constant = 80;
                
                if person.friendship == .notFriend {
                    self.actionButton.setImage(nil, for: .normal);
                    self.actionButton.setTitle("Add Friend", for: .normal);
                } else if person.friendship == .requestSent {
                    self.actionButton.setImage(nil, for: .normal);
                    self.actionButton.setTitle("Cancel Request", for: .normal);
                    self.actionButton.backgroundColor = Style.mainColor;
                } else if person.friendship == .requestReceived {
                    self.actionButton.setImage(nil, for: .normal);
                    self.actionButton.setTitle("Accept", for: .normal);
                } else {
                     self.actionButton.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
                    self.actionButton.setTitle("", for: .normal);
                    self.actionButton.backgroundColor = .white;
                }
            }
        }
    }
    
    //    var person: Person? {
    //        didSet {
    //            if let url = person?.thumbUrl {
    //                self.profileImageView.setImageFromUrl(urlStr: url);
    //            }
    //            fullNameLabel.text = person?.displayName;
    //            phoneNumberLabel.text = person?.phone;
    //        }
    //    }
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.contentMode = .scaleAspectFill;
        imageView.layer.borderColor = Style.mainColor.cgColor;
        imageView.image = #imageLiteral(resourceName: "roundedProfile").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.layer.borderWidth = 1;
        imageView.layer.cornerRadius = 25;
        imageView.backgroundColor = .white;
        imageView.layer.masksToBounds = true;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let fullNameLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallBoldFont;
        label.adjustsFontSizeToFitWidth = true;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let phoneNumberLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallFont;
        label.textColor = Style.grayFontColor.withAlphaComponent(0.8);
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var actionButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(actionButtonTap), for: .touchUpInside);
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5);
        button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
        button.titleLabel?.adjustsFontSizeToFitWidth = true;
        button.titleLabel?.font = Style.smallFont;
        button.imageView?.contentMode = .scaleAspectFit;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        
        backgroundColor = .white;
        
        addSubview(profileImageView);
        addSubview(fullNameLabel);
        addSubview(phoneNumberLabel);
        addSubview(actionButton);
        
        self.selectionStyle = .none;
        
        setupConstraints();
    }
    
    //    var favorite = false;
    @objc func actionButtonTap(){
        if let friendship = person?.friendship {
            if (friendship == .friend){
                self.friendshipAction(action: .unfriend);
            } else if friendship == .notFriend {
                self.friendshipAction(action: .sendRequest);
            } else if friendship == .requestReceived {
                self.friendshipAction(action: .acceptRequest);
            } else if friendship == .requestSent {
                self.friendshipAction(action: .cancelRequest);
            }
        }
    }
    
    
    var actionButtonWidthAnchor: NSLayoutConstraint?
    
    private func setupConstraints() {
        //        profileImageView
        profileImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
        profileImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true;
        profileImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true;
        profileImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true;
        
        //        fullNameLabel
        fullNameLabel.topAnchor.constraint(equalTo: profileImageView.topAnchor, constant: 2).isActive = true;
        fullNameLabel.heightAnchor.constraint(equalTo: profileImageView.heightAnchor, multiplier: 0.5).isActive = true;
        fullNameLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 12).isActive = true;
        fullNameLabel.trailingAnchor.constraint(equalTo: actionButton.leadingAnchor, constant: -8).isActive = true;
        
        //        phoneNumberLabel
        phoneNumberLabel.topAnchor.constraint(equalTo: fullNameLabel.bottomAnchor, constant: 1).isActive = true;
        phoneNumberLabel.leadingAnchor.constraint(equalTo: fullNameLabel.leadingAnchor).isActive = true;
        
        actionButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true;
        actionButton.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true;
        actionButton.heightAnchor.constraint(equalToConstant: 30).isActive = true;
        actionButtonWidthAnchor = actionButton.widthAnchor.constraint(equalToConstant: 40);
        actionButtonWidthAnchor?.isActive = true;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented");
    }
    
    private func friendshipAction(action: FriendshipAction) {
        guard let currentUser = Values.currentUser?.id else { return }
        guard let receiver = person?.id else { return }
        
        if action == .unfriend {
            let parameters = ["sender": currentUser, "receiver": receiver] as [String : Any];
            self.actionButton.setTitle("Add Friend", for: .normal);
            Helpers.request(api: API.unfriend, method: .post, parameters: parameters) { (_) in }
        } else {
            
            let parameters = ["action": action.rawValue, "sender": currentUser, "receiver": receiver] as [String : Any];
            
            self.startActivityIndicator();
            Helpers.request(api: API.friendship, method: .post, parameters: parameters) { (result) in
                self.stopActivityIndicator();
                
                let person1 = self.person;
                
                guard let result = result else { return }
                
                print("Action: ", action)
                
                if let success = result["success"] as? Bool, success {
                    if(action == .cancelRequest){
                        self.actionButton.setTitle("Add Friend", for: .normal);
                        person1?.friendship = .notFriend;
                    } else if (action == .acceptRequest) {
                        self.actionButton.setTitle("", for: .normal);
                        self.actionButton.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
                        self.actionButtonWidthAnchor?.constant = 42;
                        person1?.friendship = .friend;
                    } else if (action == .sendRequest) {
                        self.actionButton.setTitle("Cancel Request", for: .normal);
                        person1?.friendship = .requestSent;
                    } else {
                        self.actionButton.setTitle("Add Friend", for: .normal);
                        person1?.friendship = .notFriend;
                    }
                    
                    self.person = person1;
                    
                    UIView.animate(withDuration: 0.25, animations: {
                        self.layoutSubviews();
                    })
                }
            }
        }
    }
}

enum FriendshipAction: Int {
    case sendRequest = 1, cancelRequest = 2, rejectRequest = 3, acceptRequest = 4, block = 5, unfriend = 6;
}
