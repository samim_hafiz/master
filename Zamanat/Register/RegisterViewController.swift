//
//  LoginViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 4/9/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import FirebaseAuth

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    let containerView: UIView = {
        let view = UIView();
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "buber_logo").withRenderingMode(.alwaysOriginal);
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let descriptionLabel: UILabel = {
        let label = UILabel();
        label.text = "TakeRidesTrackBeloved".localized
        label.font = Style.smallFont;
        label.textColor = Style.grayColor;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let seperatorView: UIView = {
        let view = UIView();
        view.backgroundColor = Style.grayColor.withAlphaComponent(0.3);
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let countryCodeButton: UIButton = {
        let button = UIButton(type: .custom);
        button.setTitle(Helpers.getCurrentCountryCode(), for: .normal);
        button.setTitle("+93", for: .normal);
        button.backgroundColor = Style.grayColor.withAlphaComponent(0.3);
        button.titleLabel?.adjustsFontSizeToFitWidth = true;
        button.setTitleColor(.black, for: .normal);
        button.titleLabel?.textAlignment = .center;
        button.titleLabel?.font = Style.smallFont;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    let phoneNumberTextField: UITextField = {
        let textField = UITextField();
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.rightView = padView;
        textField.tag = 1;
        textField.leftView = padView;
        textField.placeholder = "e.g, 797134454";
        textField.keyboardType = .asciiCapableNumberPad;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    let phoneView: UIView = {
        let view = UIView();
        view.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        view.layer.borderWidth = 1;
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = true;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let passwordTextField: UITextField = {
        let textField = UITextField();
        textField.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        textField.layer.borderWidth = 1;
        textField.layer.cornerRadius = 4;
        textField.tag = 4;
        textField.layer.masksToBounds = true;
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.rightView = padView;
        textField.leftView = padView;
        textField.placeholder = "Password".localized;
        textField.keyboardType = .asciiCapable;
        textField.isSecureTextEntry = true;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    let firstNameTextField: UITextField = {
        let textField = UITextField();
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        textField.layer.borderWidth = 1;
        textField.layer.cornerRadius = 4;
        textField.tag = 2;
        textField.layer.masksToBounds = true;
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.autocapitalizationType = .words;
        textField.rightView = padView;
        textField.leftView = padView;
        textField.placeholder = "FirstName".localized;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    let lastNameTextField: UITextField = {
        let textField = UITextField();
        textField.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        textField.layer.borderWidth = 1;
        textField.layer.cornerRadius = 4;
        textField.tag = 3;
        textField.autocapitalizationType = .words;
        textField.layer.masksToBounds = true;
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.rightView = padView;
        textField.leftView = padView;
        textField.placeholder = "LastName".localized;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    lazy var nextButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(nextStep), for: .touchUpInside);
        button.setTitle("Next".localized, for: .normal);
        button.setTitleColor(.white, for: .normal);
        button.backgroundColor = Style.mainColor;
        button.layer.cornerRadius = 4;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        view.addSubview(containerView);
        containerView.addSubview(logoImageView);
        containerView.addSubview(descriptionLabel);
        containerView.addSubview(seperatorView);
        phoneView.addSubview(countryCodeButton);
        phoneView.addSubview(phoneNumberTextField);
        containerView.addSubview(phoneView);
        containerView.addSubview(firstNameTextField);
        containerView.addSubview(lastNameTextField);
        containerView.addSubview(passwordTextField);
        containerView.addSubview(nextButton);
        
        phoneNumberTextField.delegate = self;
        firstNameTextField.delegate = self;
        lastNameTextField.delegate = self;
        passwordTextField.delegate = self;
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard));
        self.view.addGestureRecognizer(tap);
        
        setupConstraints();
    }
    
    @objc private func closeKeyboard(){
        self.view.endEditing(true);
        
        self.containverViewTopAnchor?.constant = 0;
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded();
            self.containerView.layoutIfNeeded();
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        var constant: CGFloat = 0;
        switch textField.tag {
        case 1:
            constant = 0;
        case 2:
            constant = -50;
        case 3:
            constant = -100;
        case 4:
            constant = -160;
        default:
            constant = 0
        }
        
        self.containverViewTopAnchor?.constant = constant;
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded();
            self.containerView.layoutIfNeeded();
        }
    }
    
    @objc private func nextStep(){
        closeKeyboard();
        
        guard let phoneNumber = phoneNumberTextField.text, !phoneNumber.isEmpty else {
            PopupMessages.showInfoMessage(title: "", message: "EnterPhone".localized);
            return;
        }
        guard let firstName = firstNameTextField.text, !firstName.isEmpty else {
            PopupMessages.showInfoMessage(title: "", message: "EnterFirstName".localized);
            return;
        }
        guard let lastName = lastNameTextField.text, !lastName.isEmpty else {
            PopupMessages.showInfoMessage(title: "", message: "EnterLastName".localized);
            return;
        }
        guard let password = passwordTextField.text, (!password.isEmpty && password.count > 4) else {
            PopupMessages.showInfoMessage(title: "", message: "EnterValidPassword".localized);
            return;
        }

        guard let phoneCode = countryCodeButton.titleLabel?.text else {
            PopupMessages.showInfoMessage(title: "", message: "SelectCountryCode".localized);
            return;
        }
        
        var phone = phoneNumber;
        if(phone.first! == "0") {
            phone.remove(at: phone.startIndex);
        }
        let phoneWithCode = phoneCode + phone;
        
        let userDetails = ["name": firstName, "display_name": firstName + " " + lastName, "last_name": lastName, "phone": phoneWithCode, "password": password, "country": "Afghanistan"];
        
        view.startActivityIndicator();
        Helpers.request(api: API.checkPhone, method: .post, parameters: ["phone": phoneWithCode]) { (resultDic) in
            self.view.stopActivityIndicator();
            
            guard let resultDic = resultDic else { return }
            
            if let success = resultDic["success"] as? Bool, success {
                PopupMessages.showInfoMessage(title: "", message: "UserAlreadyExists".localized);
            } else {
                self.validatePhone(phoneString: phoneWithCode, userDetails: userDetails);
            }
        }
    }
    
    private func validatePhone(phoneString: String, userDetails: [String: String]) {
        view.startActivityIndicator();
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneString, uiDelegate: nil) { (aid, error) in
            self.view.stopActivityIndicator();
            
            if(error != nil){
                PopupMessages.showWarningMessage(title: "", message: error!.localizedDescription.localized);
                return;
            } else {
                UserDefaults.standard.setValue(aid!, forKey: "aid");
                UserDefaults.standard.setValue(phoneString, forKey: KeyStrings.phoneNumber);
                UserDefaults.standard.synchronize();
                let entercodeViewController = EnterCodeViewController();
                entercodeViewController.userDetails = userDetails;
                self.navigationController?.pushViewController(entercodeViewController, animated: true);
            }
        }
    }
    
    var containverViewTopAnchor: NSLayoutConstraint?
    
    private func setupConstraints() {
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true;
        containverViewTopAnchor = containerView.topAnchor.constraint(equalTo: view.topAnchor);
        containverViewTopAnchor?.isActive = true;
        containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
       containerView.heightAnchor.constraint(equalTo: view.heightAnchor, constant: 0).isActive = true;
        
        logoImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 28).isActive = true;
        logoImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true;
        logoImageView.heightAnchor.constraint(equalToConstant: 32).isActive = true;
        logoImageView.widthAnchor.constraint(equalToConstant: 120).isActive = true;
        
        descriptionLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 12).isActive = true;
        descriptionLabel.centerXAnchor.constraint(equalTo: logoImageView.centerXAnchor).isActive = true;
        
        phoneView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 30).isActive = true;
        phoneView.centerXAnchor.constraint(equalTo: descriptionLabel.centerXAnchor).isActive = true;
        phoneView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 0.85).isActive = true;
        phoneView.heightAnchor.constraint(equalToConstant: 48).isActive = true;
        
        countryCodeButton.leftAnchor.constraint(equalTo: phoneView.leftAnchor).isActive = true;
        countryCodeButton.centerYAnchor.constraint(equalTo: phoneView.centerYAnchor).isActive = true;
        countryCodeButton.widthAnchor.constraint(equalToConstant: 44).isActive = true;
        countryCodeButton.heightAnchor.constraint(equalTo: phoneView.heightAnchor).isActive = true;
        
        phoneNumberTextField.leftAnchor.constraint(equalTo: countryCodeButton.rightAnchor).isActive = true;
        phoneNumberTextField.centerYAnchor.constraint(equalTo: phoneView.centerYAnchor).isActive = true;
        phoneNumberTextField.rightAnchor.constraint(equalTo: phoneView.rightAnchor).isActive = true;
        phoneNumberTextField.heightAnchor.constraint(equalTo: phoneView.heightAnchor).isActive = true;
        
        firstNameTextField.topAnchor.constraint(equalTo: phoneView.bottomAnchor, constant: 8).isActive = true;
        firstNameTextField.centerXAnchor.constraint(equalTo: phoneView.centerXAnchor).isActive = true;
        firstNameTextField.widthAnchor.constraint(equalTo: phoneView.widthAnchor).isActive = true;
        firstNameTextField.heightAnchor.constraint(equalTo: phoneView.heightAnchor).isActive = true;
        
        lastNameTextField.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor, constant: 8).isActive = true;
        lastNameTextField.centerXAnchor.constraint(equalTo: phoneView.centerXAnchor).isActive = true;
        lastNameTextField.widthAnchor.constraint(equalTo: phoneView.widthAnchor).isActive = true;
        lastNameTextField.heightAnchor.constraint(equalTo: phoneView.heightAnchor).isActive = true;
        
        passwordTextField.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor, constant: 8).isActive = true;
        passwordTextField.centerXAnchor.constraint(equalTo: phoneView.centerXAnchor).isActive = true;
        passwordTextField.widthAnchor.constraint(equalTo: phoneView.widthAnchor).isActive = true;
        passwordTextField.heightAnchor.constraint(equalTo: phoneView.heightAnchor).isActive = true;
        
        nextButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 36).isActive = true;
        nextButton.centerXAnchor.constraint(equalTo: passwordTextField.centerXAnchor).isActive = true;
        nextButton.widthAnchor.constraint(equalTo: passwordTextField.widthAnchor).isActive = true;
        nextButton.heightAnchor.constraint(equalTo: passwordTextField.heightAnchor).isActive = true;
    }
}
