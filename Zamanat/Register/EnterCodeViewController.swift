//
//  EnterCodeViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 4/24/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.


import UIKit
import FirebaseAuth

class EnterCodeViewController: UIViewController {

    var userDetails = [String: String]();
    
    let enterCodelabel: UILabel = {
        let label = UILabel();
        label.font = Style.veryBigFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let codeTextField: UITextField = {
        let textField = UITextField();
        textField.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        textField.layer.borderWidth = 1;
        textField.layer.cornerRadius = 4;
        textField.layer.masksToBounds = true;
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.textAlignment = .center;
        textField.rightView = padView;
        textField.leftView = padView;
        textField.placeholder = "EnterCode".localized;
        textField.keyboardType = .asciiCapableNumberPad;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    lazy var signupButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(signup), for: .touchUpInside);
        button.setTitle("Register".localized, for: .normal);
        button.setTitleColor(.white, for: .normal);
        button.backgroundColor = Style.mainColor;
        button.layer.cornerRadius = 4;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        
        view.addSubview(enterCodelabel);
        view.addSubview(codeTextField);
        view.addSubview(signupButton);
        
        if let phoneNumber = UserDefaults.standard.value(forKey: KeyStrings.phoneNumber) as? String {
            self.enterCodelabel.text = phoneNumber;
        }

        setupConstraints();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.codeTextField.becomeFirstResponder();
    }
    
    @objc private func signup() {
        
        self.view.endEditing(true);
        
        guard let aid = UserDefaults.standard.value(forKey: "aid") as? String else {
            return;
        }
        
        guard let code = codeTextField.text, !code.isEmpty else {
            PopupMessages.showInfoMessage(title: "", message: "EnterCode".localized);
            return;
        }
        
        view.startActivityIndicator();
        let credintials = PhoneAuthProvider.provider().credential(withVerificationID: aid, verificationCode: code);
        Auth.auth().signIn(with: credintials) { (user, error) in
            self.view.stopActivityIndicator();
            print(error);
            if (error != nil) {
                PopupMessages.showWarningMessage(title: "", message: error!.localizedDescription.localized)
                return;
            }
            
            if !self.userDetails.isEmpty {
                Helpers.request(api: API.registerUser, method: .post, parameters: self.userDetails, completion: { (result) in
                    guard let result = result else { return }
                    
                    if let success = result["success"] as? Bool, success {
                        if let userData = result["data"] as? [String: Any] {
                            let user = User(dic: userData);
                            Values.currentUser = user;
                            self.dismiss(animated: true, completion: nil);
                            
                            if let bounus = result["bounus_amount"] as? Double, bounus > 0 {
                                PopupMessages.showInfoMessage(title: "congratulations".localized, message: String(format: "youReceiveBonus".localized, bounus));
                            }
                        }
                    }
                })
            } else {
                PopupMessages.showWarningMessage(title: "", message: "AnErrorOccurred".localized);
            }
        }
    }
    
    private func setupConstraints() {
//        enterCodelabel
        enterCodelabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 44).isActive = true;
        enterCodelabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
//        codeTextField
        codeTextField.topAnchor.constraint(equalTo: enterCodelabel.bottomAnchor, constant: 30).isActive = true;
        codeTextField.centerXAnchor.constraint(equalTo: enterCodelabel.centerXAnchor).isActive = true;
        codeTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true;
        codeTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true;
        
//        signupButton
        signupButton.topAnchor.constraint(equalTo: codeTextField.bottomAnchor, constant: 22).isActive = true;
        signupButton.centerXAnchor.constraint(equalTo: codeTextField.centerXAnchor).isActive = true;
        signupButton.heightAnchor.constraint(equalTo: codeTextField.heightAnchor).isActive = true;
        signupButton.widthAnchor.constraint(equalTo: codeTextField.widthAnchor).isActive = true;
    }
}
