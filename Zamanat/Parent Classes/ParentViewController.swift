//
//  ParentViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 2/3/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

/// This class a parrent class for all view controller is the app. It makes it eassy to implement a functionality or behaviour accroos all controllers in one place.
class ParentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad();
        view.backgroundColor = .white;

        // Do any additional setup after loading the view.
    }

}
