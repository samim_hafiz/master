//
//  ParentNavigationController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 2/3/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class ParentNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false;
        self.navigationBar.barTintColor = .white;
        self.navigationBar.isTranslucent = false;
        self.navigationBar.tintColor = Style.mainColor;
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.clear], for: .normal);
    }
}
