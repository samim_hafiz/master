//
//  TabBarViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 2/4/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import Lottie

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        tabBar.isTranslucent = false;

        let activeTripViewController = ActiveTripViewController();
        activeTripViewController.title = "Ride".localized;
        let activeTripTabBar = UITabBarItem(title: "Ride".localized, image: #imageLiteral(resourceName: "pin"), tag: 0);
        activeTripViewController.tabBarItem = activeTripTabBar;
        
        let trackingViewController = TrackingViewController();
        trackingViewController.title = "Tracking".localized;
        let trackingTabBar = UITabBarItem(title: "Tracking".localized, image: #imageLiteral(resourceName: "tracking"), tag: 1);
        trackingViewController.tabBarItem = trackingTabBar;
        
        let friendsViewController = FriendsCollectionViewController();
        friendsViewController.title = "Friends".localized;
        let friendsTabBar = UITabBarItem(title: "Friends".localized, image: #imageLiteral(resourceName: "friends"), tag: 2);
        friendsViewController.tabBarItem = friendsTabBar;
        
        let moreViewController = MoreTableViewController();
        moreViewController.title = "More".localized;
        let moreTabBar = UITabBarItem(title: "More".localized, image: #imageLiteral(resourceName: "more"), tag: 3);
        moreViewController.tabBarItem = moreTabBar;
        
        let activeTripNavigationController = ParentNavigationController(rootViewController: activeTripViewController);
        let trackingNavigationController = ParentNavigationController(rootViewController: trackingViewController);
        let friendsNavigationController = ParentNavigationController(rootViewController: friendsViewController);
        let moreNavigationController = ParentNavigationController(rootViewController: moreViewController);
        
        self.viewControllers = [activeTripNavigationController, trackingNavigationController, friendsNavigationController, moreNavigationController];
        
        self.tabBar.tintColor = Style.mainColor;
        UITabBar.appearance().tintColor = Style.mainColor;
        self.tabBar.isTranslucent = false;
    }
}
