//
//  AppDelegate.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 2/3/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import GooglePlaces
import Firebase
import Reachability
import SwiftMessages
import UserNotifications
import Siren

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var reachibility = Reachability()
    var wasUnreachable = false;
    let gcmMessageIDKey = "gcm.message_id";
    var currentRide: Ride?
    
    override init() {
        if UserDefaults.standard.value(forKey: KeyStrings.currentLanguageCode) == nil {
            LanguageManager.currentLanguage = .english;
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure();
        // Override point for customization after application launch.
        GMSServices.provideAPIKey(KeyStrings.googleAPIKey);
        GMSPlacesClient.provideAPIKey(KeyStrings.googleAPIKey);
        
        Style.setupTheme(theme: .default_);
        
        window = UIWindow(frame: UIScreen.main.bounds);
        window?.rootViewController = TabBarViewController();
        window?.makeKeyAndVisible();
        
        // Version Check
        // Siren is a singleton
        let siren = Siren.shared
        
        // Optional: Defaults to .option
        siren.alertType = .force;
        
        // Optional: Change the various UIAlertController and UIAlertAction messaging. One or more values can be changes. If only a subset of values are changed, the defaults with which Siren comes with will be used.
        siren.alertMessaging = SirenAlertMessaging(updateTitle: "UpdateAvailable".localized,
                                                   updateMessage: "updateDescription".localized,
                                                   updateButtonMessage: "update".localized,
                                                   nextTimeButtonMessage: "NextTime".localized);
        
        
        // Optional: Set this variable if you would only like to show an alert if your app has been available on the store for a few days.
        // This default value is set to 1 to avoid this issue: https://github.com/ArtSabintsev/Siren#words-of-caution
        // To show the update immediately after Apple has updated their JSON, set this value to 0. Not recommended due to aforementioned reason in https://github.com/ArtSabintsev/Siren#words-of-caution.
        siren.showAlertAfterCurrentVersionHasBeenReleasedForDays = 3
        
        // Replace .immediately with .daily or .weekly to specify a maximum daily or weekly frequency for version checks.
        // DO NOT CALL THIS METHOD IN didFinishLaunchingWithOptions IF YOU ALSO PLAN TO CALL IT IN applicationDidBecomeActive.
        siren.checkVersion(checkType: .immediately)
        
        // checking internet connection
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: Notification.Name.reachabilityChanged, object: reachibility)
        
        do { try reachibility?.startNotifier() }
        catch {
            //            PopupMessages.showThinyWarningMessage(message: Texts.couldNotStartReachablility);
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications();
        Messaging.messaging().delegate = self;
        Messaging.messaging().shouldEstablishDirectChannel = true;
        
        checkLogin();
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    
    // For iOS 9+
    func application(_ application: UIApplication, open url: URL,
                     options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        // URL not auth related, developer should handle it.
        return true;
    }
    
    // For iOS 8-
    func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?,
                     annotation: Any) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        // URL not auth related, developer should handle it.
        return true;
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        SocketIOManager.sharedInstance.closeConnection();
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        SocketIOManager.sharedInstance.establishConnection();
        Siren.shared.checkVersion(checkType: .immediately);
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        SocketIOManager.sharedInstance.establishConnection();
        if let userid = Values.currentUser?.id {
            SocketIOManager.sharedInstance.emitWelcome("welcome", ["user_id": userid]);
            print("USer id sent: ", userid);
        }
        
        Siren.shared.checkVersion(checkType: .daily);
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Zamanat")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    @objc func reachabilityChanged() {
        print("Reachability changed");
        if(!Utilities.isInternetAvailable()){
            print("Not reachable")
            let popupMessage = PopupMessages.getThinyForeverErrorMessage(message: "noInternetAccess".localized);
            popupMessage.view.id = "internetPopup";
            SwiftMessages.sharedInstance.show(config: popupMessage.config, view: popupMessage.view);
            self.wasUnreachable = true;
        } else {
            print("reachable")
            if(self.wasUnreachable){
                if let userId = Values.currentUser?.id {
                    let socketData = ["user_id": userId];
                    SocketIOManager.sharedInstance.emitWelcome("welcome", socketData);
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadActiveTrip"), object: nil);
                self.wasUnreachable = false;
            }
            SwiftMessages.sharedInstance.hide(id: "internetPopup");
        }
    }
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate {
    func checkLogin(){
        if (Values.currentUser == nil){
            print("User nil");
            let loginViewController = WelcomeViewController();
            let navigationController = ParentNavigationController(rootViewController: loginViewController);
            self.window?.rootViewController?.present(navigationController, animated: true, completion: nil);
        }
    }
}

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)");
        KeyStrings.fcmToken = fcmToken;
        Helpers.updateFcmTocke(token: fcmToken);
        self.checkFCMSubmit(fcmToken: fcmToken);
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)");
    }
    // [END ios_10_data_message]
    
    func checkFCMSubmit(fcmToken: String) {
        Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { (timer) in
            if !Values.isFcmTokenSent {
                Helpers.updateFcmTocke(token: fcmToken);
            } else {
                timer.invalidate();
            }
        }
    }
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        // when app is active, we receive notification here
        //        print("willPresent notification - User info__: ", userInfo)
        handleNotificationWithData(userInfo: userInfo);
        Sound.play(file: "buber.mp3");
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        // change .sandbox to .prod before building for App Store
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod);
        Messaging.messaging().setAPNSToken(deviceToken, type: .prod);
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        handleNotificationWithData(userInfo: userInfo);
        
        completionHandler()
    }
    
    fileprivate func handleNotificationWithData(userInfo: [AnyHashable: Any]) {
        print("Received notification: ", userInfo);
        guard let theData = userInfo as? [String: Any], let type = theData["notification_type"] as? String, let appDataString = userInfo["appData"] as? String else {
            print("cannot convert anyhashable");
            return;
        }
        
        let topController = UIApplication.shared.topMostViewController();
        guard let appData = Helpers.convertToDictionary(text: appDataString) else { return }
        
        print("App Data__: ", appData);
        
        if let notification = NotificationType(rawValue: type) {
            switch notification {
            case .driver_response: // --------------------------------
                ActiveTripViewController.statusText = "RideAcceptedWait".localized;
                
                currentRide = Ride(dic: appData);
                
                if let driverDic = appData["driver"] as? [String: Any] {
                    let driver = Driver(dic: driverDic);
                    driver.distanceToUser = appData["distance"] as? String;
                    driver.durationToUser = appData["duration"] as? String;
                    currentRide?.driver = driver;
                    ActiveTripViewController.activeTrip?.ride?.driver = driver;
                    
                    if let driverDisplayName = driver.displayName {
                        PopupMessages.showSuccessMessage(message: driverDisplayName + "AcceptedYourRide".localized);
                    }
                }
                
            case .pay_for_ride_cash: // -------------------------
                print("pay_for_ride_cash");
                if let aps = userInfo["aps"] as? NSDictionary {
                    if let alert = aps["alert"] as? NSDictionary {
                        if let alertMessage = alert["body"] as? String {
                            PopupMessages.showSuccessMessage(message: alertMessage);
                        }
                    }
                }
                
            case .create_trip_or_track: // -------------------------
                print("pay_for_ride_cash");
                if let aps = userInfo["aps"] as? NSDictionary {
                    if let alert = aps["alert"] as? NSDictionary {
                        if let alertMessage = alert["body"] as? String {
                            PopupMessages.showSuccessMessage(message: alertMessage);
                        }
                    }
                }
                
            case .pay_for_ride_wallet: // -------------------------
                print("pay_for_ride_wallet");
                ActiveTripViewController.statusText = "PaymentSucceededEnjoy".localized;
                if let aps = userInfo["aps"] as? NSDictionary {
                    if let alert = aps["alert"] as? NSDictionary {
                        if let alertMessage = alert["body"] as? String {
                            PopupMessages.showSuccessMessage(message: alertMessage);
                        }
                    }
                }
                
            case .req_pay_for_ride: // --------------------------
                
                print("Req Pay for ride, ", appData, "\n\n\n\n\n");
                ActiveTripViewController.statusText = "PleasePayForRide".localized;
                if let amount = appData["amount"] as? Double, let currency = appData["currency"] as? String, let id = appData["_id"] as? String, let tripId = appData["trip_id"] as? String, let userId = Values.currentUser?.id, let toUserId = appData["user_id"] as? String, let toAddress = appData["to_addr"] as? String, let distance = appData["distance"] as? String, let duration = appData["duration"] as? String, let user = appData["user"] as? [String: Any] {
                    
                    let paymentDetails = ["amount": amount, "currency": currency, "_id": id, "trip_id": tripId, "user_id": userId, "to_user_id": toUserId, "to_addr": toAddress, "distance": distance, "duration": duration, "user": user] as [String : Any];
                    
                    ActiveTripViewController.activeTrip?.ride?.paymentDetails = paymentDetails;
                    
                    UserDefaults.standard.setValue(paymentDetails, forKey: "paymentDetails");
                    
                    let payforRidePopup = PayForRidePopup();
                    let rideDetailsPayment = RideDetailsPayment(dic: appData);
                    payforRidePopup.ridePaymentDetails = rideDetailsPayment;
                    payforRidePopup.modalTransitionStyle = .coverVertical;
                    payforRidePopup.modalPresentationStyle = .overFullScreen;
                    topController?.present(payforRidePopup, animated: true, completion: nil);
                }
                
            case .received_cash: // ---------------------------
                print("received cash");
                ActiveTripViewController.statusText = "PaymentSucceededEnjoy".localized;
                if let aps = userInfo["aps"] as? NSDictionary {
                    if let alert = aps["alert"] as? NSDictionary {
                        if let alertMessage = alert["body"] as? String {
                            PopupMessages.showSuccessMessage(message: alertMessage);
                        }
                    }
                }
                
            case .user_ignored_ride_payment: // ---------------------------
                print("user ignored payment");
                NotificationCenter.default.post(name: Notification.Name(rawValue: "cancelTrip"), object: nil);
                ActiveTripViewController.statusText = "NoRide".localized;
                if let aps = userInfo["aps"] as? NSDictionary {
                    if let alert = aps["alert"] as? NSDictionary {
                        if let alertMessage = alert["body"] as? String {
                            PopupMessages.showSuccessMessage(message: alertMessage);
                        }
                    }
                }
                
            case .drop_off: // ------------------------------------
                print("Drop off user");
                // rate the driver
                let rateDriverPopup = RateDriverViewController();
                rateDriverPopup.ride = currentRide == nil ? ActiveTripViewController.activeTrip?.ride : currentRide;
                rateDriverPopup.modalTransitionStyle = .coverVertical;
                rateDriverPopup.modalPresentationStyle = .overFullScreen;
                topController?.present(rateDriverPopup, animated: true, completion: nil);
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "completeTrip"), object: nil);
                
                ActiveTripViewController.statusText = "NoRide".localized;
                if let aps = userInfo["aps"] as? NSDictionary {
                    if let alert = aps["alert"] as? NSDictionary {
                        if let alertMessage = alert["body"] as? String {
                            PopupMessages.showSuccessMessage(message: alertMessage);
                        }
                    }
                }
                
            case .cancel_ride_by_driver: // ---------------------------
                print("ride cancelled by the driver");
                
                ActiveTripViewController.statusText = "RideCancelledByDriver".localized;
                NotificationCenter.default.post(name: Notification.Name(rawValue: "completeTrip"), object: nil);
                
                if let aps = userInfo["aps"] as? NSDictionary {
                    if let alert = aps["alert"] as? NSDictionary {
                        if let alertMessage = alert["body"] as? String {
                            if let _  = UIApplication.shared.topMostViewController() as? ParentNavigationController, let _ = UIApplication.shared.topMostViewController() as? ActiveTripViewController, let _ = UIApplication.shared.topMostViewController() as? TabBarViewController {
                                UIApplication.shared.topMostViewController()?.dismiss(animated: true, completion: nil);
                                PopupMessages.showWarningMessage(message: alertMessage);
                            } else {
                                UIApplication.shared.topMostViewController()?.dismiss(animated: true, completion: nil);
                                PopupMessages.showWarningMessage(message: alertMessage);
                            }
                        }
                    }
                }
                
            case .trip_update_status: // ---------------------------
                ActiveTripViewController.statusText = "NoRide".localized;
                
            case .other: // ----------------------------------------
                print("Other notification type");
                
            }
        }
        // remove delivered notifications
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if( Auth.auth().canHandleNotification(userInfo)) {
            completionHandler(.noData);
        }
        
        print("didReceiveRemoteNotification - User dataaa: ", userInfo);
    }
}

