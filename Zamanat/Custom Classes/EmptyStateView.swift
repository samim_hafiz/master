import UIKit

class EmptyStateView: UIView {
    
    let iconImageView: UIImageView = {
        let iamgeView = UIImageView();
        iamgeView.contentMode = .scaleAspectFit;
        iamgeView.translatesAutoresizingMaskIntoConstraints = false;
        return iamgeView;
    }();
    
    let titleLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalBoldFont;
        label.textColor = UIColor.black.withAlphaComponent(0.7);
        label.text = "noInfoFound".localized;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let descriptionLabel: UILabel = {
        let label = UILabel();
        label.numberOfLines = 0;
        label.font = Style.smallBoldFont;
        label.textColor = Style.grayColor;
        //        label.text = "The table is empty. Checking your internet connection and refreshing the page may help to retrieve new data from the server.";
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }()
    
    convenience init(image: UIImage = #imageLiteral(resourceName: "emptyState"), message: String) {
        self.init(frame: CGRect.zero);
        self.iconImageView.image = image;
        self.descriptionLabel.text = message;
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        self.addSubview(iconImageView);
        self.addSubview(titleLabel);
        self.addSubview(descriptionLabel);
        
        setupConstraints();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints(){
        iconImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 42).isActive = true;
        iconImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true;
        iconImageView.widthAnchor.constraint(equalToConstant: 65).isActive = true;
        iconImageView.heightAnchor.constraint(equalToConstant: 65).isActive = true;
        
        titleLabel.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: 24).isActive = true;
        titleLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8).isActive = true;
        titleLabel.heightAnchor.constraint(equalToConstant: 32).isActive = true;
        titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true;
        
        descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4).isActive = true;
        descriptionLabel.widthAnchor.constraint(equalTo: titleLabel.widthAnchor, multiplier: 0.9).isActive = true;
        descriptionLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true;
    }
}


class EmptyMessageView: UIView {
    
    let descriptionLabel: UILabel = {
        let label = UILabel();
        label.numberOfLines = 0;
        label.font = Style.smallBoldFont;
        label.textColor = Style.grayColor;
        //        label.text = "The table is empty. Checking your internet connection and refreshing the page may help to retrieve new data from the server.";
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }()
    
    convenience init(message: String) {
        self.init(frame: CGRect.zero);
        self.descriptionLabel.text = message;
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        self.addSubview(descriptionLabel);
        
        setupConstraints();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints(){
        
        descriptionLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
        descriptionLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9).isActive = true;
        descriptionLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true;
        descriptionLabel.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.9).isActive = true;
    }
}



