//
//  RankingStars.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 2/17/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class RankingStars: UIView {
    
    lazy var star1Button: UIButton = {
        let button = UIButton(type: .custom);
         button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
        button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7);
        button.addTarget(self, action: #selector(starTapped(_:)), for: .touchUpInside);
        button.tag = 1;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    lazy var star2Button: UIButton = {
        let button = UIButton(type: .custom);
        button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
        button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7);
        button.addTarget(self, action: #selector(starTapped(_:)), for: .touchUpInside);
        button.tag = 2;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    lazy var star3Button: UIButton = {
        let button = UIButton(type: .custom);
        button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
        button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7);
        button.addTarget(self, action: #selector(starTapped(_:)), for: .touchUpInside);
        button.tag = 3;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    lazy var star4Button: UIButton = {
        let button = UIButton(type: .custom);
         button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
        button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7);
        button.addTarget(self, action: #selector(starTapped(_:)), for: .touchUpInside);
        button.tag = 4;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    lazy var star5Button: UIButton = {
        let button = UIButton(type: .custom);
        button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
        button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7);
        button.addTarget(self, action: #selector(starTapped(_:)), for: .touchUpInside);
        button.tag = 5;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        self.addSubview(star1Button);
        self.addSubview(star2Button);
        self.addSubview(star3Button);
        self.addSubview(star4Button);
        self.addSubview(star5Button);
        
        setupConstraints();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var rating = 0;
    @objc private func starTapped(_ button: UIButton) {
            print("Tag of the view: ", button.tag);
        rating = button.tag;
        
        switch button.tag {
        case 1:
            self.star1Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star2Button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            self.star3Button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            self.star4Button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            self.star5Button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            
        case 2:
            self.star1Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star2Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star3Button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            self.star4Button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            self.star5Button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            
        case 3:
            self.star1Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star2Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star3Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star4Button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            self.star5Button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            
        case 4:
            self.star1Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star2Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star3Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star4Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star5Button.setImage(#imageLiteral(resourceName: "fav_empty"), for: .normal);
            
        case 5:
            self.star1Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star2Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star3Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star4Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            self.star5Button.setImage(#imageLiteral(resourceName: "fav_fill"), for: .normal);
            
        default:
            return;
        }
        
        self.layoutIfNeeded();
    }
    
    private func setupConstraints(){
        //        star1ImageView
        star1Button.rightAnchor.constraint(equalTo: star2Button.leftAnchor, constant: -2).isActive = true;
        star1Button.widthAnchor.constraint(equalToConstant: 36).isActive = true;
        star1Button.heightAnchor.constraint(equalToConstant: 36).isActive = true;
        star1Button.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
        
        //        star2ImageView
        star2Button.rightAnchor.constraint(equalTo: star3Button.leftAnchor, constant: -2).isActive = true;
        star2Button.widthAnchor.constraint(equalToConstant: 36).isActive = true;
        star2Button.heightAnchor.constraint(equalToConstant: 36).isActive = true;
        star2Button.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
        
        //        star3ImageView
        star3Button.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true;
        star3Button.widthAnchor.constraint(equalToConstant: 36).isActive = true;
        star3Button.heightAnchor.constraint(equalToConstant: 36).isActive = true;
        star3Button.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
        
        //        star4ImageView
        star4Button.leftAnchor.constraint(equalTo: star3Button.rightAnchor, constant: 2).isActive = true;
        star4Button.widthAnchor.constraint(equalToConstant: 36).isActive = true;
        star4Button.heightAnchor.constraint(equalToConstant: 36).isActive = true;
        star4Button.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
        
        //        star5ImageView
        star5Button.leftAnchor.constraint(equalTo: star4Button.rightAnchor, constant: 2).isActive = true;
        star5Button.widthAnchor.constraint(equalToConstant: 36).isActive = true;
        star5Button.heightAnchor.constraint(equalToConstant: 36).isActive = true;
        star5Button.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
    }
}
