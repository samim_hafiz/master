//
//  FeedLoader.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 3/17/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import Lottie

class FeedLoader: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    var firstProgressView: LOTAnimationView!
    var secondProgressView: LOTAnimationView!
    var thirdProgressView: LOTAnimationView!
    
    convenience init(fileName: String) {
        self.init(frame: CGRect.zero);
        
        firstProgressView = LOTAnimationView(filePath: fileName);
        firstProgressView.loopAnimation = true;
//        firstProgressView.contentMode = .scaleAspectFill;
        firstProgressView.translatesAutoresizingMaskIntoConstraints = false;
        
        secondProgressView = LOTAnimationView(filePath: fileName);
        secondProgressView.loopAnimation = true;
//        secondProgressView.contentMode = .scaleAspectFill;
        secondProgressView.translatesAutoresizingMaskIntoConstraints = false;
        
        thirdProgressView = LOTAnimationView(filePath: fileName);
        thirdProgressView.loopAnimation = true;
        thirdProgressView.contentMode = .scaleAspectFill;
         thirdProgressView.translatesAutoresizingMaskIntoConstraints = false;
        
        self.addSubview(firstProgressView);
//        self.addSubview(secondProgressView);
//        self.addSubview(thirdProgressView);
        
        firstProgressView.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true;
        firstProgressView.heightAnchor.constraint(equalToConstant: 200).isActive = true;
        firstProgressView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true;
        firstProgressView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true;
        
//        secondProgressView.topAnchor.constraint(equalTo: firstProgressView.bottomAnchor).isActive = true;
//        secondProgressView.heightAnchor.constraint(equalTo: firstProgressView.heightAnchor).isActive = true;
//        secondProgressView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true;
//        secondProgressView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true;
//
//        thirdProgressView.topAnchor.constraint(equalTo: secondProgressView.bottomAnchor).isActive = true;
//        thirdProgressView.heightAnchor.constraint(equalTo: firstProgressView.heightAnchor).isActive = true;
//        thirdProgressView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true;
//        thirdProgressView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true;
        
    }
    
    func play() {
        print("Animation played");
        self.firstProgressView.play { (completed) in
            print(completed);
        }
//        self.secondProgressView.play();
//        self.thirdProgressView.play();
    }
    
    func stop() {
        self.firstProgressView.stop();
//        self.secondProgressView.stop();
//        self.thirdProgressView.stop();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
