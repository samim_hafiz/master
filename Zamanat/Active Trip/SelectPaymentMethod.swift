//
//  SelectPaymentMethod.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 6/9/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

//
//  PayForRidePopup.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 6/6/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class SelectPaymentMethod: UIViewController {
    var paymentDetails: [String: Any]?;
    var paymentTaype: Int = -1;
    
    let popupView: UIView = {
        let view = UIView();
        view.layer.cornerRadius = 3;
        view.layer.masksToBounds = true;
        view.backgroundColor = .white;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let titleLabel: UILabel = {
        let label = UILabel();
        label.text = "PaymentMethod".localized;
        label.adjustsFontSizeToFitWidth = true;
        label.textColor = Style.mainColor;
        label.font = Style.veryBigFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var walletTitleLabel: UILabel = {
        let label = UILabel();
        let tap = UITapGestureRecognizer(target: self, action: #selector(walletTapped));
        label.isUserInteractionEnabled = true;
        label.addGestureRecognizer(tap);
        label.text = "Wallet".localized;
        label.textAlignment = .left;
        label.font = Style.normalFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let walletIcon: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "wallet").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    lazy var cashTitleLabel: UILabel = {
        let label = UILabel();
        let tap = UITapGestureRecognizer(target: self, action: #selector(cashTapped));
        label.addGestureRecognizer(tap);
        label.isUserInteractionEnabled = true;
        label.text = "Cash".localized;
        label.textAlignment = .left;
        label.font = Style.normalFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let checkMarkImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.contentMode = .scaleAspectFit;
        imageView.image = #imageLiteral(resourceName: "checkmark").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let cashIcon: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "cash").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let firstSeperatorView: UIView = {
        let view = UIView();
        view.backgroundColor = Style.mainColor;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let secondSeperatorView: UIView = {
        let view = UIView();
        view.backgroundColor = Style.mainColor;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    lazy var payButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(proceedPayment), for: .touchUpInside);
        button.setTitle("Pay".localized.uppercased(), for: .normal);
        button.backgroundColor = Style.mainColor;
        button.setTitleColor(.white, for: .normal);
        button.translatesAutoresizingMaskIntoConstraints = false;
        return  button;
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.view.backgroundColor = UIColor.darkGray.withAlphaComponent(0.4);
        
        view.addSubview(popupView);
        popupView.addSubview(titleLabel);
        popupView.addSubview(firstSeperatorView);
        popupView.addSubview(walletIcon);
        popupView.addSubview(walletTitleLabel);
        popupView.addSubview(secondSeperatorView);
        popupView.addSubview(cashIcon);
        popupView.addSubview(cashTitleLabel);
        popupView.addSubview(walletIcon);
        popupView.addSubview(payButton);
        popupView.addSubview(checkMarkImageView);
        
        listenToPayForRide();
        setupConstraints();
    }
    
    @objc private func proceedPayment() {
        if self.paymentTaype == -1 {
            PopupMessages.showInfoMessage(message: "SelectaPaymentMethod".localized);
            return
        }
        
        var paymentDetails = [String: Any]()
        
        if let _paymentDetails = UserDefaults.standard.value(forKey: "paymentDetails") as? [String: Any], !_paymentDetails.isEmpty {
            print("Payment Details: ", paymentDetails);
             paymentDetails = _paymentDetails;
            paymentDetails["payment_type"] = paymentTaype;
            finishPayment(paymentDetails);
        } else {
            self.getPaymentDetailsFromRide(paymentType: paymentTaype);
        }
    }
    
    private func getPaymentDetailsFromRide(paymentType: Int) {
        guard let userId = Values.currentUser?.id else { return }
        
        let parameters = ["user_id": userId];
        var paymentDetails = [String: Any]()
        self.view.startActivityIndicator();
        Helpers.request(api: API.getCurrentActiveTrip, method: .post, parameters: parameters) { (result) in
            self.view.stopActivityIndicator();
            
            guard let result = result else { return }
            
            print("Load active trip_: ", result);
            
            if let success = result["success"] as? Bool, success {
                guard let data = result["data"] as? [String: Any] else { return }
                var userDic = [String: Any]();
                let trip = Trip(dic: data);
                if let userId = trip.user?.id, let displayName = trip.user?.displayName {
                    let phoneNumber = trip.user?.phone ?? "0799";
                    let thumbNail = trip.user?.thumbUrl ?? "";
                    userDic = ["display_name": displayName, "thumb_url": thumbNail, "_id": userId, "phone": phoneNumber];
                }
                
                if let amount = trip.ride?.price, let currency = trip.ride?.currency, let id = trip.ride?.id, let tripId = trip.id, let userId = trip.user?.id, let toUserId = trip.ride?.driver?.id, let toAddress = trip.toAddress, let distance = trip.distance, let duration = trip.duration {
                    var paymentData = ["amount": amount, "currency": currency, "_id": id, "trip_id": tripId, "user_id": userId, "to_user_id": toUserId, "to_addr": toAddress, "distance": distance.replacingOccurrences(of: "km", with: "").replacingOccurrences(of: "کیلو متر", with: "").replacingOccurrences(of: " ", with: ""), "duration": duration.replacingOccurrences(of: "min", with: "").replacingOccurrences(of: "دقیقه", with: "").replacingOccurrences(of: " ", with: ""), "user": userDic] as [String : Any];
                    
                    
                    print("\n\n\nPayment Details__: ", paymentData);
                    
                    paymentData["payment_type"] = paymentType;
                    self.finishPayment(paymentData);
                    
                }
            }
        }
    }
    
    private func finishPayment(_ paymentDetails: [String: Any]) {
        SocketIOManager.sharedInstance.emit("pay-for-ride", paymentDetails);
        print("Emitted success with data: ", paymentDetails);
        self.view.startActivityIndicator();
    }
    
    private func listenToPayForRide() {
        SocketIOManager.sharedInstance.socket.on("pay-for-ride") { (result, _) in
            self.view.stopActivityIndicator();
            print("\n\n\nPay-for-Ride response: ", result);
            
            if let response = result as? Array<[String: Any]> {
                let message = response[0]["message"] as? String ?? "";
                if let success = response[0]["success"] as? Bool, success {
                    if let data = response[0]["data"] as? [String: Any], let paymentType = data["payment_type"] as? Int, let type = PaymentMethod(rawValue: paymentType), type == .cash {
                        ActiveTripViewController.statusText = "WaitForDriverToAcceptPayment".localized;
                    }
                    self.dismiss(animated: true, completion: nil);
                    UIApplication.shared.topMostViewController()?.dismiss(animated: true, completion: nil);
                } else {
                    PopupMessages.showWarningMessage(message: message);
                }
            }
        }
    }
    
    @objc private func cashTapped() {
        self.paymentTaype = 1;
        self.checkmarkCenterYAnchor?.isActive = false;
        self.checkmarkCenterYAnchor = checkMarkImageView.centerYAnchor.constraint(equalTo: cashIcon.centerYAnchor);
        self.checkmarkCenterYAnchor?.isActive = true;
    }
    
    @objc private func walletTapped() {
        self.paymentTaype = 2;
        self.checkmarkCenterYAnchor?.isActive = false;
        self.checkmarkCenterYAnchor = checkMarkImageView.centerYAnchor.constraint(equalTo: walletIcon.centerYAnchor);
        self.checkmarkCenterYAnchor?.isActive = true;
    }
    
    var checkmarkCenterYAnchor: NSLayoutConstraint?
    private func setupConstraints() {
        popupView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7).isActive = true;
        popupView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.45).isActive = true;
        popupView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true;
        popupView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        titleLabel.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        titleLabel.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 30).isActive = true;
        titleLabel.widthAnchor.constraint(equalTo: popupView.widthAnchor, multiplier: 0.8).isActive = true;
        
        firstSeperatorView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30).isActive = true;
        firstSeperatorView.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        firstSeperatorView.widthAnchor.constraint(equalTo: popupView.widthAnchor).isActive = true;
        firstSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true;
        
        walletIcon.topAnchor.constraint(equalTo: firstSeperatorView.bottomAnchor, constant: 16).isActive = true;
        walletIcon.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 16).isActive = true;
        walletIcon.heightAnchor.constraint(equalToConstant: 36).isActive = true;
        walletIcon.widthAnchor.constraint(equalToConstant: 36).isActive = true;
        
        walletTitleLabel.leadingAnchor.constraint(equalTo: walletIcon.trailingAnchor, constant: 10).isActive = true;
        walletTitleLabel.centerYAnchor.constraint(equalTo: walletIcon.centerYAnchor).isActive = true;
        walletTitleLabel.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -20).isActive = true;
        walletTitleLabel.heightAnchor.constraint(equalTo: walletIcon.heightAnchor).isActive = true;
        
        secondSeperatorView.topAnchor.constraint(equalTo: walletIcon.bottomAnchor, constant: 16).isActive = true;
        secondSeperatorView.centerXAnchor.constraint(equalTo: firstSeperatorView.centerXAnchor).isActive = true;
        secondSeperatorView.widthAnchor.constraint(equalTo: firstSeperatorView.widthAnchor).isActive = true;
        secondSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true;
        
        cashIcon.topAnchor.constraint(equalTo: secondSeperatorView.bottomAnchor, constant: 16).isActive = true;
        cashIcon.centerXAnchor.constraint(equalTo: walletIcon.centerXAnchor).isActive = true;
        cashIcon.heightAnchor.constraint(equalTo: walletIcon.heightAnchor).isActive = true;
        cashIcon.widthAnchor.constraint(equalTo: walletIcon.widthAnchor).isActive = true;
        
        cashTitleLabel.leadingAnchor.constraint(equalTo: walletTitleLabel.leadingAnchor).isActive = true;
        cashTitleLabel.centerYAnchor.constraint(equalTo: cashIcon.centerYAnchor).isActive = true;
        cashTitleLabel.trailingAnchor.constraint(equalTo: walletTitleLabel.trailingAnchor).isActive = true;
        cashTitleLabel.heightAnchor.constraint(equalTo: cashIcon.heightAnchor).isActive = true;
        
        payButton.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        payButton.bottomAnchor.constraint(equalTo: popupView.bottomAnchor).isActive = true;
        payButton.widthAnchor.constraint(equalTo: popupView.widthAnchor).isActive = true;
        payButton.heightAnchor.constraint(equalToConstant: 50).isActive = true;
        
        checkMarkImageView.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -16).isActive = true;
        checkMarkImageView.heightAnchor.constraint(equalToConstant: 16).isActive = true;
        checkMarkImageView.widthAnchor.constraint(equalToConstant: 16).isActive = true;
    }
}

