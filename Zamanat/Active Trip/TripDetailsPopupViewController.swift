//
//  DetailsPopupViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 2/17/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import GoogleMaps

class TripDetailsPopupViewController: UIViewController {
    
    var trip: Trip! {
        didSet {
            self.addressLabel.text = trip.toAddress;
            self.minutesLabel.text = trip.duration! + " min";
            if let distance = trip.distance {
                let distanceInDouble = distance.replacingOccurrences(of: "km", with: "").trimmingCharacters(in: .whitespaces);
                self.distanceLabel.text = "\(Double(distanceInDouble)!/1000) km";
                if let tripCost = trip.ride?.price{
                    self.priceInUsd.text = "\(Int(tripCost))" + "AFN".localized;
                } else{
                    self.loadPrice(distance: Double(distanceInDouble)!)
                }
                
            }
            
            self.driverNameLabel.text = trip.ride?.driver?.displayName;
            if let profileUrl = trip.ride?.driver?.thumbUrl {
                self.driverImageView.setImageFromUrl(urlStr: profileUrl);
            }
            
            self.phoneNumberLabel.text = trip.ride?.driver?.phone;
            if trip.ride?.driver == nil {
                self.driverLabel.isHidden = true;
                self.driverImageView.isHidden = true;
                self.driverNotAssigned.isHidden = false;
                
            } else {
                self.driverLabel.isHidden = false;
                self.driverImageView.isHidden = false;
                self.driverNotAssigned.isHidden = true;
            }
        
            self.updateTrackingLabel();
            
            self.trackingCollectionView.reloadData();
        }
    }
    
    let popupView: UIView = {
        let view = UIView();
        view.backgroundColor = .white;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    lazy var closeButton: UIButton = {
        let button = UIButton();
        button.setImage(#imageLiteral(resourceName: "close"), for: .normal);
        button.addTarget(self, action: #selector(closePopup), for: .touchUpInside);
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    let addressLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverNotAssigned: UILabel = {
        let label = UILabel();
        label.font = Style.normalFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let minutesLabel: UILabel = {
        let label = UILabel();
        label.textColor = Style.blueColor;
        label.font = Style.smallFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceLabel: UILabel = {
        let label = UILabel();
        label.textColor = Style.blueColor;
        label.font = Style.smallFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverLabel: UILabel = {
        let label = UILabel();
        label.text = "DRIVER".localized;
        label.font = Style.smallFont;
        label.textColor = Style.grayFontColor;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let priceInUsd: UILabel = {
        let label = UILabel();
        label.textColor = Style.blueColor;
        label.font = Style.normalFont;
        label.textAlignment = .center;
        label.text = "..."
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.layer.cornerRadius = 22;
        imageView.layer.borderWidth = 2;
        imageView.image = #imageLiteral(resourceName: "default_profile").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.differColor;
        imageView.layer.borderColor = Style.mainColor.cgColor;
        imageView.layer.masksToBounds = true;
        imageView.contentMode = .scaleAspectFill;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let geoImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "geo");
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let carImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "car");
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let distanceImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "distance");
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let driverNameLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let phoneNumberLabel: UILabel = {
        let label = UILabel();
        label.font = Style.normalFont;
        label.textColor = Style.mainColor;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let seperatorLine: UIView = {
        let view = UIView();
        view.backgroundColor = Style.grayFontColor;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let trackingIconImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "tracking").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let trackingLabel: UILabel = {
        let label = UILabel();
        label.font = Style.smallFont;
        label.adjustsFontSizeToFitWidth = true;
        label.textColor = Style.grayFontColor;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var trackingCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout();
        layout.scrollDirection = .horizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout);
        collection.delegate = self;
        collection.dataSource = self;
        collection.backgroundColor = .white;
        collection.showsHorizontalScrollIndicator = false;
        collection.alwaysBounceHorizontal = true;
        collection.register(TrackerCell.self, forCellWithReuseIdentifier: cellIdetifier);
        collection.translatesAutoresizingMaskIntoConstraints = false;
        return collection;
    }();
    
    lazy var cancelTripButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(cancelTrip), for: .touchUpInside);
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
        button.titleLabel?.adjustsFontSizeToFitWidth = true;
        button.titleLabel?.font = Style.smallFont;
        button.setTitle("CancelRide".localized, for: .normal);
        button.layer.cornerRadius = 3;
        button.layer.masksToBounds = true;
        button.backgroundColor = Style.mainColor;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.2);
        
        view.addSubview(popupView);
        
        popupView.addSubview(closeButton);
        popupView.addSubview(cancelTripButton);
        popupView.addSubview(priceInUsd);
        popupView.addSubview(addressLabel);
        popupView.addSubview(geoImageView);
        popupView.addSubview(carImageView);
        popupView.addSubview(distanceLabel);
        popupView.addSubview(minutesLabel);
        popupView.addSubview(distanceImageView);
        popupView.addSubview(driverLabel);
        popupView.addSubview(driverImageView);
        popupView.addSubview(driverNameLabel);
        popupView.addSubview(phoneNumberLabel);
        popupView.addSubview(seperatorLine);
        popupView.addSubview(trackingIconImageView);
        popupView.addSubview(trackingLabel);
        popupView.addSubview(trackingCollectionView);
        popupView.addSubview(driverNotAssigned);
        
        setupConstraints();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.driverNotAssigned.text = Values.numberOfDiverMessage.uppercased();
    }
    
    @objc private func cancelTrip() {
        if(!Helpers.isInternetAccessAvailable()) {
            PopupMessages.showInfoMessage(title: "", message: "noInternetAccess".localized);
            return;
        }
        
        let alert = UIAlertController(title: "CancelRide".localized, message: "CancelRideMessage".localized, preferredStyle: .alert);
        let yesAction = UIAlertAction(title: "Yes".localized, style: .default, handler: { (_) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cancelTrip"), object: nil);
            self.dismiss(animated: true, completion: nil);
        });
        
        let cancelAction = UIAlertAction(title: "No".localized, style: .cancel, handler: nil);
        alert.addAction(yesAction);
        alert.addAction(cancelAction);
        self.present(alert, animated: true, completion: nil);
    }
    
    private func setupConstraints(){
        
        popupView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true;
        popupView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true;
        popupView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        popupView.heightAnchor.constraint(equalToConstant: 330).isActive = true;
        
//        closeButton
        closeButton.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 10).isActive = true;
        closeButton.widthAnchor.constraint(equalToConstant: 32).isActive = true;
        closeButton.heightAnchor.constraint(equalToConstant: 32).isActive = true;
        closeButton.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        
//        cancelTripButton
        cancelTripButton.leftAnchor.constraint(equalTo: popupView.leftAnchor, constant: 16).isActive = true;
        cancelTripButton.centerYAnchor.constraint(equalTo: closeButton.centerYAnchor).isActive = true;
        cancelTripButton.heightAnchor.constraint(equalToConstant: 24).isActive = true;
        cancelTripButton.widthAnchor.constraint(equalToConstant: 66).isActive = true;
        
//        priceInUsd
        priceInUsd.rightAnchor.constraint(equalTo: popupView.rightAnchor, constant: -20).isActive = true;
        priceInUsd.centerYAnchor.constraint(equalTo: cancelTripButton.centerYAnchor).isActive = true;
        
//        addressLabel
        addressLabel.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: 16).isActive = true;
        addressLabel.widthAnchor.constraint(equalTo: popupView.widthAnchor, multiplier: 0.9).isActive = true;
        addressLabel.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        
//        geoImageView
        geoImageView.leftAnchor.constraint(equalTo: addressLabel.leftAnchor, constant: -4).isActive = true;
        geoImageView.topAnchor.constraint(equalTo: addressLabel.bottomAnchor, constant: 20).isActive = true;
        geoImageView.widthAnchor.constraint(equalToConstant: 26).isActive = true;
        geoImageView.heightAnchor.constraint(equalToConstant: 26).isActive = true;
        
//        carImageView
        carImageView.centerYAnchor.constraint(equalTo: geoImageView.centerYAnchor).isActive = true;
        carImageView.leftAnchor.constraint(equalTo: geoImageView.rightAnchor, constant: 20).isActive = true;
        carImageView.heightAnchor.constraint(equalToConstant: 22).isActive = true;
        carImageView.widthAnchor.constraint(equalToConstant: 22).isActive = true;
        
//        minutesLabel
        minutesLabel.leftAnchor.constraint(equalTo: carImageView.rightAnchor, constant: 10).isActive = true;
        minutesLabel.centerYAnchor.constraint(equalTo: carImageView.centerYAnchor).isActive = true;
        minutesLabel.widthAnchor.constraint(equalToConstant: 66).isActive = true;
        
//        distanceImageView
        distanceImageView.leftAnchor.constraint(equalTo: minutesLabel.rightAnchor, constant: 8).isActive = true;
        distanceImageView.centerYAnchor.constraint(equalTo: carImageView.centerYAnchor).isActive = true;
        distanceImageView.widthAnchor.constraint(equalToConstant: 25).isActive = true;
        distanceImageView.heightAnchor.constraint(equalToConstant: 25).isActive = true;
        
//        distanceLabel
        distanceLabel.leftAnchor.constraint(equalTo: distanceImageView.rightAnchor, constant: 10).isActive = true;
        distanceLabel.centerYAnchor.constraint(equalTo: carImageView.centerYAnchor).isActive = true;
        distanceLabel.widthAnchor.constraint(equalToConstant: 66).isActive = true;
        
//        driverNotAssigned
        driverNotAssigned.topAnchor.constraint(equalTo: driverImageView.topAnchor).isActive = true;
        driverNotAssigned.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        
//        driverLabel
        driverLabel.topAnchor.constraint(equalTo: carImageView.bottomAnchor, constant: 20).isActive = true;
        driverLabel.centerXAnchor.constraint(equalTo: driverImageView.centerXAnchor).isActive = true;
        
//        driverImageView
        driverImageView.topAnchor.constraint(equalTo: driverLabel.bottomAnchor, constant: 12).isActive = true;
        driverImageView.leftAnchor.constraint(equalTo: geoImageView.leftAnchor).isActive = true;
        driverImageView.heightAnchor.constraint(equalToConstant: 44).isActive = true;
        driverImageView.widthAnchor.constraint(equalToConstant: 44).isActive = true;
        
//        driverNameLabel
        driverNameLabel.leftAnchor.constraint(equalTo: driverImageView.rightAnchor, constant: 16).isActive = true;
        driverNameLabel.topAnchor.constraint(equalTo: driverImageView.topAnchor, constant: 2).isActive = true;
        driverNameLabel.heightAnchor.constraint(equalTo: driverImageView.heightAnchor, multiplier: 0.5).isActive = true;
        
//        phoneNumberLabel
        phoneNumberLabel.leftAnchor.constraint(equalTo: driverNameLabel.leftAnchor).isActive = true;
        phoneNumberLabel.topAnchor.constraint(equalTo: driverNameLabel.bottomAnchor).isActive = true;
        
//        seperatorLine
        seperatorLine.topAnchor.constraint(equalTo: driverImageView.bottomAnchor, constant: 20).isActive = true;
        seperatorLine.widthAnchor.constraint(equalTo: popupView.widthAnchor, multiplier: 0.9).isActive = true;
        seperatorLine.heightAnchor.constraint(equalToConstant: 1).isActive = true;
        seperatorLine.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        
//        trackingIconImageView
        trackingIconImageView.leftAnchor.constraint(equalTo: seperatorLine.leftAnchor).isActive = true;
        trackingIconImageView.topAnchor.constraint(equalTo: seperatorLine.bottomAnchor, constant: 2).isActive = true;
        trackingIconImageView.heightAnchor.constraint(equalToConstant: 28).isActive = true;
        trackingIconImageView.widthAnchor.constraint(equalToConstant: 28).isActive = true;
        
//        trackingLabel
        trackingLabel.leftAnchor.constraint(equalTo: trackingIconImageView.rightAnchor, constant: 8).isActive = true;
        trackingLabel.centerYAnchor.constraint(equalTo: trackingIconImageView.centerYAnchor).isActive = true;
        trackingLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -30).isActive = true;
        
//        trackingCollectionView
        trackingCollectionView.topAnchor.constraint(equalTo: trackingLabel.bottomAnchor, constant: 5).isActive = true;
        trackingCollectionView.widthAnchor.constraint(equalTo: popupView.widthAnchor, multiplier: 0.9).isActive = true;
        trackingCollectionView.heightAnchor.constraint(equalToConstant: 70).isActive = true;
        trackingCollectionView.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
    }
}

private let cellIdetifier = "AvatarCell";
extension TripDetailsPopupViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: - Collection Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let trackers = self.trip.track?.trackers {
            return trackers.count;
        } else {
            return 0;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdetifier, for: indexPath) as! TrackerCell
        cell.tracker = self.trip.track?.trackers[indexPath.row];
        cell.trackingDelegate = self;
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 56);
    }
    
    // MARK: - Functions
    
    @objc fileprivate func closePopup() {
        self.dismiss(animated: true, completion: nil);
    }
    
    private func updateTrackingLabel() {
        var count = 0;
        if let trackers = self.trip.track?.trackers {
            for tracker in trackers {
                if tracker.isTracking {
                    count += 1;
                }
            }
        }
        
        if count > 0 {
            let text = String(format: "YouAreBeingTracked".localized, count);
            self.trackingLabel.text = text;
        } else {
            self.trackingLabel.text = "YouAreNotTrackingByAnyone".localized;
        }
    }
}

extension TripDetailsPopupViewController: TrackingDelegate {
    
        func updateTracking(id: String, tracking: Bool) {
            guard let trackers = trip.track?.trackers else { return }
            if trackers.isEmpty { return }
            
            if (tracking){
                trackers.first(where: {$0.id! == id})?.isTracking = true;
            } else {
                trackers.first(where: {$0.id! == id})?.isTracking = false;
            }
            
            self.updateTrackingLabel();
    }
    
    
    // To load trip cost from server for the first time we are requesting...
    func loadPrice(distance:Double){
        let params:[String:Any] = ["distance" : distance]
        Helpers.request(api: API.fareCalculation, method: .post, parameters: params) { (result) in
            guard let result = result else { return }
            if let success = result["success"] as? Bool, success {
                guard let data = result["data"] as? [String: Any] else { return }
                
                if let price = data["cost"]{
                    self.trip.ride?.price = Float((price as? Double)!);
                    self.priceInUsd.text =  "\(Int((price as? Double)!))"
                }
            }
        }
    }
}
