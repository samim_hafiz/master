//
//  RequestTripViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 5/5/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import GoogleMaps

class RequestTripViewController: UIViewController {
    
    var trackers = [Tracker]();
    
    var destinationAddress: String? {
        didSet {
            self.addressLabel.text = destinationAddress;
        }
    }
    
    var distanceInMeters: Double? {
        didSet {
            if let distance = distanceInMeters {
                updateRidePrice(distance: distance);
            }
        }
    }
    var durationInMinutes: Int?
    
    var fromLocationId: String = "";
    var toPlaceId: String = "";
    var sourceAddress = "";
    var destinationLocation: CLLocationCoordinate2D?
    var sourceLocation: CLLocationCoordinate2D?
    
    let popupView: UIView = {
        let view = UIView();
        view.backgroundColor = .white;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    lazy var requestRideView: UIView = {
        let view = UIView();
        let tap = UITapGestureRecognizer(target: self, action: #selector(submiteRideRequst));
        view.addGestureRecognizer(tap);
        view.isUserInteractionEnabled = true;
        view.layer.cornerRadius = 15;
        view.layer.masksToBounds = true;
        view.backgroundColor = Style.mainColor;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let requestRideLabel: UILabel = {
        let label = UILabel();
        label.text = "RequestRide".localized;
        label.font = Style.smallBoldFont;
        label.textColor = .white;
        label.adjustsFontSizeToFitWidth = true;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let checkMarkImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "circledCheckMark").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = .white;
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let rideTitleBackgroundView: UIView = {
        let view = UIView();
        view.backgroundColor = Style.grayColor.withAlphaComponent(0.5);
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let rideImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "car").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let rideTitleLabel: UILabel = {
        let label = UILabel();
        label.text = "Ride".localized.uppercased();
        label.font = Style.biggerFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var rideSwich: UISwitch = {
        let swich = UISwitch();
        swich.addTarget(self, action: #selector(hidePrice), for: .valueChanged);
        swich.isOn = true;
        swich.translatesAutoresizingMaskIntoConstraints = false;
        return swich;
    }();
    
    let trackTitleBackgroundView: UIView = {
        let view = UIView();
        view.backgroundColor = Style.grayColor.withAlphaComponent(0.5);
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let trackImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "tracking").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let trackTitleLabel: UILabel = {
        let label = UILabel();
        label.text = "Share Live Location".localized.uppercased();
        label.font = Style.biggerFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var trackSwich: UISwitch = {
        let swich = UISwitch();
        swich.addTarget(self, action: #selector(handleTrackingView), for: .touchUpInside);
        swich.isOn = false;
        swich.translatesAutoresizingMaskIntoConstraints = false;
        return swich;
    }();
    
    let addressLabel: UILabel = {
        let label = UILabel();
        label.numberOfLines = 0;
        label.font = Style.normalFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let priceInUsd: UILabel = {
        let label = UILabel();
//        label.text = "($4.5)"
        label.textColor = Style.blueColor;
        label.font = Style.normalFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let minutesLabel: UILabel = {
        let label = UILabel();
        label.textColor = Style.blueColor;
        label.font = Style.smallFont;
        label.adjustsFontSizeToFitWidth = true;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceLabel: UILabel = {
        let label = UILabel();
        label.textColor = Style.blueColor;
        label.font = Style.smallFont;
        label.adjustsFontSizeToFitWidth = true;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let geoImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "geo");
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let carImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "car");
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let distanceImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "distance");
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    lazy var trackingCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout();
        layout.scrollDirection = .horizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout);
        collection.delegate = self;
        collection.dataSource = self;
        collection.backgroundColor = .white;
        collection.showsHorizontalScrollIndicator = false;
        collection.alwaysBounceHorizontal = true;
        collection.register(TrackerCell.self, forCellWithReuseIdentifier: cellIdetifier);
        collection.translatesAutoresizingMaskIntoConstraints = false;
        return collection;
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.2);
        
        view.addSubview(popupView);
        popupView.addSubview(rideTitleBackgroundView);
        rideTitleBackgroundView.addSubview(rideImageView);
        rideTitleBackgroundView.addSubview(rideTitleLabel);
        rideTitleBackgroundView.addSubview(rideSwich);
        requestRideView.addSubview(checkMarkImageView);
        requestRideView.addSubview(requestRideLabel);
        popupView.addSubview(requestRideView);
        popupView.addSubview(addressLabel);
        popupView.addSubview(geoImageView);
        popupView.addSubview(carImageView);
        popupView.addSubview(distanceLabel);
        popupView.addSubview(minutesLabel);
        popupView.addSubview(distanceImageView);
        popupView.addSubview(priceInUsd);
        popupView.addSubview(trackTitleBackgroundView);
        trackTitleBackgroundView.addSubview(trackImageView);
        trackTitleBackgroundView.addSubview(trackTitleLabel);
        trackTitleBackgroundView.addSubview(trackSwich);
        popupView.addSubview(trackingCollectionView);
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissPopup));
        view.addGestureRecognizer(tap);
        
        // this is only for rejecting tap on main view
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(popupTapped));
        tap1.cancelsTouchesInView = true;
        popupView.addGestureRecognizer(tap1);
        
        //        listenToRequestRide()
        setupConstraints();
        loadFriends();
        self.trackingCollectionViewHeightAnchor?.constant = 0;
    }
    
    /// This function is no need to do anything, it only reject the behaviour of background view.
    @objc private func popupTapped() {
        // leave it as it is. it controlls tap and dismiss behaviour
    }
    
    @objc private func hidePrice() {
        self.priceInUsd.isHidden = !rideSwich.isOn;
    }
    
    /// Hide or Show frinds list for tracking.
    @objc private func handleTrackingView() {
        
        if(trackSwich.isOn){
            self.trackingCollectionViewHeightAnchor?.constant = 80;
            self.popupViewHeightAnchor?.constant = 330;
        } else {
            self.trackingCollectionViewHeightAnchor?.constant = 0;
            self.popupViewHeightAnchor?.constant = 250;
        }
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded();
        }
    }
    
    /// Loads friends on ViewDidload and assing them to tracking frinds list which enables user to select any friends for this trip.
    private func loadFriends(){
        guard let userId = Values.currentUser?.id else { return }
        
        Helpers.request(api: API.getAllFriends(userId: userId), method: .get) { (result) in
            guard let result = result else { return }
            
            if let data = result["data"] as? [String: Any], let friends = data["friends"] as? Array<[String: Any]> {
                for friendDic in friends {
                    self.trackers.append(Tracker(dic: friendDic));
                }
                
                self.trackingCollectionView.reloadData();
                
                if (self.trackers.count == 0) {
                    self.trackingCollectionView.backgroundView = EmptyMessageView(message: "NoFriendInYourList".localized);
                } else {
                    self.trackingCollectionView.backgroundView = nil;
                }
            }
        }
    }
    
    /// When user clicks outside the popup, it will close it.
    @objc private func dismissPopup() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "clearRoutes"), object: nil);
        self.dismiss(animated: true, completion: nil);
    }
    
    var popupViewHeightAnchor: NSLayoutConstraint?
    var trackingCollectionViewHeightAnchor: NSLayoutConstraint?
    
    
    /// Setting constraints for views.
    private func setupConstraints(){
        //        popupView
        popupView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true;
        popupView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true;
        popupView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        popupViewHeightAnchor = popupView.heightAnchor.constraint(equalToConstant: 240);
        popupViewHeightAnchor?.isActive = true;
        
        //        requestRideView
        requestRideView.leftAnchor.constraint(equalTo: popupView.leftAnchor, constant: 16).isActive = true;
        requestRideView.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 12).isActive = true;
        requestRideView.heightAnchor.constraint(equalToConstant: 30).isActive = true;
        requestRideView.widthAnchor.constraint(equalToConstant: 110).isActive = true;
        
        //        checkMarkImageView
        checkMarkImageView.rightAnchor.constraint(equalTo: requestRideView.rightAnchor, constant: -6).isActive = true;
        checkMarkImageView.centerYAnchor.constraint(equalTo: requestRideView.centerYAnchor).isActive = true;
        checkMarkImageView.heightAnchor.constraint(equalToConstant: 22).isActive = true;
        checkMarkImageView.widthAnchor.constraint(equalToConstant: 22).isActive = true;
        
        //        requestRideLabel
        requestRideLabel.leftAnchor.constraint(equalTo: requestRideView.leftAnchor, constant: 8).isActive = true;
        requestRideLabel.rightAnchor.constraint(equalTo: checkMarkImageView.leftAnchor, constant: -4).isActive = true;
        requestRideLabel.centerYAnchor.constraint(equalTo: requestRideView.centerYAnchor).isActive = true;
        
        //        priceInZM
        priceInUsd.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -24).isActive = true;
        priceInUsd.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 16).isActive = true;
        
        //        rideTitleBackgroundView
        rideTitleBackgroundView.topAnchor.constraint(equalTo: requestRideView.bottomAnchor, constant: 20).isActive = true;
        rideTitleBackgroundView.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        rideTitleBackgroundView.heightAnchor.constraint(equalToConstant: 40).isActive = true;
        rideTitleBackgroundView.widthAnchor.constraint(equalTo: popupView.widthAnchor).isActive = true;
        
        //        rideImageView
        rideImageView.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 20).isActive = true;
        rideImageView.centerYAnchor.constraint(equalTo: rideTitleBackgroundView.centerYAnchor).isActive = true;
        rideImageView.heightAnchor.constraint(equalToConstant: 24).isActive = true;
        rideImageView.widthAnchor.constraint(equalToConstant: 24).isActive = true;
        
        //        rideTitleLabel
        rideTitleLabel.leadingAnchor.constraint(equalTo: rideImageView.trailingAnchor, constant: 16).isActive = true;
        rideTitleLabel.centerYAnchor.constraint(equalTo: rideImageView.centerYAnchor).isActive = true;
        
        //        rideSwich
        rideSwich.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -30).isActive = true;
        rideSwich.centerYAnchor.constraint(equalTo: rideImageView.centerYAnchor).isActive = true;
        
        //        addressLabel
        addressLabel.topAnchor.constraint(equalTo: rideTitleBackgroundView.bottomAnchor, constant: 12).isActive = true;
        addressLabel.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -16).isActive = true;
        addressLabel.leadingAnchor.constraint(equalTo: geoImageView.trailingAnchor, constant: 18).isActive = true;
        
        //        geoImageView
        geoImageView.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 16).isActive = true;
        geoImageView.centerYAnchor.constraint(equalTo: addressLabel.bottomAnchor).isActive = true;
        geoImageView.widthAnchor.constraint(equalToConstant: 26).isActive = true;
        geoImageView.heightAnchor.constraint(equalToConstant: 26).isActive = true;
        
        //        carImageView
        carImageView.topAnchor.constraint(equalTo: geoImageView.centerYAnchor, constant: 6).isActive = true;
        carImageView.leadingAnchor.constraint(equalTo: addressLabel.leadingAnchor).isActive = true;
        carImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true;
        carImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true;
        
        //        minutesLabel
        minutesLabel.leadingAnchor.constraint(equalTo: carImageView.trailingAnchor, constant: 10).isActive = true;
        minutesLabel.centerYAnchor.constraint(equalTo: carImageView.centerYAnchor).isActive = true;
        minutesLabel.widthAnchor.constraint(equalToConstant: 66).isActive = true;
        
        //        distanceImageView
        distanceImageView.leadingAnchor.constraint(equalTo: minutesLabel.trailingAnchor, constant: 8).isActive = true;
        distanceImageView.centerYAnchor.constraint(equalTo: carImageView.centerYAnchor).isActive = true;
        distanceImageView.widthAnchor.constraint(equalToConstant: 22).isActive = true;
        distanceImageView.heightAnchor.constraint(equalToConstant: 22).isActive = true;
        
        //        distanceLabel
        distanceLabel.leadingAnchor.constraint(equalTo: distanceImageView.trailingAnchor, constant: 10).isActive = true;
        distanceLabel.centerYAnchor.constraint(equalTo: minutesLabel.centerYAnchor).isActive = true;
        distanceLabel.widthAnchor.constraint(equalToConstant: 66).isActive = true;
        
        //        trackTitleBackgroundView
        trackTitleBackgroundView.topAnchor.constraint(equalTo: carImageView.bottomAnchor, constant: 22).isActive = true;
        trackTitleBackgroundView.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        trackTitleBackgroundView.heightAnchor.constraint(equalToConstant: 40).isActive = true;
        trackTitleBackgroundView.widthAnchor.constraint(equalTo: popupView.widthAnchor).isActive = true;
        
        //        trackImageView
        trackImageView.centerXAnchor.constraint(equalTo: rideImageView.centerXAnchor).isActive = true;
        trackImageView.centerYAnchor.constraint(equalTo: trackTitleBackgroundView.centerYAnchor).isActive = true;
        trackImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true;
        trackImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true;
        
        //        trackTitleLabel
        trackTitleLabel.leadingAnchor.constraint(equalTo: rideTitleLabel.leadingAnchor).isActive = true;
        trackTitleLabel.centerYAnchor.constraint(equalTo: trackImageView.centerYAnchor).isActive = true;
        
        //        trackSwich
        trackSwich.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -30).isActive = true;
        trackSwich.centerYAnchor.constraint(equalTo: trackImageView.centerYAnchor).isActive = true;
        
        //        trackingCollectionView
        trackingCollectionView.topAnchor.constraint(equalTo: trackTitleBackgroundView.bottomAnchor, constant: 12).isActive = true;
        trackingCollectionView.widthAnchor.constraint(equalTo: popupView.widthAnchor, multiplier: 0.9).isActive = true;
        trackingCollectionViewHeightAnchor = trackingCollectionView.heightAnchor.constraint(equalToConstant: 80);
        trackingCollectionViewHeightAnchor?.isActive = true;
        trackingCollectionView.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
    }
}

private let cellIdetifier = "AvatarCell";
extension RequestTripViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, TrackingDelegate {
    
    // MARK: - CollectionView Delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trackers.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdetifier, for: indexPath) as! TrackerCell
        cell.tracker = trackers[indexPath.row];
        cell.trackingDelegate = self;
        return cell;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 70);
    }
    
    /// Submitting ride request to the server.
    @objc fileprivate func submiteRideRequst() {
        guard let userId = Values.currentUser?.id else { return }
        guard let toAddress = self.addressLabel.text else { return }
        guard let toLocation = self.destinationLocation else { return }
        guard let sourceLocation = self.sourceLocation else { return }
        
        var trackers = [[String: Any]]();
        for tracker in self.trackers {
            if let id = tracker.id, tracker.isTracking {
                trackers.append(["user_id": id]);
            }
        }
        var tripType = TripType.ride;
        if trackSwich.isOn && !rideSwich.isOn {
            tripType = .liveTracking;
        } else if !trackSwich.isOn && !rideSwich.isOn {
            PopupMessages.showInfoMessage(title: "", message: "EitherTakeRideOrTrack".localized);
            return;
        }
        guard let distance = self.distanceInMeters, let duration = self.durationInMinutes else { return }
        
        let rideData = ["user_id":  userId, "trackers": trackers, "distance": distance, "duration": duration, "from_place_id": self.fromLocationId, "from_addr": self.sourceAddress, "from_lat": sourceLocation.latitude, "from_lng": sourceLocation.longitude, "to_addr": toAddress, "to_lat": toLocation.latitude, "to_lng": toLocation.longitude, "ride_request": 1, "type": TripType.ride.rawValue] as [String : Any];
        self.requestRideView.isUserInteractionEnabled = false;
        self.view.startActivityIndicator();
        Helpers.request(api: API.requestRide, method: .post, parameters: rideData) { (result) in
            self.view.stopActivityIndicator();
            guard let result = result else { return }
            if let success = result["success"] as? Bool, success {
                guard let data = result["data"] as? [String: Any] else { return }
                let trip = Trip(dic: data);
                ActiveTripViewController.activeTrip = trip;
                ActiveTripViewController.activeTrip?.ride?.paymentDetails = Helpers.parsePaymentDetails(trip: trip);
                ActiveTripViewController.stateLabel.text = result["message"] as? String;
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "animateRide"), object: nil);
                
                self.dismiss(animated: true, completion: nil);
            }
        }
    }
    
    private func updateRidePrice(distance: Double) {
        view.startActivityIndicator();
        let parameters = ["distance": distance];
        Helpers.request(api: API.fareCalculation, method: .post, parameters: parameters) { (result) in
            self.view.stopActivityIndicator();
            guard let result = result else { return }
            
            if let data = result["data"] as? [String: Any] {
                if let cost = data["cost"] as? Double {
                    self.priceInUsd.text = "\(Int(cost)) " + "AFN".localized;
                }
            }
        }
    }
    
    /// Updates tracking list. Adds or remove a friend to/from trackers list.
    func updateTracking(id: String, tracking: Bool) {
        if (tracking){
            trackers.first(where: {$0.id! == id})?.isTracking = true;
        } else {
            trackers.first(where: {$0.id! == id})?.isTracking = false;
        }
    }
}

/// This enumuration type indicates trip type and shows if this trip type is a trip or has ride or is a live tracking trip.
enum TripType: Int {
    case trip = 1, ride = 2, liveTracking = 3;
}
