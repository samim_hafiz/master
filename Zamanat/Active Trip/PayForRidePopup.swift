//
//  PayForRidePopup.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 6/6/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class PayForRidePopup: UIViewController {
    
    var ridePaymentDetails: RideDetailsPayment? {
        didSet {
            guard let ridePayDetails = ridePaymentDetails else { return }
            
            if let thumbUrl = ridePayDetails.driver?.thumbUrl {
                self.driverImageView.setImageFromUrl(urlStr: thumbUrl);
            }
            self.driverNameLabel.text = ridePayDetails.driver?.displayName;
            self.driverPhoneNumberLabel.text = ridePayDetails.driver?.phone;
            self.distanceLabel.text = ridePayDetails.distanceStr;
            self.timeLabel.text = ridePayDetails.durationStr;
            self.addressLabel.text = ridePayDetails.toAddress;
            if let amount = ridePayDetails.amount {
                self.priceInUsd.text = "(\(amount) \(ridePayDetails.currency.rawValue)";
            }
        }
    }
    
    let popupView: UIView = {
        let view = UIView();
        view.layer.cornerRadius = 3;
        view.layer.masksToBounds = true;
        view.backgroundColor = .white;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let headerView: UIView = {
        let view = UIView();
        view.layer.cornerRadius = 3;
        view.layer.masksToBounds = true;
        view.backgroundColor = Style.grayColor.withAlphaComponent(0.2)
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let addressLabel: UILabel = {
        let label = UILabel();
        label.numberOfLines = 0;
        label.font = Style.normalFont;
        label.adjustsFontSizeToFitWidth = true;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let titleLabel: UILabel = {
        let label = UILabel();
        label.text = "payForRide".localized;
        label.textColor = Style.mainColor;
        label.font = Style.normalFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let priceInUsd: UILabel = {
        let label = UILabel();
        label.textColor = Style.blueColor;
        label.font = Style.smallBoldFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "default_profile").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.backgroundColor = Style.differColor;
        imageView.layer.cornerRadius = 25;
        imageView.layer.borderWidth = 2;
        imageView.layer.borderColor = Style.mainColor.cgColor;
        imageView.layer.masksToBounds = true;
        imageView.contentMode = .scaleAspectFill;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let driverNameLabel: UILabel = {
        let label = UILabel();
        label.textAlignment = .center;
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverPhoneNumberLabel: UILabel = {
        let label = UILabel();
        label.textAlignment = .center;
        label.font = Style.normalFont;
        label.textColor = Style.mainColor;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "distance");
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let carImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "car");
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let timeLabel: UILabel = {
        let label = UILabel();
        label.textColor = Style.blueColor;
        label.font = Style.smallFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceLabel: UILabel = {
        let label = UILabel();
        label.textAlignment = .center;
        label.textColor = Style.blueColor;
        label.font = Style.smallFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var payButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(proceedPayment), for: .touchUpInside);
        button.setTitle("Pay".localized.uppercased(), for: .normal);
        button.backgroundColor = Style.mainColor;
        button.setTitleColor(.white, for: .normal);
        button.translatesAutoresizingMaskIntoConstraints = false;
        return  button;
    }();
    
    lazy var cancelButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(closePopup), for: .touchUpInside);
        button.setTitle("Cancel".localized.uppercased(), for: .normal);
        button.backgroundColor = Style.grayColor.withAlphaComponent(0.2);
        button.setTitleColor(UIColor.black.withAlphaComponent(0.8), for: .normal);
        button.translatesAutoresizingMaskIntoConstraints = false;
        return  button;
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.view.backgroundColor = UIColor.darkGray.withAlphaComponent(0.4);
        
        view.addSubview(popupView);
        headerView.addSubview(titleLabel);
        headerView.addSubview(priceInUsd);
        popupView.addSubview(headerView);
        popupView.addSubview(addressLabel);
        popupView.addSubview(carImageView);
        popupView.addSubview(distanceImageView);
        popupView.addSubview(distanceLabel);
        popupView.addSubview(timeLabel);
        popupView.addSubview(headerView);
        popupView.addSubview(driverImageView);
        popupView.addSubview(driverNameLabel);
        popupView.addSubview(driverPhoneNumberLabel);
        popupView.addSubview(payButton);
        popupView.addSubview(cancelButton);
        
        setupConstraints();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        if self.ridePaymentDetails?.driver?.displayName != nil {
            return;
        }
        
        guard let ride = ActiveTripViewController.activeTrip?.ride else { return }
        
        if let thumbUrl = ride.driver?.thumbUrl {
            self.driverImageView.setImageFromUrl(urlStr: thumbUrl);
        }
        self.driverNameLabel.text = ride.driver?.displayName;
        self.driverPhoneNumberLabel.text = ride.driver?.phone;
        self.distanceLabel.text = ride.distanceToUser;
        self.timeLabel.text = ride.durationToUser;
        self.addressLabel.text = ActiveTripViewController.activeTrip?.toAddress;
        if let amount = ride.price {
            self.priceInUsd.text = "(\(amount) AFN)";
        }
    }
    
    @objc private func proceedPayment() {
        self.dismiss(animated: true, completion: nil);
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "selectPaymentMethod"), object: nil);
    }
    
    @objc private func closePopup() {
        let alert = UIAlertController(title: "Cancel Payment".localized, message: "When you cancel your ride at this time, your will be charged 25% of the total ride fare.\nAre you sure for cancelling the ride?".localized, preferredStyle: .alert);
        let yesAction = UIAlertAction(title: "Yes".localized, style: .default, handler: { (_) in
            
            let trip = ActiveTripViewController.activeTrip;
            guard let userId = Values.currentUser?.id  else { return }
            guard let tripId = trip?.id else { return }
            guard let rideId = trip?.ride?.id  else { return }
            guard let toUserId = trip?.ride?.driver?.id else { return }
            guard let amount = trip?.ride?.price else { return }
            
            let data = ["ride_id": rideId, "trip_id": tripId, "user_id": userId, "to_user_id": toUserId, "amount": amount] as [String : Any];
            SocketIOManager.sharedInstance.emit("cancel-pay-from-wallet", data);
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cancelTrip"), object: nil);
            PopupMessages.showInfoMessage(message: "RideCanceled".localized);
            
            self.dismiss(animated: false, completion: nil);
        });
        
        let cancelAction = UIAlertAction(title: "No".localized, style: .cancel, handler: nil);
        alert.addAction(yesAction);
        alert.addAction(cancelAction);
        self.present(alert, animated: true, completion: nil);
    }
    
    private func setupConstraints() {
        popupView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7).isActive = true;
        popupView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.55).isActive = true;
        popupView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true;
        popupView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        headerView.topAnchor.constraint(equalTo: popupView.topAnchor).isActive = true;
        headerView.heightAnchor.constraint(equalToConstant: 42).isActive = true;
        headerView.widthAnchor.constraint(equalTo: popupView.widthAnchor).isActive = true;
        headerView.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        
        titleLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 12).isActive = true;
        titleLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true;
        
        priceInUsd.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -12).isActive = true;
        priceInUsd.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true;
        
        addressLabel.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        addressLabel.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 16).isActive = true;
        addressLabel.widthAnchor.constraint(equalTo: popupView.widthAnchor, multiplier: 0.8).isActive = true;
        
        carImageView.leadingAnchor.constraint(equalTo: addressLabel.leadingAnchor).isActive = true;
        carImageView.topAnchor.constraint(equalTo: addressLabel.bottomAnchor, constant: 12).isActive = true;
        carImageView.heightAnchor.constraint(equalToConstant: 22).isActive = true;
        carImageView.widthAnchor.constraint(equalToConstant: 22).isActive = true;
        
        timeLabel.leadingAnchor.constraint(equalTo: carImageView.trailingAnchor, constant: 12).isActive = true;
        timeLabel.centerYAnchor.constraint(equalTo: carImageView.centerYAnchor).isActive = true;
        
        distanceImageView.leadingAnchor.constraint(equalTo: timeLabel.trailingAnchor, constant: 20).isActive = true;
        distanceImageView.heightAnchor.constraint(equalToConstant: 22).isActive = true;
        distanceImageView.widthAnchor.constraint(equalToConstant: 22).isActive = true;
        distanceImageView.centerYAnchor.constraint(equalTo: carImageView.centerYAnchor).isActive = true;
        
        distanceLabel.leadingAnchor.constraint(equalTo: distanceImageView.trailingAnchor, constant: 8).isActive = true;
        distanceLabel.centerYAnchor.constraint(equalTo: timeLabel.centerYAnchor, constant: 2).isActive = true;
        
        driverImageView.topAnchor.constraint(equalTo: carImageView.bottomAnchor, constant: 30).isActive = true;
        driverImageView.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        driverImageView.heightAnchor.constraint(equalToConstant: 64).isActive = true;
        driverImageView.widthAnchor.constraint(equalToConstant: 64).isActive = true;
        
        driverNameLabel.topAnchor.constraint(equalTo: driverImageView.bottomAnchor, constant: 10).isActive = true;
        driverNameLabel.centerXAnchor.constraint(equalTo: driverImageView.centerXAnchor).isActive = true;
        driverNameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true;
        
        driverPhoneNumberLabel.topAnchor.constraint(equalTo: driverNameLabel.bottomAnchor).isActive = true;
        driverPhoneNumberLabel.centerXAnchor.constraint(equalTo: driverNameLabel.centerXAnchor).isActive = true;
        
        payButton.trailingAnchor.constraint(equalTo: popupView.trailingAnchor).isActive = true;
        payButton.bottomAnchor.constraint(equalTo: popupView.bottomAnchor).isActive = true;
        payButton.widthAnchor.constraint(equalTo: popupView.widthAnchor, multiplier: 0.5).isActive = true;
        payButton.heightAnchor.constraint(equalToConstant: 44).isActive = true;
        
        cancelButton.leadingAnchor.constraint(equalTo: popupView.leadingAnchor).isActive = true;
        cancelButton.bottomAnchor.constraint(equalTo: popupView.bottomAnchor).isActive = true;
        cancelButton.widthAnchor.constraint(equalTo: payButton.widthAnchor).isActive = true;
        cancelButton.heightAnchor.constraint(equalTo: payButton.heightAnchor).isActive = true;
        
    }
}
