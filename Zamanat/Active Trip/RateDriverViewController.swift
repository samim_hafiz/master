//
//  RateDriverViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 7/30/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class RateDriverViewController: UIViewController {
    var ride: Ride? {
        didSet {
            if let thumbUrl = ride?.driver?.thumbUrl {
                self.driverImageView.setImageFromUrl(urlStr: thumbUrl);
            }
            self.driverNameLabel.text = ride?.driver?.displayName;
            self.driverPhoneNumberLabel.text = ride?.driver?.phone;
        }
    }
    
    let popupView: UIView = {
        let view = UIView();
        view.layer.cornerRadius = 3;
        view.layer.masksToBounds = true;
        view.backgroundColor = .white;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let titleLabel: UILabel = {
        let label = UILabel();
        label.backgroundColor = Style.mainColor.withAlphaComponent(0.8);
        label.layer.cornerRadius = 2;
        label.layer.masksToBounds = true;
        label.text = "RateRide".localized.uppercased();
        label.textColor = .white;
        label.font = Style.normalFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "default_profile").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.mainColor;
        imageView.backgroundColor = Style.differColor;
        imageView.layer.cornerRadius = 25;
        imageView.layer.borderWidth = 2;
        imageView.layer.borderColor = Style.mainColor.cgColor;
        imageView.layer.masksToBounds = true;
        imageView.contentMode = .scaleAspectFill;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let driverNameLabel: UILabel = {
        let label = UILabel();
        label.textAlignment = .center;
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverPhoneNumberLabel: UILabel = {
        let label = UILabel();
        label.textAlignment = .center;
        label.font = Style.normalFont;
        label.textColor = Style.mainColor;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let starsView: RankingStars = {
        let starsView = RankingStars();
        starsView.translatesAutoresizingMaskIntoConstraints = false;
        return starsView;
    }();
    
    let ratingDescription: UILabel = {
        let label = UILabel();
        label.numberOfLines = 0;
        label.text = "RatingDescription".localized;
        label.textColor = UIColor.black.withAlphaComponent(0.6);
        label.font = Style.smallFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var closeButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(closePopup), for: .touchUpInside);
        button.setTitle("close".localized, for: .normal);
        button.titleLabel?.font = Style.smallFont;
        button.backgroundColor = .clear;
        button.setTitleColor(.white, for: .normal);
        button.layer.borderWidth = 1;
        button.layer.borderColor = Style.mainColor.cgColor;
        button.layer.cornerRadius = 5;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return  button;
    }();
    
    lazy var submitButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(submitRating), for: .touchUpInside);
        button.setTitle("Submit".localized, for: .normal);
        button.backgroundColor = Style.mainColor;
        button.setTitleColor(.white, for: .normal);
        button.layer.cornerRadius = 1;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return  button;
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.view.backgroundColor = UIColor.darkGray.withAlphaComponent(0.4);
        
        view.addSubview(popupView);
        
        popupView.addSubview(titleLabel);
        popupView.addSubview(driverImageView);
        popupView.addSubview(driverNameLabel);
        popupView.addSubview(driverPhoneNumberLabel);
        popupView.addSubview(starsView);
        popupView.addSubview(ratingDescription);
        popupView.addSubview(submitButton);
        popupView.addSubview(closeButton);
        
        setupConstraints();
    }
    
    @objc private func closePopup() {
        self.dismiss(animated: true, completion: nil);
    }
    
    @objc private func submitRating() {
        if starsView.rating == 0 {
            PopupMessages.showInfoMessage(message: "pleaseSelectRating".localized);
            return;
        }
        
        guard let rideId = self.ride?.id else {
            self.dismiss(animated: true, completion: nil)
            return }
        guard let driverId = self.ride?.driver?.id else { self.dismiss(animated: true, completion: nil); return }
        guard let userId = Values.currentUser?.id else {  self.dismiss(animated: true, completion: nil); return }
        
        let parameters = ["rate": starsView.rating, "driver_id": driverId, "ride_id": rideId, "user_id": userId] as [String : Any];
        
        view.startActivityIndicator();
        Helpers.request(api: API.rateDriver, method: .post, parameters: parameters) { (result) in
            self.view.stopActivityIndicator();
            guard let result = result else { self.dismiss(animated: true, completion: nil); return }
            if let success = result["success"] as? Bool, success {
                if let message = result["message"] as? String {
                    PopupMessages.showSuccessMessage(message: message);
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    private func setupConstraints() {
        popupView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75).isActive = true;
        popupView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.65).isActive = true;
        popupView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20).isActive = true;
        popupView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        titleLabel.topAnchor.constraint(equalTo: popupView.topAnchor).isActive = true;
        titleLabel.heightAnchor.constraint(equalToConstant: 42).isActive = true;
        titleLabel.widthAnchor.constraint(equalTo: popupView.widthAnchor).isActive = true;
        titleLabel.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        
        driverImageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30).isActive = true;
        driverImageView.centerXAnchor.constraint(equalTo: titleLabel.centerXAnchor).isActive = true;
        driverImageView.heightAnchor.constraint(equalToConstant: 64).isActive = true;
        driverImageView.widthAnchor.constraint(equalToConstant: 64).isActive = true;
        
        driverNameLabel.topAnchor.constraint(equalTo: driverImageView.bottomAnchor, constant: 10).isActive = true;
        driverNameLabel.centerXAnchor.constraint(equalTo: driverImageView.centerXAnchor).isActive = true;
        driverNameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true;
        
        driverPhoneNumberLabel.topAnchor.constraint(equalTo: driverNameLabel.bottomAnchor).isActive = true;
        driverPhoneNumberLabel.centerXAnchor.constraint(equalTo: driverNameLabel.centerXAnchor).isActive = true;
        
        starsView.topAnchor.constraint(equalTo: driverPhoneNumberLabel.bottomAnchor, constant: 30).isActive = true;
        starsView.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        starsView.heightAnchor.constraint(equalToConstant: 36).isActive = true;
        starsView.widthAnchor.constraint(equalTo: popupView.widthAnchor, multiplier: 0.6).isActive = true;
        
        ratingDescription.topAnchor.constraint(equalTo: starsView.bottomAnchor, constant: 20).isActive = true;
        ratingDescription.widthAnchor.constraint(equalTo: popupView.widthAnchor, multiplier: 0.6).isActive = true;
        ratingDescription.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;

        closeButton.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -10).isActive = true;
        closeButton.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor).isActive = true;
        closeButton.widthAnchor.constraint(equalToConstant: 48).isActive = true;
        closeButton.heightAnchor.constraint(equalToConstant: 24).isActive = true;
        
        submitButton.bottomAnchor.constraint(equalTo: popupView.bottomAnchor).isActive = true;
        submitButton.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        submitButton.widthAnchor.constraint(equalTo: popupView.widthAnchor).isActive = true;
        submitButton.heightAnchor.constraint(equalToConstant: 42).isActive = true;
    }
}
