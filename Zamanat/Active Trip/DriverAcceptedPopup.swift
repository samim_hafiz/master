//
//  DriverAcceptedPopup.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 6/4/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class DriverAcceptedPopup: UIViewController {
    
    var driver: Driver? {
        didSet {
            if let thumbUrl = driver?.thumbUrl {
                self.driverImageView.setImageFromUrl(urlStr: thumbUrl);
            }
            self.driverNameLabel.text = driver?.displayName;
            self.driverPhoneNumberLabel.text = driver?.phone;
            self.distanceLabel.text = driver?.distanceToUser;
            self.timeLabel.text = driver?.durationToUser;
        }
    }
    
    let popupView: UIView = {
        let view = UIView();
        view.layer.cornerRadius = 3;
        view.layer.masksToBounds = true;
        view.backgroundColor = .white;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let titleLabel: UILabel = {
        let label = UILabel();
        label.backgroundColor = Style.mainColor.withAlphaComponent(0.8);
        label.layer.cornerRadius = 2;
        label.layer.masksToBounds = true;
        label.text = "RideAccepted".localized;
        label.textColor = .white;
        label.font = Style.normalFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "default_profile").withRenderingMode(.alwaysTemplate);
        imageView.tintColor = Style.differColor;
        imageView.backgroundColor = Style.mainColor;
        imageView.layer.cornerRadius = 25;
        imageView.layer.borderWidth = 2;
        imageView.layer.borderColor = Style.mainColor.cgColor;
        imageView.layer.masksToBounds = true;
        imageView.contentMode = .scaleAspectFill;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let driverNameLabel: UILabel = {
        let label = UILabel();
        label.textAlignment = .center;
        label.font = Style.normalBoldFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let driverPhoneNumberLabel: UILabel = {
        let label = UILabel();
        label.textAlignment = .center;
        label.font = Style.normalFont;
        label.textColor = Style.mainColor;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "distance");
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let timeLabel: UILabel = {
        let label = UILabel();
        label.textColor = Style.blueColor;
        label.font = Style.smallFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceLabel: UILabel = {
        let label = UILabel();
        label.textAlignment = .center;
        label.textColor = Style.blueColor;
        label.font = Style.smallFont;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let distanceDetailsStackView: UIStackView = {
        let stack = UIStackView();
        stack.distribution = .fillEqually;
        stack.axis = .horizontal;
        stack.alignment = .center;
        stack.spacing = 8;
        stack.translatesAutoresizingMaskIntoConstraints = false;
        return stack;
    }();
    
    lazy var okButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(closePopup), for: .touchUpInside);
        button.setTitle("OK".localized, for: .normal);
        button.backgroundColor = Style.mainColor;
        button.setTitleColor(.white, for: .normal);
        button.layer.cornerRadius = 3;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return  button;
    }();

    override func viewDidLoad() {
        super.viewDidLoad();
        self.view.backgroundColor = UIColor.darkGray.withAlphaComponent(0.4);
        
        view.addSubview(popupView);
        
        popupView.addSubview(titleLabel);
        popupView.addSubview(driverImageView);
        popupView.addSubview(driverNameLabel);
        popupView.addSubview(driverPhoneNumberLabel);
        popupView.addSubview(distanceDetailsStackView);
        distanceDetailsStackView.addArrangedSubview(distanceImageView);
        distanceDetailsStackView.addArrangedSubview(distanceLabel);
        distanceDetailsStackView.addArrangedSubview(timeLabel);
        popupView.addSubview(okButton);
        
        setupConstraints();
    }
    
    @objc private func closePopup() {
        self.dismiss(animated: true, completion: nil);
    }
    
    private func setupConstraints() {
        popupView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7).isActive = true;
        popupView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.47).isActive = true;
        popupView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true;
        popupView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        titleLabel.topAnchor.constraint(equalTo: popupView.topAnchor).isActive = true;
        titleLabel.heightAnchor.constraint(equalToConstant: 42).isActive = true;
        titleLabel.widthAnchor.constraint(equalTo: popupView.widthAnchor).isActive = true;
        titleLabel.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        
        driverImageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true;
        driverImageView.centerXAnchor.constraint(equalTo: titleLabel.centerXAnchor).isActive = true;
        driverImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true;
        driverImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true;
        
        driverNameLabel.topAnchor.constraint(equalTo: driverImageView.bottomAnchor, constant: 10).isActive = true;
        driverNameLabel.centerXAnchor.constraint(equalTo: driverImageView.centerXAnchor).isActive = true;
        driverNameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true;
        
        driverPhoneNumberLabel.topAnchor.constraint(equalTo: driverNameLabel.bottomAnchor).isActive = true;
        driverPhoneNumberLabel.centerXAnchor.constraint(equalTo: driverNameLabel.centerXAnchor).isActive = true;
        
        distanceDetailsStackView.widthAnchor.constraint(equalToConstant: 200).isActive = true;
        distanceDetailsStackView.heightAnchor.constraint(equalToConstant: 24).isActive = true;
        distanceDetailsStackView.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        distanceDetailsStackView.topAnchor.constraint(equalTo: driverPhoneNumberLabel.bottomAnchor, constant: 30).isActive = true;
        
        okButton.bottomAnchor.constraint(equalTo: popupView.bottomAnchor).isActive = true;
        okButton.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true;
        okButton.widthAnchor.constraint(equalTo: popupView.widthAnchor).isActive = true;
        okButton.heightAnchor.constraint(equalToConstant: 42).isActive = true;
    }
}
