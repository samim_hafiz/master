//
//  ActiveTripViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 2/4/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import Floaty
import GoogleMaps
import GooglePlaces
import MapKit
import CoreLocation

class ActiveTripViewController: UIViewController, FloatyDelegate {
    
    var routeLine: GMSPolyline?
    var marker: GMSMarker?;
    var taxiMarker: GMSMarker?
    static var activeTrip: Trip?;
    var assignedDriver: Driver?;
    var distanceInMeters: Double = 0.0;
    var distanceText: String = "";
    var durationInMinutes: Int = 0;
    var durationText = "";
    
    var driverPath = GMSMutablePath();
    
    lazy var floatyButton: Floaty = {
        let floaty = Floaty();
        floaty.fabDelegate = self;
        floaty.buttonColor = Style.differColor;
        floaty.buttonImage = #imageLiteral(resourceName: "buber_icon_blue");
        floaty.translatesAutoresizingMaskIntoConstraints = false;
        return floaty;
    }();
    
    let stateView: UIView = {
        let view = UIView();
        view.backgroundColor = Style.mainColor.withAlphaComponent(0.7);
        view.clipsToBounds = true;
        view.layer.cornerRadius = 2;
        view.layer.masksToBounds = true;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    static var stateLabel: UILabel = {
        let label = UILabel();
        label.numberOfLines = 0;
        label.text = "NoRide".localized;
        label.font = Style.smallBoldFont;
        label.textAlignment = .center;
        label.textColor = .white;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let availableDriversLabel: UILabel = {
        let label = UILabel();
        label.backgroundColor = Style.mainColor.withAlphaComponent(0.7);
        label.clipsToBounds = true;
        label.adjustsFontSizeToFitWidth = true;
        label.layer.cornerRadius = 1;
        label.layer.masksToBounds = true;
        label.font = Style.smallFont;
        label.textAlignment = .center;
        label.textColor = .white;
        label.isHidden = true;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let availableDriversCountLabel: UILabel = {
        let label = UILabel();
        label.backgroundColor = Style.mainColor.withAlphaComponent(0.7);
        label.clipsToBounds = true;
        label.adjustsFontSizeToFitWidth = true;
        label.layer.cornerRadius = 1;
        label.layer.masksToBounds = true;
        label.font = Style.smallFont;
        label.textAlignment = .center;
        label.textColor = .white;
        label.isHidden = true;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    var myMapView: GMSMapView!
    let locationManager = CLLocationManager()
    var fromLocation: CLLocationCoordinate2D?
    
    var myLocation: CLLocationCoordinate2D? {
        didSet {
            if(availableDriversLabel.isHidden && !driverChecked) {
                checkAvailableDrivers();
                driverChecked = true;
            }
        }
    }
    
    var toPlaceId = "";
    var driverChecked = false;
    static var statusText: String? {
        didSet {
            ActiveTripViewController.stateLabel.text = ActiveTripViewController.statusText;
            UserDefaults.standard.set(ActiveTripViewController.statusText, forKey: KeyStrings.statusText);
        }
    }
    
    var searchBarButtonItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        searchBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showSearchController));
        self.navigationItem.leftBarButtonItem = searchBarButtonItem;
        
        let camera = GMSCameraPosition.camera(withLatitude: 34.479295, longitude: 69.11341099999, zoom: 15);
        myMapView = GMSMapView.map(withFrame: view.bounds, camera: camera);
        myMapView.addSubview(floatyButton);
        myMapView.settings.myLocationButton = false;
        myMapView.delegate = self;
        view = myMapView;
        
        locationManager.delegate = self;
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            myMapView.isMyLocationEnabled = true
            myMapView.settings.myLocationButton = true
            
        } else {
            checkLocationAuthorizationStatus();
        }
        
        if Values.currentUser == nil {
            Helpers.logout();
            return;
        }
        
        loadActiveTrip();
        updateDriverPath();
        listenToAvailableDrivers();
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadActiveTrip), name: NSNotification.Name(rawValue: "loadActiveTrip"), object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(clearRoutes), name: NSNotification.Name(rawValue: "clearRoutes"), object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(cancelTrip), name: NSNotification.Name(rawValue: "cancelTrip"), object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(selectPaymentMethod), name: NSNotification.Name(rawValue: "selectPaymentMethod"), object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(completeTrip), name: NSNotification.Name(rawValue: "completeTrip"), object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(animateRide), name: Notification.Name(rawValue: "animateRide"), object: nil);
        
        view.addSubview(availableDriversLabel);
        view.addSubview(availableDriversCountLabel);
        view.addSubview(stateView);
        stateView.addSubview(ActiveTripViewController.stateLabel);
        
        if let statusText = UserDefaults.standard.value(forKey: KeyStrings.statusText) as? String {
            ActiveTripViewController.statusText = statusText;
        }
        
        setupConstrainst();
    }
    
    private func setupConstrainst() {
        stateView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -74).isActive = true;
        stateView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true;
        stateView.heightAnchor.constraint(equalToConstant: 42).isActive = true;
        stateView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true;
        
        ActiveTripViewController.stateLabel.leftAnchor.constraint(equalTo: self.stateView.leftAnchor, constant: 8).isActive = true;
        ActiveTripViewController.stateLabel.centerYAnchor.constraint(equalTo: self.stateView.centerYAnchor).isActive = true;
        ActiveTripViewController.stateLabel.rightAnchor.constraint(equalTo: self.stateView.rightAnchor, constant: -8).isActive = true;
        ActiveTripViewController.stateLabel.heightAnchor.constraint(equalTo: self.stateView.heightAnchor).isActive = true;
        
        availableDriversLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 4).isActive = true;
        availableDriversLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true;
        availableDriversLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 4).isActive = true;
        availableDriversLabel.widthAnchor.constraint(equalToConstant: 108).isActive = true;
        
        availableDriversCountLabel.centerYAnchor.constraint(equalTo: availableDriversLabel.centerYAnchor).isActive = true;
        availableDriversCountLabel.heightAnchor.constraint(equalTo: availableDriversLabel.heightAnchor).isActive = true;
        availableDriversCountLabel.leadingAnchor.constraint(equalTo: availableDriversLabel.trailingAnchor, constant: 1).isActive = true;
        availableDriversCountLabel.widthAnchor.constraint(equalToConstant: 26).isActive = true;
    }
    
    @objc private func showSearchController() {
        let autocompleteController = GMSAutocompleteViewController();
        let filter = GMSAutocompleteFilter();
        filter.country = "AF";
        autocompleteController.autocompleteFilter = filter;
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @objc private func animateRide() {
        self.myMapView.animate(toLocation: myLocation!);
    }
    
    private func checkAvailableDrivers() {
        guard let fromLat = self.myLocation?.latitude else { return }
        guard let fromLang = self.myLocation?.longitude else { return }
        let data = ["country":"Afghanistan", "province":"Kabul", "state": "Kabul", "from_lat": fromLat, "from_lng": fromLang] as [String : Any];
        
        SocketIOManager.sharedInstance.socket.emit("get-active-drivers", data);
        Timer.scheduledTimer(withTimeInterval: Values.checkAvailableDriversTimeInterval, repeats: true) { (_) in
            SocketIOManager.sharedInstance.socket.emit("get-active-drivers", data);
        }
    }
    
    fileprivate func listenToAvailableDrivers() {
        SocketIOManager.sharedInstance.socket.on("get-active-drivers") { (result, _) in
            if let socketArray = result as? Array<[String: Any]>, let total = socketArray[0]["totalDrivers"] as? Int {
                self.availableDriversLabel.isHidden = false;
                if(total <= 0){
                    self.availableDriversLabel.text = "noDriverAvailable".localized;
                } else {
                    self.availableDriversLabel.text = "availableDrivers".localized;
                    self.availableDriversCountLabel.text = "\(total)";
                    Values.numberOfDriversAvailable = total;
                    self.availableDriversCountLabel.isHidden = false;
                }
            }
        }
    }
    
    @objc private func cancelTrip() {
        guard let tripId = ActiveTripViewController.activeTrip?.id else { return }
        guard let userId = Values.currentUser?.id else { return }
        let rideStatus = ActiveTripViewController.activeTrip?.ride?.driver == nil ? -1 : 0;
        
//        if (ActiveTripViewController.activeTrip?.ride?.driver == nil) {
//            PopupMessages.showWarningMessage(message: "cantCancelActiveRide".localized);
//            return;
//        }
        // status 1 = complete, 2 = cancel
        
        self.view.startActivityIndicator();
        let parameters = ["trip_id": tripId, "user_id": userId, "status": 2, "ride_status": rideStatus] as [String : Any];
        Helpers.request(api: API.changeTripStatus, method: .post, parameters: parameters, completion: { (result) in
            self.view.stopActivityIndicator();
            if let tripId = ActiveTripViewController.activeTrip?.id, let userId = Values.currentUser?.id, let fullName = Values.currentUser?.displayName, let fromAddress = ActiveTripViewController.activeTrip?.fromAddress, let fromLat = ActiveTripViewController.activeTrip?.fromLatitude, let fromLang = ActiveTripViewController.activeTrip?.fromLongtitude, let updated = ActiveTripViewController.activeTrip?.updated, let toLat = ActiveTripViewController.activeTrip?.toLatitude, let toLang = ActiveTripViewController.activeTrip?.toLongtitude, let toAddress = ActiveTripViewController.activeTrip?.toAddress, let distance = ActiveTripViewController.activeTrip?.distance, let duration = ActiveTripViewController.activeTrip?.duration, let trackId = ActiveTripViewController.activeTrip?.track?.id {
                
                let data = ["status": 2, "_id": tripId, "user_id": ["user_id": userId, "display_name": fullName], "from_addr": fromAddress, "from_lat": fromLat, "from_lng": fromLang, "updated": updated, "created": updated, "to_lat": toLat, "to_lng": toLang, "to_addr": toAddress, "distance": distance, "duration": duration, "track_id": trackId, "type": TripType.ride.rawValue] as [String : Any]
                
                SocketIOManager.sharedInstance.socket.emit("trip-update-status", data);
            }
            
            self.clearRoutes();
            PopupMessages.showInfoMessage(title: "", message: "RideCanceled".localized);
            ActiveTripViewController.activeTrip = nil;
            self.searchBarButtonItem.isEnabled = true;
            ActiveTripViewController.statusText = "NoRide".localized;
            
            self.myMapView.animate(toLocation: self.myLocation!);
        });
    }
    
    @objc private func completeTrip() {
        self.clearRoutes();
        if myLocation != nil {
            myMapView.animate(toLocation: myLocation!);
        }
        guard let userId = Values.currentUser?.id else { return }
        guard let tripId = ActiveTripViewController.activeTrip?.id else {
            ActiveTripViewController.activeTrip = nil;
            self.clearRoutes();
            return;
        }
        
        let parameters = ["trip_id": tripId, "user_id": userId, "status": 1, "ride_status": RideStatus.completed.rawValue] as [String : Any];
        Helpers.request(api: API.changeTripStatus, method: .post, parameters: parameters, completion: { (result) in
            self.view.stopActivityIndicator();
            
            if let tripId = ActiveTripViewController.activeTrip?.id, let userId = Values.currentUser?.id, let displayName = Values.currentUser?.displayName, let phone = Values.currentUser?.phone, let fromAddress = ActiveTripViewController.activeTrip?.fromAddress, let fromLat = ActiveTripViewController.activeTrip?.fromLatitude, let fromLang = ActiveTripViewController.activeTrip?.fromLongtitude, let updated = ActiveTripViewController.activeTrip?.updated, let toLat = ActiveTripViewController.activeTrip?.toLatitude, let toLang = ActiveTripViewController.activeTrip?.toLongtitude, let toAddress = ActiveTripViewController.activeTrip?.toAddress, let distance = ActiveTripViewController.activeTrip?.distance, let duration = ActiveTripViewController.activeTrip?.duration, let trackId = ActiveTripViewController.activeTrip?.track?.id, let tolocationId = ActiveTripViewController.activeTrip?.toLocationId, let fromLoactionId = ActiveTripViewController.activeTrip?.fromLocationId {
                
                let thumbUrl = Values.currentUser?.thumbUrl == nil ? "" : Values.currentUser!.thumbUrl!;
                let data =  ["trip_id": tripId, "track_id": trackId, "user_id": userId, "user": ["display_name": displayName, "thumb_url": thumbUrl, "phone": phone], "from_addr": fromAddress, "from_lat": fromLat, "from_lng": fromLang, "from_place_id": fromLoactionId , "to_addr": toAddress, "to_lat": toLat, "to_lng": toLang, "to_place_id": self.toPlaceId == "" ? tolocationId : self.toPlaceId, "duration": duration, "distance": distance, "updated": updated] as [String : Any];
                
                SocketIOManager.sharedInstance.socket.emit("trip-update-status", data);
            }
            
            ActiveTripViewController.activeTrip = nil;
            self.clearRoutes();
            self.searchBarButtonItem.isEnabled = true;
            UserDefaults.standard.setValue(nil, forKey: "paymentDetails");
        })
    }
    
    @objc private func selectPaymentMethod() {
        let selectPaymentMethod = SelectPaymentMethod();
        
        if let paymentDetails = UserDefaults.standard.value(forKey: "paymentDetails") as? [String: Any] {
            selectPaymentMethod.paymentDetails = paymentDetails
        } else if let activeTrip = ActiveTripViewController.activeTrip {
            let paymentDetails = Helpers.parsePaymentDetails(trip: activeTrip);
            selectPaymentMethod.paymentDetails = paymentDetails;
        }
        
        selectPaymentMethod.modalTransitionStyle = .coverVertical;
        selectPaymentMethod.modalPresentationStyle = .overFullScreen;
        UIApplication.shared.topMostViewController()?.present(selectPaymentMethod, animated: true, completion: nil);
    }
    
    func emptyFloatySelected(_ floaty: Floaty) {
        if myLocation != nil {
            myMapView.animate(toLocation: myLocation!);
        }
        if let trip = ActiveTripViewController.activeTrip {
            let detailsPopupController = TripDetailsPopupViewController();
            if (trip.distance == nil || trip.distance!.isEmpty){
                trip.distance = "\(self.distanceInMeters/1000) km";
                trip.duration = "\(self.durationInMinutes) min";
            }
            detailsPopupController.trip = trip;
            detailsPopupController.modalTransitionStyle = .coverVertical;
            detailsPopupController.modalPresentationStyle = .overFullScreen;
            self.present(detailsPopupController, animated: true, completion: nil);
        } else {
            PopupMessages.showInfoMessage(message: "NoActiveRide".localized);
            self.clearRoutes();
        }
    }
    
    @objc private func loadActiveTrip() {
        
        guard let userId = Values.currentUser?.id else { return }
        
        if let tripId = UserDefaults.standard.value(forKey: "tripId") as? String, self.distanceInMeters != 0 {
            let userData = ["distance": self.distanceInMeters, "duration": self.durationInMinutes, "trip_id": tripId, "user_id": userId] as [String : Any];
            SocketIOManager.sharedInstance.socket.emit("add-trip-dist-dur", userData);
        }
        
        let parameters = ["user_id": userId];
        self.view.startActivityIndicator();
        Helpers.request(api: API.getCurrentActiveTrip, method: .post, parameters: parameters) { (result) in
            self.view.stopActivityIndicator();
            self.searchBarButtonItem.isEnabled = true;
            
            guard let result = result else { return }
            if let success = result["success"] as? Bool, success {
                self.searchBarButtonItem.isEnabled = false;
                guard let data = result["data"] as? [String: Any] else { return }
                let trip = Trip(dic: data);
                ActiveTripViewController.activeTrip = trip;
                ActiveTripViewController.activeTrip?.ride?.paymentDetails = Helpers.parsePaymentDetails(trip: trip);
                
                self.checkPaymentStatus();
                if let toLatitude = trip.toLatitude, let toLongtitude = trip.toLongtitude, let fromLatitude = trip.fromLatitude, let fromLontitude = trip.fromLongtitude {
                    self.floatyButton.layoutIfNeeded();
                    self.fromLocation = CLLocationCoordinate2D(latitude: CLLocationDegrees(fromLatitude), longitude: CLLocationDegrees(fromLontitude));
                    if (self.routeLine == nil){
                        self.drawLine(toLocation: CLLocationCoordinate2D(latitude: CLLocationDegrees(toLatitude), longitude: CLLocationDegrees(toLongtitude)));
                    }
                }
            }
        }
    }
    
    private func updateTrip() {
        guard let userId = Values.currentUser?.id else { return }
        let parameters = ["user_id": userId];
        Helpers.request(api: API.getCurrentActiveTrip, method: .post, parameters: parameters) { (result) in
            guard let result = result else { return }
            if let success = result["success"] as? Bool, success {
                guard let data = result["data"] as? [String: Any] else { return }
                ActiveTripViewController.activeTrip = Trip(dic: data);
            }
        }
    }
}

extension ActiveTripViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        if(ActiveTripViewController.activeTrip == nil){
            self.clearRoutes();
            showMarker(position: coordinate);
            drawLine(toLocation: coordinate);
        }
    }
    
    func showMarker(position: CLLocationCoordinate2D){
        self.marker?.map = nil;
        self.marker = GMSMarker()
        self.marker?.position = position;
        Helpers.getCoordinateDetails(coordinate: position) { (title, snipet) in
            self.marker?.title = title;
            self.marker?.snippet = snipet;
            self.marker?.map = self.myMapView;
        }
    }
    
    // Check if user authorized access to current location of the device.
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Location Services Disabled", message: "Buber needs to access device location capabilities to use Map services. You can't take a ride or create a trip without allowing Buber to access your location services.", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                })
                alert.addAction(okAction);
                self.present(alert, animated: false, completion: nil);
                return;
            }
        } else {
            myMapView.isMyLocationEnabled = true;
            myMapView.settings.myLocationButton = true;
            locationManager.startUpdatingLocation();
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            myMapView.isMyLocationEnabled = true;
            myMapView.settings.myLocationButton = false;
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            if(myLocation == nil ){
                self.myMapView.camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 14);
                myLocation = location.coordinate;
                fromLocation = location.coordinate;
            } else {
                self.myLocation = location.coordinate;
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return false;
    }
    
    fileprivate func drawLine(toLocation: CLLocationCoordinate2D?) {
        if toLocation == nil { return }
        
        clearRoutes();
        Helpers.getPlaceId(coordinate: toLocation!, completion: { (place) in
            self.toPlaceId = place;
            ActiveTripViewController.activeTrip?.toLocationId = place;
        })
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        guard let myLocation = fromLocation else {return}
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(myLocation.latitude),\(myLocation.longitude)&destination=\(toLocation!.latitude),\(toLocation!.longitude)&alternatives=true&sensor=false&key=\(KeyStrings.googlePlacesKey)&language=en")!
        
        self.myMapView.startActivityIndicator();
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            self.myMapView.stopActivityIndicator();
            
            if error != nil {
                print(error!.localizedDescription)
                PopupMessages.showErrorMessage(message: error!.localizedDescription, buttonText: "Ok".localized);
            } else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        guard let routes = json["routes"] as? NSArray else {
                            DispatchQueue.main.async {
                                self.myMapView.stopActivityIndicator();
                            }
                            return
                        }
                        
                        print("Routes count: ", routes.count);
                        var routesDisplayed = 0;
                        
                        if (routes.count > 0) {
                            
                            if routesDisplayed < 1 {
                                let overview_polyline = routes[0] as? NSDictionary
                                let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                
                                let points = dictPolyline?.object(forKey: "points") as? String
                                
                                routesDisplayed += 1;
                                
                                
                                if let routeProperties = (((routes[0] as! NSDictionary)["legs"]) as? NSArray)?.firstObject as? [String: Any] {
                                    
                                    print("Route properties: ", routeProperties);
                                    
                                    var address = "";
                                    var startAddress = "";
                                    if let endAddress = routeProperties["end_address"] as? String {
                                        address = endAddress;
                                    }
                                    
                                    if let startAddress_ = routeProperties["start_address"] as? String {
                                        startAddress = startAddress_;
                                    }
                                    if let distance = (routeProperties["distance"] as! [String: Any])["value"] as? Double { // in meters
                                        self.distanceInMeters = distance;
                                    }
                                    
                                    if let distanceText = (routeProperties["distance"] as! [String: Any])["text"] as? String { // in kilometers
                                        self.distanceText = distanceText;
                                    }
                                    if let durationInMinutes = (routeProperties["duration"] as! [String: Any])["value"] as? Int {
                                        self.durationInMinutes = durationInMinutes / 60;
                                    }
                                    if let durationText = (routeProperties["duration"] as! [String: Any])["text"] as? String{ // in minutes
                                        self.durationText = durationText;
                                    }
                                    
                                    self.showPath(polyStr: points!, routeNumber: routesDisplayed);
                                    self.showMarker(position: toLocation!);
                                    if(ActiveTripViewController.activeTrip == nil){
                                        if let fromLocation = self.fromLocation {
                                            Helpers.getPlaceId(coordinate: fromLocation, completion: { (placeId) in
                                                ActiveTripViewController.activeTrip?.fromLocationId = placeId;
                                                self.createTrip(destination: toLocation!, startAddress: startAddress, address: address, fromPlaceId: placeId);
                                            })
                                        }
                                    }
                                }
                            }
                            
                            DispatchQueue.main.async {
                                self.myMapView.stopActivityIndicator();
                                
                                let bounds = GMSCoordinateBounds(coordinate: myLocation, coordinate: toLocation!)
                                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(170, 30, 30, 30))
                                self.myMapView.moveCamera(update)
                            }
                        }
                        else {
                            PopupMessages.showWarningMessage(message: "UnableToGetRoutes".localized);
                            DispatchQueue.main.async {
                                self.myMapView.stopActivityIndicator();
                            }
                        }
                    }
                }
                catch {
                    print("error in JSONSerialization")
                    DispatchQueue.main.async {
                        self.myMapView.stopActivityIndicator();
                    }
                }
            }
        })
        task.resume()
    }
    
    @objc private func clearRoutes(){
        if routeLine != nil {
            routeLine!.map = nil;
            routeLine = nil;
        }
        self.marker?.map = nil;
        self.taxiMarker?.map = nil;
    }
    
    private func listenToActiveTrip() {
        SocketIOManager.sharedInstance.socket.on("create-trip") { (result, emitter) in }
    }
    
    fileprivate func createTrip(destination: CLLocationCoordinate2D, startAddress: String, address: String, fromPlaceId: String) {
        ActiveTripViewController.statusText = "NoRide".localized;
        let createTripController = RequestTripViewController();
        createTripController.destinationAddress = address;
        createTripController.distanceInMeters = distanceInMeters;
        createTripController.distanceLabel.text = distanceText;
        createTripController.minutesLabel.text = durationText;
        createTripController.durationInMinutes = self.durationInMinutes;
        createTripController.fromLocationId = fromPlaceId;
        createTripController.toPlaceId = self.toPlaceId;
        createTripController.sourceAddress = startAddress;
        createTripController.sourceLocation = self.fromLocation;
        createTripController.destinationLocation = destination;
        createTripController.modalTransitionStyle = .coverVertical;
        createTripController.modalPresentationStyle = .overFullScreen;
        self.present(createTripController, animated: true, completion: nil);
    }
    
    fileprivate func showPath(polyStr: String, routeNumber: Int){
        let path = GMSPath(fromEncodedPath: polyStr);
        switch routeNumber {
        case 0, 1:
            routeLine = GMSPolyline(path: path)
            routeLine!.strokeWidth = 3.0;
            routeLine!.strokeColor = Style.mainColor;
            routeLine!.map = myMapView;
        default:
            return;
        }
    }
    
    fileprivate func drawDriverPath(polyStr: String, routeNumber: Int){
        let path = GMSPath(fromEncodedPath: polyStr);
        switch routeNumber {
        case 0:
            routeLine = GMSPolyline(path: path)
            routeLine!.strokeWidth = 3.0;
            routeLine!.strokeColor = Style.mainColor;
            routeLine!.map = myMapView;
        default:
            return;
        }
    }
    
    fileprivate func updateDriverPath(){
        SocketIOManager.sharedInstance.socket.on("update-user-location") { (result, _) in
            print("UPdate update update: ", result);
            var markerPosition: CLLocationCoordinate2D!
            if let result = result as? Array<[String: Any]> {
                if let data = result[0]["data"] as? [String: Any], let driverLocationData = data["data"] as? [String: Any] {
                    if let lat = driverLocationData["lat"] as? Double, let lng = driverLocationData["lng"] as? Double {
                        markerPosition = CLLocationCoordinate2D(latitude: lat, longitude: lng);
                        print("update route Passed...")
                        self.driverPath.add(CLLocationCoordinate2D(latitude: lat, longitude: lng));
                        if ActiveTripViewController.activeTrip == nil {
                            self.clearRoutes();
                            print("update route Oh noooo");
                            return;
                        }
                    }
                    DispatchQueue.main.async {
                        if(self.taxiMarker == nil){
                            print("update route Changing position...")
                            self.taxiMarker = GMSMarker(position: markerPosition);
                            self.taxiMarker?.icon = #imageLiteral(resourceName: "taxi").imageWithImage(scaledToSize: CGSize(width: 34, height: 34));
                            self.taxiMarker?.tracksViewChanges = true;
                            self.taxiMarker?.map = self.myMapView;
                        } else {
                            CATransaction.begin()
                            CATransaction.setAnimationDuration(1.0)
                            self.taxiMarker?.position =  markerPosition;
                            CATransaction.commit()
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func checkPaymentStatus() {
        if let paymentType = ActiveTripViewController.activeTrip?.ride?.paymentMethod, let paymentStatus = ActiveTripViewController.activeTrip?.ride?.paymentStatus {
            if paymentStatus == .pending && paymentType != .cash {
                Sound.play(file: "buber.mp3");
                let payforRidePopup = PayForRidePopup();
                UserDefaults.standard.setValue(ActiveTripViewController.activeTrip?.ride?.paymentDetails, forKey: "paymentDetails");
                payforRidePopup.modalTransitionStyle = .coverVertical;
                payforRidePopup.modalPresentationStyle = .overFullScreen;
                self.present(payforRidePopup, animated: true, completion: nil);
                ActiveTripViewController.stateLabel.text = "PleasePayForRide".localized;
            } else {
                ActiveTripViewController.stateLabel.text = "youHaveAnActiveTrip".localized;
            }
        }
    }
}

extension ActiveTripViewController: GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {

        ActiveTripViewController.activeTrip = nil;
        self.myMapView.animate(toLocation: place.coordinate);
        self.drawLine(toLocation: place.coordinate);
        dismiss(animated: true, completion: nil);
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        PopupMessages.showThinyWarningMessage(message: error.localizedDescription);
         dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
       print("Cancelled")
         dismiss(animated: true, completion: nil)
    }
}
