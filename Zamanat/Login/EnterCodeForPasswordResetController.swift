//
//  EnterCodeForPasswordResetController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 12/1/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

//
//  EnterCodeViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 4/24/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.


import UIKit
import FirebaseAuth

class EnterCodeForPasswordResetController: UIViewController {
    
    var phoneAndAid = [String: String]()  {
        didSet {
            phoneNumberLabel.text = phoneAndAid["phone"];
        }
    }
    
    let phoneNumberLabel: UILabel = {
        let label = UILabel();
        label.font = Style.veryBigFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let codeTextField: UITextField = {
        let textField = UITextField();
        textField.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        textField.layer.borderWidth = 1;
        textField.layer.cornerRadius = 4;
        textField.layer.masksToBounds = true;
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.textAlignment = .center;
        textField.rightView = padView;
        textField.leftView = padView;
        textField.placeholder = "EnterCode".localized;
        textField.keyboardType = .asciiCapableNumberPad;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    lazy var signupButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(checkCode), for: .touchUpInside);
        button.setTitle("Next".localized, for: .normal);
        button.setTitleColor(.white, for: .normal);
        button.backgroundColor = Style.mainColor;
        button.layer.cornerRadius = 4;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        
        view.addSubview(phoneNumberLabel);
        view.addSubview(codeTextField);
        view.addSubview(signupButton);
        
        if let phoneNumber = UserDefaults.standard.value(forKey: KeyStrings.phoneNumber) as? String {
            self.phoneNumberLabel.text = phoneNumber;
        }
        
        setupConstraints();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.codeTextField.becomeFirstResponder();
    }
    
    @objc private func checkCode() {
        self.view.endEditing(true);
        
        guard let code = codeTextField.text, !code.isEmpty else {
            PopupMessages.showThinyWarningMessage(message: "EnterCode".localized);
            self.codeTextField.becomeFirstResponder();
            return;
        }
        
        view.startActivityIndicator();
        let credintials = PhoneAuthProvider.provider().credential(withVerificationID: phoneAndAid["aid"]!, verificationCode: code);
        Auth.auth().signIn(with: credintials) { (user, error) in
            self.view.stopActivityIndicator();
            if (error != nil) {
                PopupMessages.showWarningMessage(title: "", message: error!.localizedDescription.localized);
                return;
            }
            
            let resetPasswordController = ResetPasswordViewController();
            resetPasswordController.phone = self.phoneAndAid["phone"]!;
            self.navigationController?.pushViewController(resetPasswordController, animated: true);
        }
    }
    
    private func setupConstraints() {
        //        enterCodelabel
        phoneNumberLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 44).isActive = true;
        phoneNumberLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        //        codeTextField
        codeTextField.topAnchor.constraint(equalTo: phoneNumberLabel.bottomAnchor, constant: 30).isActive = true;
        codeTextField.centerXAnchor.constraint(equalTo: phoneNumberLabel.centerXAnchor).isActive = true;
        codeTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true;
        codeTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true;
        
        //        signupButton
        signupButton.topAnchor.constraint(equalTo: codeTextField.bottomAnchor, constant: 22).isActive = true;
        signupButton.centerXAnchor.constraint(equalTo: codeTextField.centerXAnchor).isActive = true;
        signupButton.heightAnchor.constraint(equalTo: codeTextField.heightAnchor).isActive = true;
        signupButton.widthAnchor.constraint(equalTo: codeTextField.widthAnchor).isActive = true;
    }
}

