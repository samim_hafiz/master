//
//  LoginViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 4/9/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import GoogleMaps

class LoginViewController: UIViewController {
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.image = #imageLiteral(resourceName: "buber_logo").withRenderingMode(.alwaysOriginal);
        imageView.tintColor = Style.mainColor;
        imageView.contentMode = .scaleAspectFit;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let descriptionLabel: UILabel = {
        let label = UILabel();
        label.text = "Take rides. Track your beloved ones.".localized;
        label.font = Style.smallFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let codeLabel: UILabel = {
        let label = UILabel();
        label.text = "+93"
        label.font = Style.normalFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let phoneNumberTextField: UITextField = {
        let textField = UITextField();
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.rightView = padView;
        textField.leftView = padView;
        textField.placeholder = "e.g, 797134454";
        textField.keyboardType = .asciiCapableNumberPad;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    let phoneView: UIView = {
        let view = UIView();
        view.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        view.layer.borderWidth = 1;
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = true;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    let passwordTextField: UITextField = {
        let textField = UITextField();
        textField.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        textField.layer.borderWidth = 1;
        textField.layer.cornerRadius = 4;
        textField.layer.masksToBounds = true;
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.rightView = padView;
        textField.leftView = padView;
        textField.placeholder = "Password".localized;
        textField.isSecureTextEntry = true;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    lazy var loginButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(login), for: .touchUpInside);
        button.setTitle("Login".localized, for: .normal);
        button.setTitleColor(.white, for: .normal);
        button.backgroundColor = Style.mainColor;
        button.layer.cornerRadius = 4;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    lazy var forgotPassword: UILabel = {
        let label = UILabel();
        label.text = "ForgotPassword".localized;
        label.font = Style.smallFont;
        let tap = UITapGestureRecognizer(target: self, action: #selector(forgotPasswordTap));
        label.addGestureRecognizer(tap);
        label.isUserInteractionEnabled = true;
        label.textColor = Style.mainColor;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let dontHaveAccount: UILabel = {
        let label = UILabel();
        label.text = "Don't have account?".localized
        label.font = Style.smallerFont;
        label.textColor = Style.grayColor;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var registerButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(registerAccount), for: .touchUpInside);
        button.setTitle("Register".localized, for: .normal);
        button.setTitleColor(Style.mainColor, for: .normal);
        button.layer.cornerRadius = 4;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        // Do any additional setup after loading the view.
        
        view.addSubview(logoImageView);
        view.addSubview(descriptionLabel);
        view.addSubview(phoneView);
        phoneView.addSubview(codeLabel);
        phoneView.addSubview(phoneNumberTextField);
        view.addSubview(passwordTextField);
        view.addSubview(loginButton);
        view.addSubview(forgotPassword);
        view.addSubview(dontHaveAccount);
        view.addSubview(registerButton);
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard));
        self.view.addGestureRecognizer(tap);
        
        if !CLLocationManager.locationServicesEnabled() {
            checkLocationAuthorizationStatus();
        }
        
        setupConstraints();
    }
    
    @objc private func closeKeyboard(){
        self.view.endEditing(true);
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Location Services Disabled", message: "Buber needs to access device location capabilities to use Map services. You can't take a ride or create a trip without allowing Buber to access your location services.", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "Ok".localized, style: .default, handler: { (action) in
                })
                alert.addAction(okAction);
                self.present(alert, animated: false, completion: nil);
                return;
            }
        } 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        phoneNumberTextField.becomeFirstResponder();
    }
    
    @objc private func login(){
        self.view.endEditing(true);
        guard var phone = phoneNumberTextField.text, !phone.isEmpty else {
            PopupMessages.showInfoMessage(title: "Info".localized, message: "Enter your phone number".localized);
            return;
        }
        guard let password = passwordTextField.text, !password.isEmpty else {
            PopupMessages.showInfoMessage(title: "Info".localized, message: "Enter your password".localized);
            return;
        }
        if(phone.first! == "0"){
            phone.remove(at: phone.startIndex);
        }
        let parameters = ["phone": "93" + phone, "password": password];
        view.startActivityIndicator();
        Helpers.request(api: API.login, method: .post, parameters: parameters) { (resultDic) in
            self.view.stopActivityIndicator();
            
            guard let result = resultDic else {
                PopupMessages.showWarningMessage(message: "Server Error".localized);
                return;
            }
            if let success = result["success"] as? Bool, success {
                if let userDic = result["data"] as? [String: Any] {
                    Values.currentUser = User(dic: userDic);
                    if let id = Values.currentUser?.id {
                         SocketIOManager.sharedInstance.emitWelcome("welcome", ["user_id": id]);
                    }
                    self.dismiss(animated: true, completion: nil);
                }
            } else {
                PopupMessages.showWarningMessage(title: "", message: result["message"] as! String);
            }
        }
    }
    
    @objc private func forgotPasswordTap() {
        let enterPhoneViewController = EnterPhoneForResetPasswordViewController();
        self.navigationController?.pushViewController(enterPhoneViewController, animated: true);
    }
    
    @objc private func registerAccount(){
        let registerViewController = RegisterViewController();
        self.navigationController?.pushViewController(registerViewController, animated: true);
    }
    
    private func setupConstraints() {
        logoImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 62).isActive = true;
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        logoImageView.heightAnchor.constraint(equalToConstant: 32).isActive = true;
        logoImageView.widthAnchor.constraint(equalToConstant: 110).isActive = true;
        
        descriptionLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 12).isActive = true;
        descriptionLabel.centerXAnchor.constraint(equalTo: logoImageView.centerXAnchor).isActive = true;
        
        phoneView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 44).isActive = true;
        phoneView.centerXAnchor.constraint(equalTo: descriptionLabel.centerXAnchor).isActive = true;
        phoneView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true;
        phoneView.heightAnchor.constraint(equalToConstant: 48).isActive = true;
        
        codeLabel.leftAnchor.constraint(equalTo: phoneView.leftAnchor).isActive = true;
        codeLabel.heightAnchor.constraint(equalTo: phoneView.heightAnchor).isActive = true;
        codeLabel.widthAnchor.constraint(equalToConstant: 44).isActive = true;
        codeLabel.centerYAnchor.constraint(equalTo: phoneView.centerYAnchor).isActive = true;
        
        phoneNumberTextField.leftAnchor.constraint(equalTo: codeLabel.rightAnchor, constant: 2).isActive = true;
        phoneNumberTextField.centerYAnchor.constraint(equalTo: phoneView.centerYAnchor).isActive = true;
        phoneNumberTextField.rightAnchor.constraint(equalTo: phoneView.rightAnchor).isActive = true;
        phoneNumberTextField.heightAnchor.constraint(equalTo: phoneView.heightAnchor).isActive = true;
        
        passwordTextField.topAnchor.constraint(equalTo: phoneNumberTextField.bottomAnchor, constant: 10).isActive = true;
        passwordTextField.centerXAnchor.constraint(equalTo: phoneView.centerXAnchor).isActive = true;
        passwordTextField.widthAnchor.constraint(equalTo: phoneView.widthAnchor).isActive = true;
        passwordTextField.heightAnchor.constraint(equalTo: phoneView.heightAnchor).isActive = true;
        
        loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 30).isActive = true;
        loginButton.centerXAnchor.constraint(equalTo: passwordTextField.centerXAnchor).isActive = true;
        loginButton.widthAnchor.constraint(equalTo: passwordTextField.widthAnchor).isActive = true;
        loginButton.heightAnchor.constraint(equalTo: passwordTextField.heightAnchor).isActive = true;
        
        forgotPassword.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 16).isActive = true;
        forgotPassword.centerXAnchor.constraint(equalTo: loginButton.centerXAnchor).isActive = true;
        forgotPassword.heightAnchor.constraint(equalToConstant: 28).isActive = true;
        
        dontHaveAccount.bottomAnchor.constraint(equalTo: registerButton.topAnchor, constant: -4).isActive = true;
        dontHaveAccount.centerXAnchor.constraint(equalTo: loginButton.centerXAnchor).isActive = true;
        
        registerButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -35).isActive = true;
        registerButton.centerXAnchor.constraint(equalTo: dontHaveAccount.centerXAnchor).isActive = true;
    }
}
