//
//  WelcomeViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 7/22/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.contentMode = .scaleAspectFit;
        imageView.backgroundColor = .black;
        imageView.image = #imageLiteral(resourceName: "buber-logo-png");
        imageView.clipsToBounds = true;
        imageView.alpha = 0.8;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    let coverImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.contentMode = .scaleAspectFill;
        imageView.backgroundColor = .black;
        imageView.image = #imageLiteral(resourceName: "buber-welcome-image-min");
        imageView.clipsToBounds = true;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }();
    
    lazy var loginButton: UIButton = {
        let button = UIButton(type: .custom);
        button.layer.cornerRadius = 4;
        button.addTarget(self, action: #selector(loginTapped), for: .touchUpInside);
        button.layer.masksToBounds = true;
        button.backgroundColor = Style.differColor;
        button.setTitle("Login".localized, for: .normal);
        button.setTitleColor(.white, for: .normal);
        button.titleLabel?.font = Style.normalFont;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }()
    
    lazy var registerButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(registerTapped), for: .touchUpInside);
        button.layer.cornerRadius = 4;
        button.layer.borderColor = UIColor.white.cgColor;
        button.layer.borderWidth = 1;
        button.layer.masksToBounds = true;
        button.backgroundColor = .clear;
        button.setTitle("Register".localized, for: .normal);
        button.setTitleColor(.white, for: .normal);
        button.titleLabel?.font = Style.normalFont;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }()
    
    let buttonsStackView: UIStackView = {
        let stackView = UIStackView();
        stackView.alignment = .center;
        stackView.axis = .horizontal;
        stackView.spacing = 16;
        stackView.distribution = .fillEqually;
        stackView.isBaselineRelativeArrangement = true;
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        return stackView;
    }();

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black;
        view.addSubview(coverImageView);
        view.addSubview(logoImageView);
        view.addSubview(buttonsStackView);
        buttonsStackView.addArrangedSubview(loginButton);
        buttonsStackView.addArrangedSubview(registerButton);
        
        UIApplication.shared.statusBarStyle = .lightContent;
        
        setupConstraints();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
         self.navigationController?.setNavigationBarHidden(true, animated: true);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        self.navigationController?.setNavigationBarHidden(false, animated: true);
        UIApplication.shared.statusBarStyle = .default;
    }
    
    @objc private func registerTapped() {
        let registerViewController = RegisterViewController();
        self.navigationController?.pushViewController(registerViewController, animated: true);
    }
    
    @objc private func loginTapped() {
        let loginViewController = LoginViewController();
        self.navigationController?.pushViewController(loginViewController, animated: true);
    }
    
    private func setupConstraints() {
        logoImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 60).isActive = true;
        logoImageView.widthAnchor.constraint(equalToConstant: 120).isActive = true;
        logoImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true;
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        coverImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true;
        coverImageView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true;
        coverImageView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true;
        coverImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        buttonsStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -30).isActive = true;
        buttonsStackView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true;
        buttonsStackView.heightAnchor.constraint(equalToConstant: 48).isActive = true;
        buttonsStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        registerButton.heightAnchor.constraint(equalToConstant: 42).isActive = true;
        loginButton.heightAnchor.constraint(equalToConstant: 44).isActive = true;
        
    }
}
