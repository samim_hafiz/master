//
//  ResetPasswordViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 12/1/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit
import FirebaseAuth

class EnterPhoneForResetPasswordViewController: UIViewController {
    
    let resetPasswordTitle: UILabel = {
        let label = UILabel();
        label.text = "Reset Password".localized;
        label.font = Style.normalBoldFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let descriptionLabel: UILabel = {
        let label = UILabel();
        label.text = "EnterPhoneWithThisAccount".localized;
        label.font = Style.smallFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let codeLabel: UILabel = {
        let label = UILabel();
        label.text = "+93"
        label.font = Style.normalFont;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let phoneNumberTextField: UITextField = {
        let textField = UITextField();
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.rightView = padView;
        textField.leftView = padView;
        textField.placeholder = "e.g, 797134454";
        textField.keyboardType = .asciiCapableNumberPad;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    let phoneView: UIView = {
        let view = UIView();
        view.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        view.layer.borderWidth = 1;
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = true;
        view.translatesAutoresizingMaskIntoConstraints = false;
        return view;
    }();
    
    lazy var nextButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(sendCode), for: .touchUpInside);
        button.setTitle("Next".localized, for: .normal);
        button.setTitleColor(.white, for: .normal);
        button.backgroundColor = Style.mainColor;
        button.layer.cornerRadius = 4;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    let dontHaveAccount: UILabel = {
        let label = UILabel();
        label.text = "Don't have account?".localized
        label.font = Style.smallerFont;
        label.textColor = Style.grayColor;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    lazy var registerButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(registerAccount), for: .touchUpInside);
        button.setTitle("Register".localized, for: .normal);
        button.setTitleColor(Style.mainColor, for: .normal);
        button.layer.cornerRadius = 4;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        
        view.addSubview(resetPasswordTitle);
        view.addSubview(descriptionLabel);
        view.addSubview(phoneView);
        phoneView.addSubview(codeLabel);
        phoneView.addSubview(phoneNumberTextField);
        view.addSubview(dontHaveAccount);
        view.addSubview(registerButton);
        view.addSubview(nextButton);
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard));
        self.view.addGestureRecognizer(tap);
        
        setupConstraints();
    }
    
    @objc private func closeKeyboard(){
        self.view.endEditing(true);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        phoneNumberTextField.becomeFirstResponder();
    }
    
    @objc private func sendCode(){
        self.closeKeyboard();
        if let phoneNumber = phoneNumberTextField.text, phoneNumber.count == 9 {
            view.startActivityIndicator();
            Helpers.request(api: API.checkPhone, method: .post, parameters: ["phone": phoneNumber], completion: { (result) in
                self.view.stopActivityIndicator();
                guard let result = result, let success = result["success"] as? Bool, success else {
                    PopupMessages.showWarningMessage(message: "NoAccountForThisPhone".localized);
                    return;
                }
                
                self.sendCodeTo(phone: "+93" + phoneNumber);
            })
            
        } else {
            PopupMessages.showThinyWarningMessage(message: "EnterValidPhone".localized);
            self.phoneNumberTextField.becomeFirstResponder();
        }
    }
    
    private func sendCodeTo(phone: String) {
        print("\n\n\n\n Phone is here: ", phone);
        view.startActivityIndicator();
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { (aid, error) in
            self.view.stopActivityIndicator();
            
            if(error != nil){
                PopupMessages.showWarningMessage(title: "", message: error!.localizedDescription.localized);
                print("error: ", error);
                return;
            } else {
                
                let enterCodeController = EnterCodeForPasswordResetController();
                enterCodeController.phoneAndAid = ["phone": phone, "aid": aid!];
                self.navigationController?.pushViewController(enterCodeController, animated: true);
            }
        }
    }
    
    @objc private func registerAccount(){
        let registerViewController = RegisterViewController();
        self.navigationController?.pushViewController(registerViewController, animated: true);
    }
    
    private func setupConstraints() {
        resetPasswordTitle.topAnchor.constraint(equalTo: view.topAnchor, constant: 62).isActive = true;
        resetPasswordTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        resetPasswordTitle.heightAnchor.constraint(equalToConstant: 32).isActive = true;
        resetPasswordTitle.widthAnchor.constraint(equalToConstant: 110).isActive = true;
        
        descriptionLabel.topAnchor.constraint(equalTo: resetPasswordTitle.bottomAnchor, constant: 12).isActive = true;
        descriptionLabel.centerXAnchor.constraint(equalTo: resetPasswordTitle.centerXAnchor).isActive = true;
        
        phoneView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 44).isActive = true;
        phoneView.centerXAnchor.constraint(equalTo: descriptionLabel.centerXAnchor).isActive = true;
        phoneView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true;
        phoneView.heightAnchor.constraint(equalToConstant: 48).isActive = true;
        
        codeLabel.leftAnchor.constraint(equalTo: phoneView.leftAnchor).isActive = true;
        codeLabel.heightAnchor.constraint(equalTo: phoneView.heightAnchor).isActive = true;
        codeLabel.widthAnchor.constraint(equalToConstant: 44).isActive = true;
        codeLabel.centerYAnchor.constraint(equalTo: phoneView.centerYAnchor).isActive = true;
        
        phoneNumberTextField.leftAnchor.constraint(equalTo: codeLabel.rightAnchor, constant: 2).isActive = true;
        phoneNumberTextField.centerYAnchor.constraint(equalTo: phoneView.centerYAnchor).isActive = true;
        phoneNumberTextField.rightAnchor.constraint(equalTo: phoneView.rightAnchor).isActive = true;
        phoneNumberTextField.heightAnchor.constraint(equalTo: phoneView.heightAnchor).isActive = true;
        
        nextButton.topAnchor.constraint(equalTo: phoneView.bottomAnchor, constant: 30).isActive = true;
        nextButton.centerXAnchor.constraint(equalTo: phoneView.centerXAnchor).isActive = true;
        nextButton.widthAnchor.constraint(equalTo: phoneNumberTextField.widthAnchor).isActive = true;
        nextButton.heightAnchor.constraint(equalTo: phoneNumberTextField.heightAnchor).isActive = true;
        
        dontHaveAccount.bottomAnchor.constraint(equalTo: registerButton.topAnchor, constant: -4).isActive = true;
        dontHaveAccount.centerXAnchor.constraint(equalTo: nextButton.centerXAnchor).isActive = true;
        
        registerButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40).isActive = true;
        registerButton.centerXAnchor.constraint(equalTo: dontHaveAccount.centerXAnchor).isActive = true;
    }
}

