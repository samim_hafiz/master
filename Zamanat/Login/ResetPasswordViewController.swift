//
//  ResetPasswordViewController.swift
//  Zamanat
//
//  Created by Saifuddin Sepehr on 12/1/18.
//  Copyright © 2018 Afghanistan Holding Group. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    var phone: String = "";
    let changePasswordLabel: UILabel = {
        let label = UILabel();
        label.numberOfLines = 0;
        label.font = Style.normalFont;
        label.textAlignment = .center;
        label.text = "enterNewPasswordToReset".localized;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }();
    
    let newPasswordTextField: UITextField = {
        let textField = UITextField();
        textField.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        textField.layer.borderWidth = 1;
        textField.layer.cornerRadius = 4;
        textField.layer.masksToBounds = true;
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.textAlignment = .center;
        textField.rightView = padView;
        textField.leftView = padView;
        textField.placeholder = "New Password".localized;
        textField.keyboardType = .asciiCapableNumberPad;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    let repeatNewPassword: UITextField = {
        let textField = UITextField();
        textField.layer.borderColor = Style.mainColor.withAlphaComponent(0.5).cgColor;
        textField.layer.borderWidth = 1;
        textField.layer.cornerRadius = 4;
        textField.layer.masksToBounds = true;
        let padView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 5));
        textField.rightViewMode = .always;
        textField.leftViewMode = .always;
        textField.textAlignment = .center;
        textField.rightView = padView;
        textField.leftView = padView;
        textField.placeholder = "Repeat New Password".localized;
        textField.keyboardType = .asciiCapableNumberPad;
        textField.font = Style.normalFont;
        textField.translatesAutoresizingMaskIntoConstraints = false;
        return textField;
    }();
    
    lazy var resetButton: UIButton = {
        let button = UIButton(type: .custom);
        button.addTarget(self, action: #selector(resetPassword), for: .touchUpInside);
        button.setTitle("Reset".localized, for: .normal);
        button.setTitleColor(.white, for: .normal);
        button.backgroundColor = Style.mainColor;
        button.layer.cornerRadius = 4;
        button.layer.masksToBounds = true;
        button.translatesAutoresizingMaskIntoConstraints = false;
        return button;
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        
        view.addSubview(changePasswordLabel);
        view.addSubview(newPasswordTextField);
        view.addSubview(repeatNewPassword);
        view.addSubview(resetButton);
        
        if let phoneNumber = UserDefaults.standard.value(forKey: KeyStrings.phoneNumber) as? String {
            self.changePasswordLabel.text = phoneNumber;
        }
        
        setupConstraints();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.newPasswordTextField.becomeFirstResponder();
    }
    
    @objc private func resetPassword() {
        self.view.endEditing(true);
        
        guard let newPassword = newPasswordTextField.text, newPassword.count > 4 else {
            PopupMessages.showWarningMessage(message: "invalidPassword".localized);
            return;
        }
        
        guard let repeatPassword = repeatNewPassword.text, (repeatPassword.count > 4 || repeatPassword == newPassword) else {
            PopupMessages.showWarningMessage(message: "passwordRepeatIncorrecct".localized);
            return;
        }
        
            let parameters = ["phone": self.phone, "password": newPassword];
            
            view.startActivityIndicator();
            Helpers.request(api: API.resetPassword, method: .put, parameters: parameters, completion: { (result) in
                self.view.stopActivityIndicator();
                
                print("Reset Result: ", result);
                
                guard let result = result else { return }
                let message = result["message"] as? String ?? "";
                if let success = result["success"] as? Bool, success {
                    PopupMessages.showSuccessMessage(message: message);
                    self.navigationController?.popToRootViewController(animated: true);
                    
                } else {
                     PopupMessages.showWarningMessage(message: message);
                }
            })
    }
    
    private func setupConstraints() {
        //        enterCodelabel
        changePasswordLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 44).isActive = true;
        changePasswordLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        changePasswordLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7);
        
        //        newPasswordTextField
        newPasswordTextField.topAnchor.constraint(equalTo: changePasswordLabel.bottomAnchor, constant: 30).isActive = true;
        newPasswordTextField.centerXAnchor.constraint(equalTo: changePasswordLabel.centerXAnchor).isActive = true;
        newPasswordTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true;
        newPasswordTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true;
        
        //        repeatNewPasswordTextField
        repeatNewPassword.topAnchor.constraint(equalTo: newPasswordTextField.bottomAnchor, constant: 16).isActive = true;
        repeatNewPassword.centerXAnchor.constraint(equalTo: changePasswordLabel.centerXAnchor).isActive = true;
        repeatNewPassword.heightAnchor.constraint(equalToConstant: 44).isActive = true;
        repeatNewPassword.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true;
        
        //        resetButton
        resetButton.topAnchor.constraint(equalTo: repeatNewPassword.bottomAnchor, constant: 22).isActive = true;
        resetButton.centerXAnchor.constraint(equalTo: repeatNewPassword.centerXAnchor).isActive = true;
        resetButton.heightAnchor.constraint(equalTo: repeatNewPassword.heightAnchor).isActive = true;
        resetButton.widthAnchor.constraint(equalTo: repeatNewPassword.widthAnchor).isActive = true;
    }
}

